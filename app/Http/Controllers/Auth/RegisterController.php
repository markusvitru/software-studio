<?php

namespace App\Http\Controllers\Auth;

use Modules\Customers\Entities\Customers\Customer;
use App\Http\Controllers\Controller;
use Modules\Customers\Entities\Customers\Repositories\Interfaces\CustomerRepositoryInterface;
use Modules\Customers\Entities\Customers\Requests\CreateCustomerRequest;
use Modules\Customers\Entities\Customers\Requests\RegisterCustomerRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/accounts';

    private $customerRepo;

    /**
     * Create a new controller instance.
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->middleware('guest');
        $this->customerRepo = $customerRepository;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Customer
     */
    protected function create(array $data)
    {
        return $this->customerRepo->createCustomer($data);
    }

    /**
     * @param RegisterCustomerRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function register(RegisterCustomerRequest $request)
    {
        $request['birthday'] = null;
        $request['scholarity_id'] = null;
        $request['status'] = 1;
        $request['scholarity_id'] = 1;
        $request['customer_status_id'] = 1;
        $request['customer_lead_id'] = 1;
        $request['city_id'] = 1;
        $request['data_politics'] = 1;
        $request['genre_id'] = 1;
        $request['customer_lead_id'] = 1;
        $request['civil_status_id'] = 1;
        $customer = $this->create($request->except('_method', '_token'));
        Auth::login($customer);

        return redirect()->route('accounts');
    }
}
