<?php

namespace Modules\Studio\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Companies\Entities\ActionRole\ActionRole;
use Modules\Companies\Entities\Actions\Action;
use Modules\Companies\Entities\PermissionGroups\PermissionGroup;
use Modules\Companies\Entities\Permissions\Permission;
use Modules\Companies\Entities\Roles\Repositories\RoleRepository;
use Modules\Companies\Entities\Roles\Role;
use Modules\Studio\Entities\Horaries\Horary;

class StudioDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Horary::class)->create([
            'name' => 'Mañana'
        ]);

        factory(Horary::class)->create([
            'name' => 'Tarde'
        ]);

        factory(Horary::class)->create([
            'name' => 'Noche'
        ]);

        $roleAdmin = Role::find(1);
        $roleManager = factory(Role::class)->create([
            'name'         => 'manager',
            'display_name' => 'Supervisor'
        ]);

        $roleModel = factory(Role::class)->create([
            'name'         => 'modelo',
            'display_name' => 'Modelo'
        ]);

        $roleLeader = factory(Role::class)->create([
            'name'         => 'lider',
            'display_name' => 'Lider'
        ]);
        $roleModelRepo = new RoleRepository($roleModel);
        $roleLeaderRepo = new RoleRepository($roleLeader);
        $roleSuperRepo = new RoleRepository($roleAdmin);
        $roleManagerRepo = new RoleRepository($roleManager);

        $permissionGroupStudio = factory(PermissionGroup::class)->create([
            'name' => 'Studio'
        ]);

        $moduleHoraries = factory(Permission::class)->create([
            'name'         => 'horaries',
            'display_name' => 'Horarios',
            'icon'         => 'ni ni-watch-time',
            'permission_group_id' =>  $permissionGroupStudio->id
        ]);

        $roleSuperRepo->attachToPermission($moduleHoraries);

        // Acciones Módulo Horarios
        $actionHoraryViews = factory(Action::class)->create([
            'permission_id' => $moduleHoraries->id,
            'name'          => 'Ver Horarios',
            'icon'          => 'fas fa-eye',
            'route'         => 'admin.horaries.index',
            'principal'     => 1
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionHoraryViews->id,
            'role_id'   => $roleAdmin->id
        ]);

        $actionHoraryUpdate = factory(Action::class)->create([
            'permission_id' => $moduleHoraries->id,
            'name'          => 'Editar Horario',
            'icon'          => 'fas fa-edit',
            'route'         => 'admin.horaries.edit',
            'principal'     => 0
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionHoraryUpdate->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionHoraryDelete = factory(Action::class)->create([
            'permission_id' => $moduleHoraries->id,
            'name'          => 'Borrar Horario',
            'icon'          => 'fas fa-times',
            'route'         => 'admin.horaries.destroy',
            'principal'     => 0
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionHoraryDelete->id,
            'role_id'   => $roleAdmin->id
        ]);
        $modulePenalties = factory(Permission::class)->create([
            'name'         => 'penalties',
            'display_name' => 'Multas',
            'icon'         => 'fas fa-exclamation-triangle',
            'permission_group_id' =>  $permissionGroupStudio->id
        ]);

        $roleSuperRepo->attachToPermission($modulePenalties);
        // Acciones Módulo Multas
        $actionPenaltyViews = factory(Action::class)->create([
            'permission_id' => $modulePenalties->id,
            'name'          => 'Ver Multas',
            'icon'          => 'fas fa-eye',
            'route'         => 'admin.penalties.index',
            'principal'     => 1
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionPenaltyViews->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionPenaltyCreate = factory(Action::class)->create([
            'permission_id' => $modulePenalties->id,
            'name'          => 'Crear Multa',
            'icon'          => 'fas fa-plus',
            'route'         => 'admin.penalties.create',
            'principal'     => 1
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionPenaltyCreate->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionPenaltyUpdate = factory(Action::class)->create([
            'permission_id' => $modulePenalties->id,
            'name'          => 'Editar Multa',
            'icon'          => 'fas fa-edit',
            'route'         => 'admin.penalties.edit',
            'principal'     => 0
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionPenaltyUpdate->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionPenaltyDelete = factory(Action::class)->create([
            'permission_id' => $modulePenalties->id,
            'name'          => 'Borrar Multa',
            'icon'          => 'fas fa-times',
            'route'         => 'admin.penalties.destroy',
            'principal'     => 0
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionPenaltyDelete->id,
            'role_id'   => $roleAdmin->id
        ]);
        $modulePlatforms = factory(Permission::class)->create([
            'name'         => 'platforms',
            'display_name' => 'Plataformas',
            'icon'         => 'ni ni-collection',
            'permission_group_id' =>  $permissionGroupStudio->id
        ]);
        $roleSuperRepo->attachToPermission($modulePlatforms);
        // Acciones Módulo Multas
        $actionPlatformViews = factory(Action::class)->create([
            'permission_id' => $modulePlatforms->id,
            'name'          => 'Ver Plataformas',
            'icon'          => 'fas fa-eye',
            'route'         => 'admin.platforms.index',
            'principal'     => 1
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionPlatformViews->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionPlatformCreate = factory(Action::class)->create([
            'permission_id' => $modulePlatforms->id,
            'name'          => 'Crear Plataforma',
            'icon'          => 'fas fa-plus',
            'route'         => 'admin.platforms.create',
            'principal'     => 1
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionPlatformCreate->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionPlatformUpdate = factory(Action::class)->create([
            'permission_id' => $modulePlatforms->id,
            'name'          => 'Editar Plataforma',
            'icon'          => 'fas fa-edit',
            'route'         => 'admin.platforms.edit',
            'principal'     => 0
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionPlatformUpdate->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionPlatformDelete = factory(Action::class)->create([
            'permission_id' => $modulePlatforms->id,
            'name'          => 'Borrar Plataforma',
            'icon'          => 'fas fa-times',
            'route'         => 'admin.platforms.destroy',
            'principal'     => 0
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionPlatformDelete->id,
            'role_id'   => $roleAdmin->id
        ]);

        $moduleRooms = factory(Permission::class)->create([
            'name'         => 'rooms',
            'display_name' => 'Salas',
            'icon'         => 'fab fa-intercom',
            'permission_group_id' =>  $permissionGroupStudio->id
        ]);
        $roleSuperRepo->attachToPermission($moduleRooms);
        // Acciones Módulo Salas

        $actionRoomViews = factory(Action::class)->create([
            'permission_id' => $moduleRooms->id,
            'name'          => 'Ver Salas',
            'icon'          => 'fas fa-eye',
            'route'         => 'admin.rooms.index',
            'principal'     => 1
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionRoomViews->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionRoomCreate = factory(Action::class)->create([
            'permission_id' => $moduleRooms->id,
            'name'          => 'Crear Sala',
            'icon'          => 'fas fa-plus',
            'route'         => 'admin.rooms.create',
            'principal'     => 1
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionRoomCreate->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionRoomUpdate = factory(Action::class)->create([
            'permission_id' => $moduleRooms->id,
            'name'          => 'Editar Sala',
            'icon'          => 'fas fa-edit',
            'route'         => 'admin.rooms.edit',
            'principal'     => 0
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionRoomUpdate->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionRoomDelete = factory(Action::class)->create([
            'permission_id' => $moduleRooms->id,
            'name'          => 'Borrar Sala',
            'icon'          => 'fas fa-times',
            'route'         => 'admin.rooms.destroy',
            'principal'     => 0
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionRoomDelete->id,
            'role_id'   => $roleAdmin->id
        ]);

        $moduleShifts = factory(Permission::class)->create([
            'name'         => 'shifts',
            'display_name' => 'Turnos',
            'icon'         => 'ni ni-calendar-grid-58',
            'permission_group_id' =>  $permissionGroupStudio->id
        ]);
        $roleSuperRepo->attachToPermission($moduleShifts);
        // Acciones Módulo Salas
        $actionShiftViews = factory(Action::class)->create([
            'permission_id' => $moduleShifts->id,
            'name'          => 'Ver Turnos',
            'icon'          => 'fas fa-eye',
            'route'         => 'admin.shifts.index',
            'principal'     => 1
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionShiftViews->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionShiftCreate = factory(Action::class)->create([
            'permission_id' => $moduleShifts->id,
            'name'          => 'Crear Turno',
            'icon'          => 'fas fa-plus',
            'route'         => 'admin.shifts.create',
            'principal'     => 1
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionShiftCreate->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionShiftUpdate = factory(Action::class)->create([
            'permission_id' => $moduleShifts->id,
            'name'          => 'Editar Turno',
            'icon'          => 'fas fa-edit',
            'route'         => 'admin.shifts.edit',
            'principal'     => 0
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionShiftUpdate->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionShiftDelete = factory(Action::class)->create([
            'permission_id' => $moduleShifts->id,
            'name'          => 'Borrar Turno',
            'icon'          => 'fas fa-times',
            'route'         => 'admin.shifts.destroy',
            'principal'     => 0
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionShiftDelete->id,
            'role_id'   => $roleAdmin->id
        ]);

        $moduleTasks = factory(Permission::class)->create([
            'name'         => 'taks',
            'display_name' => 'Tareas',
            'icon'         => 'ni ni-archive-2',
            'permission_group_id' =>  $permissionGroupStudio->id
        ]);
        $roleSuperRepo->attachToPermission($moduleTasks);
        // Acciones Módulo Tareas
        $actionTaskViews = factory(Action::class)->create([
            'permission_id' => $moduleTasks->id,
            'name'          => 'Ver Tareas',
            'icon'          => 'fas fa-eye',
            'route'         => 'admin.tasks.index',
            'principal'     => 1
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionTaskViews->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionTaskCreate = factory(Action::class)->create([
            'permission_id' => $moduleTasks->id,
            'name'          => 'Crear Tarea',
            'icon'          => 'fas fa-plus',
            'route'         => 'admin.tasks.create',
            'principal'     => 1
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionTaskCreate->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionTaskUpdate = factory(Action::class)->create([
            'permission_id' => $moduleTasks->id,
            'name'          => 'Editar Tarea',
            'icon'          => 'fas fa-edit',
            'route'         => 'admin.tasks.edit',
            'principal'     => 0
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionTaskUpdate->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionTaskDelete = factory(Action::class)->create([
            'permission_id' => $moduleTasks->id,
            'name'          => 'Borrar Tarea',
            'icon'          => 'fas fa-times',
            'route'         => 'admin.tasks.destroy',
            'principal'     => 0
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionTaskDelete->id,
            'role_id'   => $roleAdmin->id
        ]);

        $moduleInformationNews = factory(Permission::class)->create([
            'name'         => 'information_news',
            'display_name' => 'Noticias',
            'icon'         => 'ni ni-tv-2',
            'permission_group_id' =>  $permissionGroupStudio->id
        ]);
        $roleSuperRepo->attachToPermission($moduleInformationNews);
        // Acciones Módulo Noticias
        $actionInformationNewsViews = factory(Action::class)->create([
            'permission_id' => $moduleInformationNews->id,
            'name'          => 'Ver Noticias',
            'icon'          => 'fas fa-eye',
            'route'         => 'admin.news.index',
            'principal'     => 1
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionInformationNewsViews->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionInformationNewCreate = factory(Action::class)->create([
            'permission_id' => $moduleInformationNews->id,
            'name'          => 'Crear Noticia',
            'icon'          => 'fas fa-plus',
            'route'         => 'admin.news.create',
            'principal'     => 1
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionInformationNewCreate->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionInformationNewUpdate = factory(Action::class)->create([
            'permission_id' => $moduleInformationNews->id,
            'name'          => 'Editar Noticia',
            'icon'          => 'fas fa-edit',
            'route'         => 'admin.news.edit',
            'principal'     => 0
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionInformationNewUpdate->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionInformationNewDelete = factory(Action::class)->create([
            'permission_id' => $moduleInformationNews->id,
            'name'          => 'Borrar Noticia',
            'icon'          => 'fas fa-times',
            'route'         => 'admin.news.destroy',
            'principal'     => 0
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionInformationNewDelete->id,
            'role_id'   => $roleAdmin->id
        ]);

        $moduleTrainings = factory(Permission::class)->create([
            'name'         => 'trainings',
            'display_name' => 'Capacitaciones',
            'icon'         => 'ni ni-archive-2',
            'permission_group_id' =>  $permissionGroupStudio->id
        ]);
        $roleSuperRepo->attachToPermission($moduleTrainings);
        // Acciones Módulo Capacitaciones
        $actionTrainingViews = factory(Action::class)->create([
            'permission_id' => $moduleTrainings->id,
            'name'          => 'Ver Capacitaciones',
            'icon'          => 'fas fa-eye',
            'route'         => 'admin.trainings.index',
            'principal'     => 1
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionTrainingViews->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionTrainingCreate = factory(Action::class)->create([
            'permission_id' => $moduleTrainings->id,
            'name'          => 'Crear Capacitación',
            'icon'          => 'fas fa-plus',
            'route'         => 'admin.trainings.create',
            'principal'     => 1
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionTrainingCreate->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionTrainingUpdate = factory(Action::class)->create([
            'permission_id' => $moduleTrainings->id,
            'name'          => 'Editar Capacitación',
            'icon'          => 'fas fa-edit',
            'route'         => 'admin.trainings.edit',
            'principal'     => 0
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionTrainingUpdate->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionTrainingDelete = factory(Action::class)->create([
            'permission_id' => $moduleTrainings->id,
            'name'          => 'Borrar Capacitación',
            'icon'          => 'fas fa-times',
            'route'         => 'admin.trainings.destroy',
            'principal'     => 0
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionTrainingDelete->id,
            'role_id'   => $roleAdmin->id
        ]);

        $moduleDiscounts = factory(Permission::class)->create([
            'name'         => 'discounts',
            'display_name' => 'Descuentos',
            'icon'         => 'ni ni-credit-card',
            'permission_group_id' =>  $permissionGroupStudio->id
        ]);
        $roleSuperRepo->attachToPermission($moduleDiscounts);
        // Acciones Módulo Descuentos
        $actionDiscountViews = factory(Action::class)->create([
            'permission_id' => $moduleDiscounts->id,
            'name'          => 'Ver Descuentos',
            'icon'          => 'fas fa-eye',
            'route'         => 'admin.discounts.index',
            'principal'     => 1
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionDiscountViews->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionDiscountCreate = factory(Action::class)->create([
            'permission_id' => $moduleDiscounts->id,
            'name'          => 'Crear Descuento',
            'icon'          => 'fas fa-plus',
            'route'         => 'admin.discounts.create',
            'principal'     => 1
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionDiscountCreate->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionDiscountUpdate = factory(Action::class)->create([
            'permission_id' => $moduleDiscounts->id,
            'name'          => 'Editar Descuento',
            'icon'          => 'fas fa-edit',
            'route'         => 'admin.discounts.edit',
            'principal'     => 0
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionDiscountUpdate->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionDiscountDelete = factory(Action::class)->create([
            'permission_id' => $moduleDiscounts->id,
            'name'          => 'Borrar Descuento',
            'icon'          => 'fas fa-times',
            'route'         => 'admin.discounts.destroy',
            'principal'     => 0
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionDiscountDelete->id,
            'role_id'   => $roleAdmin->id
        ]);


        $pendingTasks = factory(Permission::class)->create([
            'name'         => 'pending-tasks',
            'display_name' => 'Tareas Pendientes',
            'icon'         => 'ni ni-books',
            'permission_group_id' =>  $permissionGroupStudio->id
        ]);

        $roleLeaderRepo->attachToPermission($pendingTasks);
        $roleManagerRepo->attachToPermission($pendingTasks);
        $roleSuperRepo->attachToPermission($pendingTasks);

        // Acciones Módulo Tareas Pendientes
        $actionTaskViews = factory(Action::class)->create([
            'permission_id' => $pendingTasks->id,
            'name'          => 'Ver Tareas Pendientes',
            'icon'          => 'fas fa-eye',
            'route'         => 'admin.pending-tasks.index',
            'principal'     => 1
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionTaskViews->id,
            'role_id'   => $roleAdmin->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionTaskViews->id,
            'role_id'   => $roleManager->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionTaskViews->id,
            'role_id'   => $roleLeader->id
        ]);

        $actionCommentCreate = factory(Action::class)->create([
            'permission_id' => $pendingTasks->id,
            'name'          => 'Agregar Comentario',
            'icon'          => 'fas fa-plus',
            'route'         => 'admin.pending-tasks.edit',
            'principal'     => 0
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionCommentCreate->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionCommentCreate->id,
            'role_id'   => $roleManager->id
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionCommentCreate->id,
            'role_id'   => $roleLeader->id
        ]);
        $actionTaskComplete = factory(Action::class)->create([
            'permission_id' => $pendingTasks->id,
            'name'          => 'Completar Tarea',
            'icon'          => 'ni ni-check-bold',
            'route'         => 'admin.pending-tasks.destroy',
            'principal'     => 0
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionTaskComplete->id,
            'role_id'   => $roleAdmin->id
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionTaskComplete->id,
            'role_id'   => $roleManager->id
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionTaskComplete->id,
            'role_id'   => $roleLeader->id
        ]);

        $employeeAccounts = factory(Permission::class)->create([
            'name'         => 'employee-accounts',
            'display_name' => 'Cuentas de Modelos',
            'icon'         => 'fas fa-file-invoice-dollar',
            'permission_group_id' =>  $permissionGroupStudio->id
        ]);

        $roleLeaderRepo->attachToPermission($employeeAccounts);
        $roleSuperRepo->attachToPermission($employeeAccounts);
        $roleManagerRepo->attachToPermission($employeeAccounts);

        // Acciones Módulo Cuentas de Modelo
        $actionEmployeeAccountViews = factory(Action::class)->create([
            'permission_id' => $employeeAccounts->id,
            'name'          => 'Ver Cuentas',
            'icon'          => 'fas fa-eye',
            'route'         => 'admin.employee-platform-accounts.index',
            'principal'     => 1
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionEmployeeAccountViews->id,
            'role_id'   => $roleAdmin->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionEmployeeAccountViews->id,
            'role_id'   => $roleManager->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionEmployeeAccountViews->id,
            'role_id'   => $roleLeader->id
        ]);

        $actionEmployeeAccountCreate = factory(Action::class)->create([
            'permission_id' => $employeeAccounts->id,
            'name'          => 'Crear Cuenta',
            'icon'          => 'fas fa-plus',
            'route'         => 'admin.employee-platform-accounts.create',
            'principal'     => 1
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionEmployeeAccountCreate->id,
            'role_id'   => $roleAdmin->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionEmployeeAccountCreate->id,
            'role_id'   => $roleManager->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionEmployeeAccountCreate->id,
            'role_id'   => $roleLeader->id
        ]);

        $actionEmployeeAccountUpdate = factory(Action::class)->create([
            'permission_id' => $employeeAccounts->id,
            'name'          => 'Editar Cuenta',
            'icon'          => 'fas fa-edit',
            'route'         => 'admin.employee-platform-accounts.edit',
            'principal'     => 0
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionEmployeeAccountUpdate->id,
            'role_id'   => $roleAdmin->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionEmployeeAccountUpdate->id,
            'role_id'   => $roleManager->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionEmployeeAccountUpdate->id,
            'role_id'   => $roleLeader->id
        ]);

        $actionEmployeeAccountDelete = factory(Action::class)->create([
            'permission_id' => $employeeAccounts->id,
            'name'          => 'Borrar Cuenta',
            'icon'          => 'fas fa-times',
            'route'         => 'admin.employee-platform-accounts.destroy',
            'principal'     => 0
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionEmployeeAccountDelete->id,
            'role_id'   => $roleAdmin->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionEmployeeAccountDelete->id,
            'role_id'   => $roleManager->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionEmployeeAccountDelete->id,
            'role_id'   => $roleLeader->id
        ]);


        $sendingPhotos = factory(Permission::class)->create([
            'name'         => 'photos',
            'display_name' => 'Fotos Modelos',
            'icon'         => 'far fa-images',
            'permission_group_id' =>  $permissionGroupStudio->id
        ]);

        $roleManagerRepo->attachToPermission($sendingPhotos);
        $roleLeaderRepo->attachToPermission($sendingPhotos);
        $roleSuperRepo->attachToPermission($sendingPhotos);

        // Acciones Módulo Envio de fotos
        $actionSendingPhotosViews = factory(Action::class)->create([
            'permission_id' => $sendingPhotos->id,
            'name'          => 'Ver Fotos Modelos',
            'icon'          => 'fas fa-eye',
            'route'         => 'admin.sending-photos.index',
            'principal'     => 1
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionSendingPhotosViews->id,
            'role_id'   => $roleAdmin->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionSendingPhotosViews->id,
            'role_id'   => $roleManager->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionSendingPhotosViews->id,
            'role_id'   => $roleLeader->id
        ]);

        $actionSendingPhotoShow = factory(Action::class)->create([
            'permission_id' => $sendingPhotos->id,
            'name'          => 'Ver fotos',
            'icon'          => 'fas fa-search-plus',
            'route'         => 'admin.sending-photos.show',
            'principal'     => 0
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionSendingPhotoShow->id,
            'role_id'   => $roleAdmin->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionSendingPhotoShow->id,
            'role_id'   => $roleManager->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionSendingPhotoShow->id,
            'role_id'   => $roleLeader->id
        ]);

        $scores = factory(Permission::class)->create([
            'name'         => 'score',
            'display_name' => 'Puntuación Modelos',
            'icon'         => 'ni ni-diamond',
            'permission_group_id' =>  $permissionGroupStudio->id
        ]);

        $roleManagerRepo->attachToPermission($scores);
        $roleLeaderRepo->attachToPermission($scores);
        $roleSuperRepo->attachToPermission($scores);

        // Acciones Módulo Puntuacion Modelos
        $actionScoreViews = factory(Action::class)->create([
            'permission_id' => $scores->id,
            'name'          => 'Ver Puntuación Modelos',
            'icon'          => 'fas fa-eye',
            'route'         => 'admin.scores.index',
            'principal'     => 1
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionScoreViews->id,
            'role_id'   => $roleAdmin->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionScoreViews->id,
            'role_id'   => $roleManager->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionScoreViews->id,
            'role_id'   => $roleLeader->id
        ]);

        $actionScoreCreate = factory(Action::class)->create([
            'permission_id' => $scores->id,
            'name'          => 'Crear Puntuación Modelo',
            'icon'          => 'fas fa-plus',
            'route'         => 'admin.scores.create',
            'principal'     => 1
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionScoreCreate->id,
            'role_id'   => $roleAdmin->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionScoreCreate->id,
            'role_id'   => $roleManager->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionScoreCreate->id,
            'role_id'   => $roleLeader->id
        ]);

        $actionScoreUpdate = factory(Action::class)->create([
            'permission_id' => $scores->id,
            'name'          => 'Editar Puntuación',
            'icon'          => 'fas fa-edit',
            'route'         => 'admin.scores.edit',
            'principal'     => 0
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionScoreUpdate->id,
            'role_id'   => $roleAdmin->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionScoreUpdate->id,
            'role_id'   => $roleManager->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionScoreUpdate->id,
            'role_id'   => $roleLeader->id
        ]);

        $actionScoreDelete = factory(Action::class)->create([
            'permission_id' => $scores->id,
            'name'          => 'Borrar Puntuación',
            'icon'          => 'fas fa-times',
            'route'         => 'admin.scores.destroy',
            'principal'     => 0
        ]);
        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionScoreDelete->id,
            'role_id'   => $roleAdmin->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionScoreDelete->id,
            'role_id'   => $roleManager->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionScoreDelete->id,
            'role_id'   => $roleLeader->id
        ]);

        $actionScoreShow = factory(Action::class)->create([
            'permission_id' => $scores->id,
            'name'          => 'Ver Puntuación',
            'icon'          => 'fas fa-search-plus',
            'route'         => 'admin.scores.show',
            'principal'     => 0
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionScoreShow->id,
            'role_id'   => $roleAdmin->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionScoreShow->id,
            'role_id'   => $roleManager->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionScoreShow->id,
            'role_id'   => $roleLeader->id
        ]);

        $adminSales = factory(Permission::class)->create([
            'name'         => 'admin-sales',
            'display_name' => 'Ventas - Admin',
            'icon'         => 'fas fa-cash-register',
            'permission_group_id' =>  $permissionGroupStudio->id
        ]);

        $roleSuperRepo->attachToPermission($adminSales);

        // Acciones Modulo Ventas admin

        $actionSaleViews = factory(Action::class)->create([
            'permission_id' => $adminSales->id,
            'name'          => 'Ver Ventas',
            'icon'          => 'fas fa-eye',
            'route'         => 'admin.admin-sales.index',
            'principal'     => 1
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionSaleViews->id,
            'role_id'   => $roleAdmin->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionSaleViews->id,
            'role_id'   => $roleManager->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionSaleViews->id,
            'role_id'   => $roleLeader->id
        ]);

        $sales = factory(Permission::class)->create([
            'name'         => 'sales',
            'display_name' => 'Ventas',
            'icon'         => 'fas fa-cash-register',
            'permission_group_id' =>  $permissionGroupStudio->id
        ]);

        $roleManagerRepo->attachToPermission($sales);
        $roleLeaderRepo->attachToPermission($sales);
        $roleSuperRepo->attachToPermission($sales);

        // Acciones Módulo Ventas
        $actionSaleViews = factory(Action::class)->create([
            'permission_id' => $sales->id,
            'name'          => 'Ver Ventas',
            'icon'          => 'fas fa-eye',
            'route'         => 'admin.sales.index',
            'principal'     => 1
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionSaleViews->id,
            'role_id'   => $roleAdmin->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionSaleViews->id,
            'role_id'   => $roleManager->id
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionSaleViews->id,
            'role_id'   => $roleLeader->id
        ]);


        $proofPayments = factory(Permission::class)->create([
            'name'         => 'proof_payments',
            'display_name' => 'Comprobantes de Pago',
            'icon'         => 'fas fa-wallet',
            'permission_group_id' =>  $permissionGroupStudio->id
        ]);

        $roleSuperRepo->attachToPermission($proofPayments);

        // Acciones Módulo Comprobantes de pago
        $actionProofPaymentView = factory(Action::class)->create([
            'permission_id' => $proofPayments->id,
            'name'          => 'Ver Comprobantes de Pago',
            'icon'          => 'fas fa-eye',
            'route'         => 'admin.proof-payments.index',
            'principal'     => 1
        ]);

        $actionRole = factory(ActionRole::class)->create([
            'action_id' => $actionProofPaymentView->id,
            'role_id'   => $roleAdmin->id
        ]);
    }
}
