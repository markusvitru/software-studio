<?php

use Faker\Generator as Faker;
use Modules\Studio\Entities\Horaries\Horary;

$factory->define(Horary::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
    ];
});
