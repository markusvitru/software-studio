<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scores', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('employee_id');
            $table->foreign('employee_id')->references('id')->on('employees')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer('personal_presentation');
            $table->integer('clothing');
            $table->integer('hair');
            $table->integer('hand_nails');
            $table->integer('toe_nails');
            $table->integer('makeup');
            $table->integer('body_expression');
            $table->integer('oral_expression');
            $table->tinyInteger('have_social_networks')->default(0)->comment('0: No, 1: Si');
            $table->text('observation');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scores');
    }
}
