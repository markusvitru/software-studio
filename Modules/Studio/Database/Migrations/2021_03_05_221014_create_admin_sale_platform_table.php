<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminSalePlatformTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_sale_platform', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('platform_id');
            $table->foreign('platform_id')->references('id')->on('platforms')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->unsignedInteger('admin_sale_id');
            $table->foreign('admin_sale_id')->references('id')->on('admin_sales')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer('total_token');
            $table->float('total_cost', 13, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_sales_platform');
    }
}
