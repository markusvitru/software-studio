<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlatformSaleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('platform_sale', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('platform_id');
            $table->foreign('platform_id')->references('id')->on('platforms')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->unsignedInteger('sale_id');
            $table->foreign('sale_id')->references('id')->on('sales')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer('total_token');
            $table->float('total_cost', 13, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('platform_sale');
    }
}
