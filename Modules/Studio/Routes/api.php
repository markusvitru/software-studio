<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/studio', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'admin/studio', 'as' => 'admin.','middleware' => ['cors']], function () {
    Route::namespace('Admin')->group(function () {
        Route::namespace('InformationNews')->group(function () {
            Route::get('news', 'InformationNewController@getNewsForApp')->name('news.newsApp');
        });

        Route::namespace('Trainings')->group(function () {
            Route::get('trainings', 'TrainingController@getTraingnsForApp');
            Route::put('training/{id}', 'TrainingController@trainingToEmployee');
        });

        Route::namespace('Goals')->group(function () {
            Route::get('goal/{employee_id}', 'GoalController@getGoalForEmployee');
        });

        Route::namespace('ProofPayments')->group(function () {
            Route::get('proof-payment/{employee_id}', 'ProofPaymentController@getProofPaymentForEmployee');
        });

        Route::namespace('SendingPhotos')->group(function () {
            Route::post('sending-photos', 'SendingPhotoController@storePhotos');
        });
    });
});
