<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin/studio', 'middleware' => ['employee'], 'as' => 'admin.'], function () {
    Route::namespace('Admin')->group(function () {
        Route::namespace('Discounts')->group(function () {
            Route::resource('discounts', 'DiscountController');
        });

        Route::namespace('Goals')->group(function () {
            Route::resource('goals', 'GoalController');
        });

        Route::namespace('Horaries')->group(function () {
            Route::resource('horaries', 'HoraryController');
        });

        Route::namespace('LoginLogs')->group(function () {
            Route::resource('login-logs', 'LoginLogController');
        });

        Route::namespace('Penalties')->group(function () {
            Route::resource('penalties', 'PenaltyController');
        });

        Route::namespace('Photos')->group(function () {
            Route::resource('photos', 'PhotoController');
        });

        Route::namespace('Platforms')->group(function () {
            Route::resource('platforms', 'PlatformController');
        });

        Route::namespace('ProofPayments')->group(function () {
            Route::resource('proof-payments', 'ProofPaymentController');
            Route::get('/generate/proof-payments/{subsidiary_id}', 'ProofPaymentController@generateProofPayments');
        });

        Route::namespace('Rooms')->group(function () {
            Route::resource('rooms', 'RoomController');
        });

        Route::namespace('Sales')->group(function () {
            Route::resource('sales', 'SaleController')->except([
                'create'
            ]);
            Route::get('sales/create/{employee_id}', 'SaleController@create')->name('sales.create');
        });

        Route::namespace('AdminSales')->group(function () {
            Route::resource('admin-sales', 'AdminSaleController')->except([
                'create'
            ]);
            Route::get('admin-sales/create/{employee_id}', 'AdminSaleController@create')->name('sales.create');
        });

        Route::namespace('Scores')->group(function () {
            Route::resource('scores', 'ScoreController');
        });

        Route::namespace('SendingPhotos')->group(function () {
            Route::resource('sending-photos', 'SendingPhotoController');
        });

        Route::namespace('Shifts')->group(function () {
            Route::resource('shifts', 'ShiftController');
        });

        Route::namespace('Tasks')->group(function () {
            Route::resource('tasks', 'TaskController');
        });

        Route::namespace('EmployeePlatformAccounts')->group(function () {
            Route::resource('employee-platform-accounts', 'EmployeePlatformAccountController');
        });

        Route::namespace('PendingTasks')->group(function () {
            Route::resource('pending-tasks', 'PendingTaskController');
            Route::put('pending-tasks/{task}/complete', 'PendingTaskController@completeTask')->name('pending-task.task_complete');
        });

        Route::namespace('Trainings')->group(function () {
            Route::resource('trainings', 'TrainingController');
        });

        Route::namespace('InformationNews')->group(function () {
            Route::resource('news', 'InformationNewController');
        });
    });
});
