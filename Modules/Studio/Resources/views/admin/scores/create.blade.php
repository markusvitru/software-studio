@extends('generals::layouts.admin.app')
@section('header')
<div class="header pb-2">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item " active aria-current="page">Puntuaciòn Modelos</li>
                            <li class="breadcrumb-item active">Crear Puntuación</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<section class="content">
    @include('generals::layouts.errors-and-messages')
    <div class="card">
        <form action="{{ route('admin.scores.store') }}" method="post" class="form">
            <div class="card-body">
                @csrf
                <div class="col pl-0 mb-3">
                    <h2>Crear Puntuación</h2>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-6 form-group">
                        <label for="employee_id">Modelo</label>
                        <select class="form-control" name="employee_id" id="employee_id" class="form-control">
                            @foreach ($employees as $employee)
                                <option value="{{ $employee['id'] }}">{{ $employee['label'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-12 col-sm-6 form-group">
                        <label for="personal_presentation">Presentación Personal</label>
                        <select class="form-control" name="personal_presentation" id="personal_presentation">
                            @for ($i = 5; $i <= 100; $i = $i+5)
                                <option value="{{ $i }}"> {{ $i }} % </option>
                            @endfor
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-6 form-group">
                        <label for="clothing">Vestuario</label>
                        <select class="form-control" name="clothing" id="clothing">
                            @for ($i = 5; $i <= 100; $i = $i+5)
                                <option value="{{ $i }}"> {{ $i }} % </option>
                            @endfor
                        </select>
                    </div>
                    <div class="col-12 col-sm-6 form-group">
                        <label for="hair">Cabello</label>
                        <select class="form-control" name="hair" id="hair">
                            @for ($i = 5; $i <= 100; $i = $i+5)
                                <option value="{{ $i }}"> {{ $i }} % </option>
                            @endfor
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-6 form-group">
                        <label for="hand_nails">Uñas de las manos</label>
                        <select class="form-control" name="hand_nails" id="hand_nails">
                            @for ($i = 5; $i <= 100; $i = $i+5)
                                <option value="{{ $i }}"> {{ $i }} % </option>
                            @endfor
                        </select>
                    </div>
                    <div class="col-12 col-sm-6 form-group">
                        <label for="toe_nails">Uñas de los pies</label>
                        <select class="form-control" name="toe_nails" id="toe_nails">
                            @for ($i = 5; $i <= 100; $i = $i+5)
                                <option value="{{ $i }}"> {{ $i }} % </option>
                            @endfor
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-6 form-group">
                        <label for="makeup">Maquillaje</label>
                        <select class="form-control" name="makeup" id="makeup">
                            @for ($i = 5; $i <= 100; $i = $i+5)
                                <option value="{{ $i }}"> {{ $i }} % </option>
                            @endfor
                        </select>
                    </div>
                    <div class="col-12 col-sm-6 form-group">
                        <label for="body_expression">Expresión corporal</label>
                        <select class="form-control" name="body_expression" id="body_expression">
                            @for ($i = 5; $i <= 100; $i = $i+5)
                                <option value="{{ $i }}"> {{ $i }} % </option>
                            @endfor
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-6 form-group">
                        <label for="oral_expression">Expresión oral</label>
                        <select class="form-control" name="oral_expression" id="oral_expression">
                            @for ($i = 5; $i <= 100; $i = $i+5)
                                <option value="{{ $i }}"> {{ $i }} % </option>
                            @endfor
                        </select>
                    </div>
                    <div class="col-12 col-sm-6 form-group">
                        <label for="have_social_networks">Tiene redes sociales</label>
                        <select class="form-control" name="have_social_networks" id="have_social_networks">
                            <option value="1">Si</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                </div>
                <div class="row col-12 form-group">
                    <label for="observation">Observación</label>
                    <textarea class="form-control" name="observation" id="observation" cols="10" rows="5"></textarea>
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-primary btn-sm">Crear</button>
                <a href="{{ route('admin.scores.index') }}" class="btn btn-default btn-sm">Regresar</a>
            </div>
        </form>
    </div>
</section>
@endsection
