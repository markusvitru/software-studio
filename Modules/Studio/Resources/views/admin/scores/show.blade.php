@extends('generals::layouts.admin.app')
@section('header')
<div class="header pb-2">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('admin.scores.index') }}">Puntuación Modelo</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{$score->employee->name}}
                                {{$score->employee->last_name}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<section class="content">
    @include('generals::layouts.errors-and-messages')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <h3 class="mb-0">Puntuación - {{$score->employee->name}} {{$score->employee->last_name}}</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-6 form-group">
                    <label for="personal_presentation">Presentación personal</label>
                    <input type="text" disabled="true" value="{{ $score->personal_presentation.'%' }}" class="form-control">
                </div>
                <div class="col-12 col-sm-6 form-group">
                    <label for="clothing">Vestuario</label>
                    <input type="text" value="{{ $score->clothing.'%' }}" disabled="true" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-6 form-group">
                    <label for="clothing">Cabello</label>
                    <input type="text" disabled="true" value="{{ $score->clothing.'%' }}" class="form-control">
                </div>
                <div class="col-12 col-sm-6 form-group">
                    <label for="hand_nails">Uñas de las manos</label>
                    <input type="text" disabled="true" value="{{ $score->hand_nails.'%' }}" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-6 form-group">
                    <label for="toe_nails">Uñas de los pies</label>
                    <input type="text" disabled="true" value="{{ $score->toe_nails.'%' }}" class="form-control"></div>
                <div class="col-12 col-sm-6 form-group">
                    <label for="makeup">Maquillaje</label>
                    <input type="text" value="{{ $score->makeup.'%' }}" disabled="true" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-6 form-group">
                    <label for="body_expression">Expresión corporal</label>
                    <input type="text" disabled="true" value="{{ $score->body_expression.'%' }}" class="form-control">
                </div>
                <div class="col-12 col-sm-6 form-group">
                    <label for="oral_expression">Expresión oral</label>
                    <input type="text" value="{{ $score->oral_expression.'%' }}" disabled="true" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-6 form-group">
                    <label for="have_social_networks">Tiene redes sociales</label>
                    <input type="text" value="{{ $score->have_social_networks }}" disabled="true" class="form-control">
                </div>
                <div class="col-12 col-sm-6 form-group">
                    <label for="created_at">Fecha</label>
                    <input type="text" value="{{ $score->created_at }}" disabled="true" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-12 form-group">
                    <label for="observation">Observación</label>
                    <textarea class="form-control" name="observation" disabled="true" id="observation" cols="30" rows="5">{{ $score->observation }}</textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <a href="{{ route('admin.scores.index') }}" class="btn btn-default btn-sm">Regresar</a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
