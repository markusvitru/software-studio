@extends('generals::layouts.admin.app')
@section('header')
<div class="header pb-2">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item " active aria-current="page">Plataformas</li>
                            <li class="breadcrumb-item active">Crear Plataforma</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<section class="content">
    @include('generals::layouts.errors-and-messages')
    <div class="card">
        <form action="{{ route('admin.platforms.store') }}" method="post" class="form">
            <div class="card-body">
                @csrf
                <div class="col pl-0 mb-3">
                    <h2>Crear Plataforma</h2>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-12">
                        <label for="name">Nombre</label>
                        <input class="form-control" type="text" id="name" name="name" />
                    </div>
                    <div class="col-sm-6 col-12 form-group">
                        <label for="type">Tipo</label>
                        <select name="type" id="type" class="form-control">
                            <option value="0">Dolar</option>
                            <option value="1">Token</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-12">
                        <label for="token_cost">Valor del token</label>
                        <input class="form-control" type="number" step="any" id="token_cost" name="token_cost" />
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-primary btn-sm">Crear</button>
                <a href="{{ route('admin.platforms.index') }}" class="btn btn-default btn-sm">Regresar</a>
            </div>
        </form>
    </div>
</section>
@endsection
