@extends('generals::layouts.admin.app')
@section('header')
<div class="header pb-2">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('admin.sending-photos.index') }}">Fotos</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{$sendingPhoto->employee->name}}
                                {{$sendingPhoto->employee->last_name}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<section class="content">
    @include('generals::layouts.errors-and-messages')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <h3 class="mb-0">Fotos</h3>
                </div>
            </div>
            <div class="row">
                @foreach ($sendingPhoto->photos as $key => $photo)
                    <div class="col-12 col-sm-6 col-md-3 text-center mb-2">
                        <img src="{{ $photo->photo }}" alt="{{ $sendingPhoto->employee->name.$key }}" />
                    </div>
                @endforeach
            </div>
            <div class="row">
                <div class="col-12">
                    <a href="{{ route('admin.sending-photos.index') }}" class="btn btn-default btn-sm">Regresar</a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
