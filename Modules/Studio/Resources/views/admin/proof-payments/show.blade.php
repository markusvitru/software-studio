@extends('generals::layouts.admin.app')
@section('header')
<div class="header pb-2">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('admin.sales.index') }}">Venta</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{$sale->employee->name}}
                                {{$sale->employee->last_name}} - {{ $sale->employee->subsidiary->name }}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<section class="content">
    @include('generals::layouts.errors-and-messages')
    <div class="card">
        <div class="card-body">
            <div class="col pl-0 mb-3">
                <h2>{{ $sale->employee->name . ' ' . $sale->employee->last_name }} - {{ $sale->employee->subsidiary->name }}</h2>
            </div>
            <div class="row">
                <div class="col-12 col-sm-6">
                    <label for="room_id">Sala</label>
                    <input type="text" value="{{ $sale->room->name }}" disabled="true" class="form-control">
                </div>
                <div class="col-12 col-sm-6">
                    <label for="horary_id">Horario</label>
                    <input type="text" value="{{ $sale->horary->time_start . ' - ' . $sale->horary->time_end }}" disabled="true" class="form-control">
                </div>
            </div>
            <hr>
            <div class="row">
                @foreach ($sale->platforms->toArray() as $item)
                    <div class="col-12 col-sm-6 form-group">
                        <label for="platforms[{{ $item['id'] }}]">{{ $item['name'] }}</label>
                        <input value="{{ $item['pivot']['total_cost'] }}" name="platforms[{{ $item['id'] }}]" disabled="true" id="platforms[{{ $item['id'] }}]" type="text" class="form-control">
                    </div>
                @endforeach
            </div>
        </div>
        <div class="card-footer text-right">
            <button type="submit" class="btn btn-primary btn-sm">Crear</button>
            <a href="{{ route('admin.sales.index') }}" class="btn btn-default btn-sm">Regresar</a>
        </div>
    </div>
</section>
@endsection
