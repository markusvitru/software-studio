@extends('generals::layouts.admin.app')
@section('header')
<div class="header pb-2">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active" active aria-current="page">Comprobantes de pago</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<section class="content">
    @include('generals::layouts.errors-and-messages')
    <div class="card">
        @if(!$proofPayments->isEmpty())
        <div class="card-header border-0">
            <h3 class="mb-0">Comprobantes de pago</h3>
            @include('generals::layouts.search', ['route' => route('admin.proof-payments.index')])
        </div>
        <div class="row">
            <div class="col-12 text-right mb-3">
                <a data-toggle="modal" data-target="#addSale" href="" class="btn btn-primary btn-sm"
                    data-toggle="tooltip" data-original-title="Añadir Venta"><i class="fa fa-edit"></i>Generar
                    Comprobantes de Pago</a>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table align-items-center table-flush table-hover">
                <thead class="thead-light">
                    <tr>
                        <th>Id</th>
                        <th>Empleado</th>
                        <th>Total Descuentos</th>
                        <th>Total Multas</th>
                        <th>Total Ventas</th>
                        <th>Total</th>
                        <th>Fecha</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody class="list">
                    @foreach ($proofPayments as $item)
                    <tr>
                        <td>
                            {{ $item['id'] }}
                        </td>
                        <td>
                            {{ $item['employee_id'] }}
                        </td>
                        <td>
                            {{ $item['total_discounts'] }}
                        </td>
                        <td>
                            {{ $item['total_penalties'] }}
                        </td>
                        <td>
                            {{ $item['total_sales'] }}
                        </td>
                        <td>
                            {{ $item['created_at'] }}
                        </td>
                        <td>
                            {{ $item['total'] }}
                        </td>
                        <td>
                            <a href="" class=" table-action table-action" data-toggle="tooltip" data-original-title="">
                                <i class="fas fa-search"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer py-2">
            @include('generals::layouts.admin.pagination.pagination', [$skip])
        </div>
        @else
        <div class="card-header border-0">
            <h3 class="mb-0">Comprobantes de pago</h3>
        </div>
        <div class="card-footer py-2">
            @include('generals::layouts.admin.pagination.pagination_null', [$skip])
        </div>
        @endif
    </div>
</section>

<div class="modal fade" id="addSale" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Añadir Venta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body py-0">
                <div class="row">
                    <div class="col-12 form-group">
                        <label for="subsidiary_id">Sucursal</label>
                        <select class="form-control" name="subsidiary_id" id="subsidiary_id" class="form-control">
                            @foreach ($subsidiaries as $subsidiarie)
                            <option value="{{ $subsidiarie['id'] }}">{{ $subsidiarie['label'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-center mb-3">
                        <button type="button" onclick="redirect()" class="btn btn-primary btn-sm">Crear</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script>
    function redirect() {
            var subsidiary_id = document.getElementById("subsidiary_id").value;
            window.location.href = "/admin/studio/generate/proof-payments/"+subsidiary_id;
        }
</script>
@endsection