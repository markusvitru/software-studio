@extends('generals::layouts.admin.app')
@section('header')
<div class="header pb-2">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active" active aria-current="page">Descuentos</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<section class="content">
    @include('generals::layouts.errors-and-messages')
    <div class="card">
        @if(!$discounts->isEmpty())
        <div class="card-header border-0">
            <h3 class="mb-0">Descuentos</h3>
            @include('generals::layouts.search', ['route' => route('admin.discounts.index'), $searchInputs])
        </div>
        @include('studio::layouts.list',[$headers, $inputs, $routeEdit,'datas' => $discounts])
        <div class="card-footer py-2">
            @include('generals::layouts.admin.pagination.pagination', [$skip])
        </div>
        @else
        <div class="card-header border-0">
            <h3 class="mb-0">Descuentos</h3>
        </div>
        <div class="card-footer py-2">
            @include('generals::layouts.admin.pagination.pagination_null', [$skip])
        </div>
        @endif
    </div>
</section>
@endsection