@extends('generals::layouts.admin.app')
@section('header')
<div class="header pb-2">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item " active aria-current="page">Descuentos</li>
                            <li class="breadcrumb-item active">Crear Descueto</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<section class="content">
    @include('generals::layouts.errors-and-messages')
    <div class="card">
        <form action="{{ route('admin.discounts.store') }}" method="post" class="form">
            <div class="card-body">
                @csrf
                <div class="col pl-0 mb-3">
                    <h2>Crear Descuento</h2>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-12 form-group">
                        <label for="employee_id">Empleado</label>
                        <select name="employee_id" id="employee_id" class="form-control">
                            @foreach ($employees as $employee)
                                <option value="{{ $employee['id'] }}">{{ $employee['label'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-6 col-12">
                        <label for="name">Descuento</label>
                        <input class="form-control" type="text" id="name" name="name" />
                    </div>
                    <div class="col-sm-6 col-12">
                        <label for="cost">Costo</label>
                        <input class="form-control" type="number" id="cost" name="cost" />
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-primary btn-sm">Crear</button>
                <a href="{{ route('admin.discounts.index') }}" class="btn btn-default btn-sm">Regresar</a>
            </div>
        </form>
    </div>
</section>
@endsection
