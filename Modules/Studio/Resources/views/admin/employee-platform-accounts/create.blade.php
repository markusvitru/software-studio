@extends('generals::layouts.admin.app')
@section('header')
<div class="header pb-2">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item " active aria-current="page">Cuentas de Modelos</li>
                            <li class="breadcrumb-item active">Crear Cuenta</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<section class="content">
    @include('generals::layouts.errors-and-messages')
    <div class="card">
        <form action="{{ route('admin.employee-platform-accounts.store') }}" method="post" class="form">
            <div class="card-body">
                @csrf
                <div class="col pl-0 mb-3">
                    <h2>Crear Cuenta</h2>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <label for="employee_id">Modelo</label>
                        <select class="form-control" name="employee_id" id="employee_id">
                            @foreach ($employees as $item)
                                <option value="{{ $item['id'] }}">{{ $item['label'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-12 col-sm-6 form-group">
                        <label for="platform_id">Plataforma</label>
                        <select class="form-control" name="platform_id" id="platform_id">
                            @foreach ($platforms as $item)
                                <option value="{{ $item['id'] }}">{{ $item['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-12 col-sm-6 form-group">
                        <label for="user">Usuario</label>
                        <input type="text" name="user" id="user" class="form-control">
                    </div>
                    <div class="col-12 col-sm-6 form-group">
                        <label for="password">Contraseña</label>
                        <input type="text" name="password" id="password" class="form-control">
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-primary btn-sm">Crear</button>
                <a href="{{ route('admin.employee-platform-accounts.index') }}" class="btn btn-default btn-sm">Regresar</a>
            </div>
        </form>
    </div>
</section>
@endsection
