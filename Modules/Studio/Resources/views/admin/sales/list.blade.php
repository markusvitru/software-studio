@extends('generals::layouts.admin.app')
@section('header')
<div class="header pb-2">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active" active aria-current="page">Ventas</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<section class="content">
    @include('generals::layouts.errors-and-messages')
    <div class="card">
        @if ($sales->toArray())
        <div class="card-header border-0">
            <h3 class="mb-0">Ventas</h3>
            @include('generals::layouts.search', ['route' => route('admin.sales.index')])
        </div>
        <div class="row mx-0">
            <div class="col-12 text-right mb-3">
                <a data-toggle="modal" data-target="#addSale" href="" class="btn btn-primary btn-sm"
                    data-toggle="tooltip" data-original-title="Añadir Venta"><i class="fa fa-edit"></i>Agregar Venta</a>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table align-items-center table-flush table-hover">
                <thead class="thead-light">
                    <tr>
                        <th>Id</th>
                        <th>Empleado</th>
                        <th>Sucursal</th>
                        <th>Sala</th>
                        <th>Horario</th>
                        <th>Total</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody class="list">
                    @foreach ($sales as $item)
                    <tr>
                        <td>
                            {{ $item['id'] }}
                        </td>
                        <td>
                            {{ $item['employee_id'] }}
                        </td>
                        <td>
                            {{ $item['subsidiary_id'] }}
                        </td>
                        <td>
                            {{ $item['room_id'] }}
                        </td>
                        <td>
                            {{ $item['horary_id'] }}
                        </td>
                        <td>
                            {{ $item['total'] }}
                        </td>
                        <td>
                            <a href="{{ route('admin.sales.show', $item['id']) }}" class=" table-action table-action"
                                data-toggle="tooltip" data-original-title="Ver Venta">
                                <i class="fas fa-search"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer py-2">
            @include('generals::layouts.admin.pagination.pagination', [$skip])
        </div>
        @else
        <div class="card-header border-0">
            <h3 class="mb-0">Ventas</h3>
            <div class="row">
                <div class="col-12 text-right mb-3">
                    <a data-toggle="modal" data-target="#addSale" href="" class="btn btn-primary btn-sm"
                        data-toggle="tooltip" data-original-title="Añadir Venta"><i class="fa fa-edit"></i>Agregar Venta</a>
                </div>
            </div>
        </div>
        <div class="card-footer py-2">
            @include('generals::layouts.admin.pagination.pagination_null', [$skip])
        </div>
        @endif
    </div>
</section>

<div class="modal fade" id="addSale" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Añadir Venta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body py-0">
                <div class="row">
                    <div class="col-12 form-group">
                        <label for="employee_id">Modelo</label>
                        <select class="form-control" name="employee_id" id="employee_id" class="form-control">
                            @foreach ($employees as $employee)
                            <option value="{{ $employee['id'] }}">{{ $employee['label'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-center mb-3">
                        <button type="button" onclick="redirect()" class="btn btn-primary btn-sm">Crear</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script>
    function redirect() {
            var employee_id = document.getElementById("employee_id").value;
            window.location.href = "/admin/studio/sales/create/"+employee_id;
        }
</script>
@endsection
