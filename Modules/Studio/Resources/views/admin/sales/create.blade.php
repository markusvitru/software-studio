@extends('generals::layouts.admin.app')
@section('header')
<div class="header pb-2">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item " active aria-current="page">Ventas</li>
                            <li class="breadcrumb-item active">Crear Venta</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<section class="content">
    @include('generals::layouts.errors-and-messages')
    <div class="card">
        <form action="{{ route('admin.sales.store') }}" method="post" class="form">
            <input type="hidden" name="employee_id" value="{{ $employee['id'] }}">
            <input type="hidden" name="subsidiary_id" value="{{ $employee->subsidiary->id }}">
            <div class="card-body">
                @csrf
                <div class="col pl-0 mb-3">
                    <h2>Crear Venta - {{ $employee->name . ' ' . $employee->last_name }} - {{ $employee->subsidiary->name }}</h2>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <label for="room_id">Sala</label>
                        <select name="room_id" id="room_id" class="form-control">
                            @foreach ($employee->subsidiary->rooms->toarray() as $item)
                                <option value="{{ $item['id'] }}">{{ $item['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="horary_id">Horario</label>
                        <select name="horary_id" id="horary_id" class="form-control">
                            @foreach ($employee->horaries->toarray() as $item)
                                <option value="{{ $item['id'] }}">{{ $item['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <hr>
                <div class="row">
                    @foreach ($employee->platforms->toArray() as $item)
                        <div class="col-12 col-sm-6 form-group">
                            <label for="platforms[{{ $item['id'] }}]">{{ $item['name'] }}</label>
                            <input name="platforms[{{ $item['id'] }}]" id="platforms[{{ $item['id'] }}]" type="text" class="form-control">
                        </div>
                    @endforeach
                </div>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <label for="comments">Observaciones</label>
                        <textarea name="comments" id="comments" cols="30" rows="5" class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-primary btn-sm">Crear</button>
                <a href="{{ route('admin.sales.index') }}" class="btn btn-default btn-sm">Regresar</a>
            </div>
        </form>
    </div>
</section>
@endsection
