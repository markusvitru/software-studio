<div class="card">
    <div class="card-body">
        <div class="row align-items-center mb-3">
            <div class="col-12">
                <h2>Modelos</h2>
                <div class="table-responsive">
                    <table class="table align-items-center table-flush table-hover">
                            <thead class="thead-light">
                            <tr>
                                <th class="text-center" scope="col">Nombre</th>
                                <th class="text-center" scope="col">Apellido</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($models as $model)
                                <tr>
                                    <td class="text-center">{{ $model->name }}</td>
                                    <td class="text-center">{{ $model->last_name }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
