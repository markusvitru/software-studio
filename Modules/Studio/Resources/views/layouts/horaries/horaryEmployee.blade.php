<div class="card">
    <div class="card-header bg-transparent">
        <h3 class="mb-0">Horarios</h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-12 text-right mb-3">
                <a data-toggle="modal" data-target="#addHorary" href="" class="btn btn-primary btn-sm"
                    data-toggle="tooltip" data-original-title="Añadir Horario"><i class="fa fa-edit"></i>Agregar Horario</a>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table align-items-center table-flush table-hover">
                <thead class="thead-light">
                    <tr>
                        <th>Horario</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($employee->horaries as $horary)
                    <tr>
                        <td>{{ $horary->name }}</td>
                        <td>
                            <form id="form_{{$horary->id}}" action="/admin/employees/{{ $employee->id }}/deleteHorary" method="post"
                                class="form-horizontal">
                                <input type="hidden" name="horary_id" value="{{ $horary->id }}">
                                @csrf
                                <button onclick="return confirm('Estas seguro de eliminar este registro?')" type="submit"
                                    class="table-action table-action-delete button-reset" data-toggle="tooltip"
                                    data-original-title="Eliminar horario">
                                    <i class="fas fa-trash"></i>
                                </button>
                                <input type="hidden" name="_method" value="delete">
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="addHorary" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Añadir Horario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="/admin/employees/{{ $employee->id }}/addHorary" method="POST">
                @csrf
                @method('PUT')
                <div class="modal-body py-0">
                    <input name="employee_id" id="employee_id" type="hidden" value="{{ $employee->id }}">
                    <div class="row">
                        <div class="col-12 text-center mb-3">
                            <label for="">Horario</label>
                            <select name="horary_id" id="horary_id" class="form-control">
                                @foreach ($horaries as $horary)
                                    <option value="{{ $horary->id }}">{{ $horary->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center mb-3">
                            <button type="submit" class="btn btn-primary btn-sm">Añadir</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
