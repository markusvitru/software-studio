<div class="card">
    <div class="card-header bg-transparent">
        <h3 class="mb-0">Metas</h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-12 text-right mb-3">
                <a data-toggle="modal" data-target="#addGoal" href="" class="btn btn-primary btn-sm"
                    data-toggle="tooltip" data-original-title="Añadir Horario"><i class="fa fa-edit"></i>Agregar Meta</a>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table align-items-center table-flush table-hover">
                <thead class="thead-light">
                    <tr>
                        <th>Meta</th>
                        <th>Mes / Quincena</th>
                        <th>% cumplido</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($goals as $goal)
                    <tr>
                        <td>{{ $goal['total'] }}</td>
                        <td>{{ $goal['month'] }}, {{ $goal['fortnight'] }}</td>
                        <td>{{ $goal['percentage_completed'] }} %</td>
                        <td>
                            <form id="form_{{$goal['id']}}" action="{{ route('admin.goals.destroy', $goal['id']) }}" method="post"
                                class="form-horizontal">
                                <input type="hidden" name="id" value="{{ $goal['id'] }}">
                                <input type="hidden" name="employee_id" value="{{ $goal['employee_id'] }}">
                                @csrf
                                <button onclick="return confirm('Estas seguro de eliminar este registro?')" type="submit"
                                    class="table-action table-action-delete button-reset" data-toggle="tooltip"
                                    data-original-title="Eliminar horario">
                                    <i class="fas fa-trash"></i>
                                </button>
                                <input type="hidden" name="_method" value="delete">
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="addGoal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Añadir Meta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('admin.goals.store') }}" method="POST">
                @csrf
                <div class="modal-body py-0">
                    <input name="employee_id" id="employee_id" type="hidden" value="{{ $employee->id }}">
                    <div class="row">
                        <div class="col-12 form-group">
                            <label for="total">Total</label>
                            <input type="number" name="total" id="total" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-6 form-group">
                            <label for="month">Mes</label>
                            <select  name="month" id="month" class="form-control">
                                <option value="1">Enero</option>
                                <option value="2">Febrero</option>
                                <option value="3">Marzo</option>
                                <option value="4">Abril</option>
                                <option value="5">Mayo</option>
                                <option value="6">Junio</option>
                                <option value="7">Julio</option>
                                <option value="8">Agosto</option>
                                <option value="9">Septiembre</option>
                                <option value="10">Octubre</option>
                                <option value="11">Noviembre</option>
                                <option value="12">Diciembre</option>
                            </select>
                        </div>
                        <div class="col-12 col-sm-6 form-group">
                            <label for="fortnight">Quincena</label>
                            <select name="fortnight" id="fortnight" class="form-control">
                                <option value="0">01-15</option>
                                <option value="1">16-30</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center mb-3">
                            <button type="submit" class="btn btn-primary btn-sm">Añadir</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
