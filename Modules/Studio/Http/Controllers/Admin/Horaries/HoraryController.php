<?php

namespace Modules\Studio\Http\Controllers\Admin\Horaries;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Studio\Entities\Horaries\Repositories\Interfaces\HoraryRepositoryInterface;

class HoraryController extends Controller
{
    private $horaryInterface;

    public function __construct(
        HoraryRepositoryInterface $horaryRepositoryInterface
    ) {
        $this->horaryInterface   = $horaryRepositoryInterface;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {

        $skip = request()->input('skip') ? request()->input('skip') : 0;
        $time_start = request()->input('time_start');
        $time_end   = request()->input('time_end');

        if (request()->input('q') != '' && ($time_start == '' ||  $time_end  == '')) {
            $list = $this->horaryInterface->searchHorary(request()->input('q'), $skip * 30);
            $paginate = $this->horaryInterface->countHorary(request()->input('q'));
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } elseif ((request()->input('q') != '' || $time_start != '' ||  $time_end  != '')) {
            $list = $this->horaryInterface->searchHorary(request()->input('q'), $skip * 30, $time_start, $time_end);
            $paginate = $this->horaryInterface->countHorary(request()->input('q'), $time_start, $time_end);
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } else {
            $paginate = $this->horaryInterface->countHorary('');
            $list = $this->horaryInterface->listHoraries($skip * 30);
        }

        $paginate = ceil($paginate  / 30);
        $skipPaginate = $skip;

        $pageList = ($skipPaginate + 1) / 5;
        if (is_int($pageList) || $pageList > 1) {
            $countPage = $skipPaginate - 5;
            $maxPage = $skipPaginate + 6 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 6;
        } else {
            $countPage = 0;
            $maxPage = $skipPaginate + 5 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 5;
        }

        return view('studio::admin.horaries.list', [
            'horaries'   => $list,
            'headers'    => ['id', 'Horario', 'acciones'],
            'searchInputs'    => [],
            'inputs'     => [['label' => 'Horario', 'type' => 'text', 'name' => 'name']],
            'routeEdit' => 'admin.horaries.update',
            'optionsRoutes'   => 'admin.horaries',
            'skip'      => $skip,
            'pag'       => $pageList,
            'i'         => $countPage,
            'max'       => $maxPage,
            'paginate'  => $paginate
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('studio::admin.horaries.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->horaryInterface->createHorary($request->input());

        return redirect()->route('admin.horaries.index')->with('message', 'Horario Creado Exitosamente!');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('studio::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('studio::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->horaryInterface->updateHorary($request->input(), $id);
        return redirect()->route('admin.horaries.index')->with('message', 'Horario Actualizado Exitosamente!');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->horaryInterface->deleteHorary($id);
        return redirect()->route('admin.horaries.index')->with('message', 'Horario Eliminado Exitosamente!');
    }
}
