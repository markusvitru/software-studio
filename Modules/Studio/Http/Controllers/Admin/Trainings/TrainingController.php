<?php

namespace Modules\Studio\Http\Controllers\Admin\Trainings;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Companies\Entities\Employees\Repositories\Interfaces\EmployeeRepositoryInterface;
use Modules\Studio\Entities\Trainings\Repositories\Interfaces\TrainingRepositoryInterface;

class TrainingController extends Controller
{
    private $trainingInterface;
    private $employeeInterface;

    public function __construct(
        TrainingRepositoryInterface $trainingRepositoryInterface,
        EmployeeRepositoryInterface $employeeRepositoryInterface
    ) {
        $this->trainingInterface   = $trainingRepositoryInterface;
        $this->employeeInterface = $employeeRepositoryInterface;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function getTraingnsForApp()
    {
        $list = $this->trainingInterface->listTrainings();

        return $list;
    }

    public function trainingToEmployee(Request $request, $id)
    {
        $employee = $this->employeeInterface->findEmployeeById($id);
        $employee->trainings()->attach($request->input('training_id'), ['comment' => $request->input('comment')]);

        return 'true';
    }

    public function index(Request $request)
    {
        $skip = request()->input('skip') ? request()->input('skip') : 0;
        $from = request()->input('from') ? request()->input('from') . " 00:00:01" : Carbon::now()->subMonths(1);
        $to   = request()->input('to') ? request()->input('to') . " 23:59:59" : Carbon::now();

        if (request()->input('q') != '' && (request()->input('from') == '' || request()->input('to') == '')) {
            $list = $this->trainingInterface->searchTraining(request()->input('q'), $skip * 30);
            $paginate = $this->trainingInterface->countTraining(request()->input('q'));
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } elseif ((request()->input('q') != '' || request()->input('from') != '' || request()->input('to') != '')) {
            $list = $this->trainingInterface->searchTraining(request()->input('q'), $skip * 30, $from, $to);
            $paginate = $this->trainingInterface->countTraining(request()->input('q'), $from, $to);
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } else {
            $paginate = $this->trainingInterface->countTraining('');
            $list = $this->trainingInterface->listTrainingsAdmin($skip * 30);
        }

        $paginate = ceil($paginate  / 30);
        $skipPaginate = $skip;

        $pageList = ($skipPaginate + 1) / 5;
        if (is_int($pageList) || $pageList > 1) {
            $countPage = $skipPaginate - 5;
            $maxPage = $skipPaginate + 6 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 6;
        } else {
            $countPage = 0;
            $maxPage = $skipPaginate + 5 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 5;
        }

        return view('studio::admin.trainings.list', [
            'trainings'       => $list,
            'headers'         => ['id', 'Nombre', 'Descripcion', 'URL', 'Opciones'],
            'inputs'          => [['label' => 'Nombre', 'type' => 'text', 'name' => 'name'], ['label' => 'Descripcion', 'type' => 'text', 'name' => 'description'], ['label' => 'Url', 'type' => 'url', 'name' => 'url']],
            'searchInputs'    => [['label' => 'Buscar', 'type' => 'text', 'name' => 'q']],
            'routeEdit'       => 'admin.trainings.update',
            'optionsRoutes'   => 'admin.trainings',
            'skip'            => $skip,
            'pag'             => $pageList,
            'i'               => $countPage,
            'max'             => $maxPage,
            'paginate'        => $paginate
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('studio::admin.trainings.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->trainingInterface->createTraining($request->input());
        return redirect()->route('admin.trainings.index')->with('message', 'Capacitacion Creada Exitosamente!');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('studio::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('studio::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->trainingInterface->updateTraining($request->input(), $id);
        return redirect()->route('admin.trainings.index')->with('message', 'Capacitacion Actualizada Exitosamente!');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->trainingInterface->deleteTraining($id);
        return redirect()->route('admin.trainings.index')->with('message', 'Capacitacion Eliminada Exitosamente!');
    }
}
