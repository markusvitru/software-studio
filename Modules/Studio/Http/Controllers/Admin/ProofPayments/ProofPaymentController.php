<?php

namespace Modules\Studio\Http\Controllers\Admin\ProofPayments;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Companies\Entities\Employees\Repositories\Interfaces\EmployeeRepositoryInterface;
use Modules\Companies\Entities\Roles\Repositories\Interfaces\RoleRepositoryInterface;
use Modules\Companies\Entities\Subsidiaries\Repositories\Interfaces\SubsidiaryRepositoryInterface;
use Modules\Studio\Entities\ProofPayments\Repositories\Interfaces\ProofPaymentRepositoryInterface;

class ProofPaymentController extends Controller
{
    private $proofPaymentInterface;
    private $employeeInterface, $roleInterface, $subsidaryInterface;

    public function __construct(
        ProofPaymentRepositoryInterface $proofPaymentRepositoryInterface,
        EmployeeRepositoryInterface $employeeRepositoryInterface,
        RoleRepositoryInterface $roleRepositoryInterface,
        SubsidiaryRepositoryInterface $subsidaryInterface
    ) {
        $this->proofPaymentInterface   = $proofPaymentRepositoryInterface;
        $this->employeeInterface = $employeeRepositoryInterface;
        $this->roleInterface = $roleRepositoryInterface;
        $this->subsidaryInterface = $subsidaryInterface;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $skip = request()->input('skip') ? request()->input('skip') : 0;
        $from = request()->input('from') ? request()->input('from') . " 00:00:01" : Carbon::now()->subMonths(1);
        $to   = request()->input('to') ? request()->input('to') . " 23:59:59" : Carbon::now();

        if (request()->input('q') != '' && (request()->input('from') == '' || request()->input('to') == '')) {
            $list = $this->proofPaymentInterface->searchProofPayment(request()->input('q'), $skip * 30);
            $paginate = $this->proofPaymentInterface->countProofPayment(request()->input('q'));
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } elseif ((request()->input('q') != '' || request()->input('from') != '' || request()->input('to') != '')) {
            $list = $this->proofPaymentInterface->searchProofPayment(request()->input('q'), $skip * 30, $from, $to);
            $paginate = $this->proofPaymentInterface->countProofPayment(request()->input('q'), $from, $to);
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } else {
            $paginate = $this->proofPaymentInterface->countProofPayment('');
            $list = $this->proofPaymentInterface->listProofPayments($skip * 30);
        }

        $paginate = ceil($paginate  / 30);
        $skipPaginate = $skip;

        $pageList = ($skipPaginate + 1) / 5;
        if (is_int($pageList) || $pageList > 1) {
            $countPage = $skipPaginate - 5;
            $maxPage = $skipPaginate + 6 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 6;
        } else {
            $countPage = 0;
            $maxPage = $skipPaginate + 5 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 5;
        }

        foreach ($list as $key => $value) {
            $list[$key]['employee_id'] = $list[$key]->employee->name . ' ' . $list[$key]->employee->last_name;
        }

        $subsidiaries = [];
        $getSubsidiaries = $this->subsidaryInterface->getAllSubsidiaryNames();
        foreach ($getSubsidiaries->toArray() as $key => $value) {
            $subsidiaries[] = ['id' => $value['id'], 'label' => $value['name']];
        }

        return view('studio::admin.proof-payments.list', [
            'proofPayments'   => $list,
            'subsidiaries'    => $subsidiaries,
            'searchInputs'    => [['label' => 'Buscar', 'type' => 'text', 'name' => 'q'], ['label' => 'Desde', 'type' => 'date', 'name' => 'from'], ['label' => 'Hasta', 'type' => 'date', 'name' => 'to']],
            'optionsRoutes'   => 'admin.proof-payments',
            'skip'            => $skip,
            'pag'             => $pageList,
            'i'               => $countPage,
            'max'             => $maxPage,
            'paginate'        => $paginate
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('studio::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('studio::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('studio::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function generateProofPayments($subsidiaryId)
    {
        $date = Carbon::now();
        $year = $date->isoFormat('Y');
        $day = $date->isoFormat('D');
        $month = $date->isoFormat('MM');
        $startDay = ($day >= 1 && $day <= 15) ? '01' : '16';
        $endDay = ($day >= 1 && $day <= 15) ? '15' : '31';
        $startDate = $year . '-' . $month . '-' . $startDay . ' 01:00:00';
        $endDate = $year . '-' . $month . '-' . $endDay . ' 23:59:59';
        $fortnight = ($day >= 1 && $day <= 15) ? '0' : '1';

        $models = $this->roleInterface->findModelsBySubsidiaryId($subsidiaryId);

        foreach ($models as $key => $model) {
            $totalSales = 0;
            $totalPenalties = 0;
            $totalDiscounts = 0;
            $sales = $model->sales->where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate)->toArray();
            $penalties = $model->penalties->where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate)->toArray();
            $discounts = $model->discounts->where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate)->toArray();

            foreach ($sales as $key => $sale) {
                $totalSales += $sale['total'];
            }

            if (count($penalties) > 0) {
                foreach ($penalties as $key => $penalty) {
                    $totalPenalties += $penalty['cost'];
                }
            }

            if (count($discounts) > 0) {
                foreach ($discounts as $key => $discount) {
                    $totalDiscounts += $discount['cost'];
                }
            }
            $totalProofPayment = $totalSales - ($totalDiscounts + $totalPenalties);
            $data = [
                'employee_id' => $model->id,
                'total_sales' => $totalSales,
                'total_discounts' => $totalDiscounts,
                'total_penalties' => $totalPenalties,
                'total' => $totalProofPayment
            ];

            $this->proofPaymentInterface->createProofPayment($data);
        }

        return redirect()->route('admin.proof-payments.index')->with('message', 'Comprobantes de pago generados exitosamente!');
    }

    public function getProofPaymentForEmployee($employee_id)
    {
        $proofPayments = $this->proofPaymentInterface->findProofPaymentsByModel($employee_id);

        return $proofPayments;
    }
}
