<?php

namespace Modules\Studio\Http\Controllers\Admin\EmployeePlatformAccounts;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Companies\Entities\Roles\Repositories\Interfaces\RoleRepositoryInterface;
use Modules\Studio\Entities\EmployeePlatformAccounts\Repositories\Interfaces\EmployeePlatformAccountRepositoryInterface;
use Modules\Studio\Entities\Platforms\Repositories\Interfaces\PlatformRepositoryInterface;

class EmployeePlatformAccountController extends Controller
{
    private $roleInterface, $platformInterface, $employeePlatformAccountInterface;
    public function __construct(
        RoleRepositoryInterface $roleRepositoryInterface,
        PlatformRepositoryInterface $platformRepositoryInterface,
        EmployeePlatformAccountRepositoryInterface $employeePlatformAccountRepositoryInterface
    ) {
        $this->roleInterface = $roleRepositoryInterface;
        $this->platformInterface = $platformRepositoryInterface;
        $this->employeePlatformAccountInterface = $employeePlatformAccountRepositoryInterface;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $skip = request()->input('skip') ? request()->input('skip') : 0;
        $from = request()->input('from') ? request()->input('from') . " 00:00:01" : Carbon::now()->subMonths(1);
        $to   = request()->input('to') ? request()->input('to') . " 23:59:59" : Carbon::now();

        if (request()->input('q') != '' && (request()->input('from') == '' || request()->input('to') == '')) {
            $list = $this->employeePlatformAccountInterface->searchEmployeePlatformAccount(request()->input('q'), $skip * 30);
            $paginate = $this->employeePlatformAccountInterface->countEmployeePlatformAccount(request()->input('q'));
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } elseif ((request()->input('q') != '' || request()->input('from') != '' || request()->input('to') != '')) {
            $list = $this->employeePlatformAccountInterface->searchEmployeePlatformAccount(request()->input('q'), $skip * 30, $from, $to);
            $paginate = $this->employeePlatformAccountInterface->countEmployeePlatformAccount(request()->input('q'), $from, $to);
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } else {
            $paginate = $this->employeePlatformAccountInterface->countEmployeePlatformAccount('');
            $list = $this->employeePlatformAccountInterface->listEmployeePlatformAccounts($skip * 30);
        }

        $paginate = ceil($paginate  / 30);
        $skipPaginate = $skip;

        $pageList = ($skipPaginate + 1) / 5;
        if (is_int($pageList) || $pageList > 1) {
            $countPage = $skipPaginate - 5;
            $maxPage = $skipPaginate + 6 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 6;
        } else {
            $countPage = 0;
            $maxPage = $skipPaginate + 5 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 5;
        }


        foreach ($list as $key => $value) {
            $list[$key]['employee_id'] = $list[$key]->employee->name . ' ' . $list[$key]->employee->last_name . ' / ' . $list[$key]->employee->subsidiary->name;
            $list[$key]['platform_id'] = $list[$key]->platform->name;
        }

        $role = $this->roleInterface->findRoleById('3');
        $employees = [];
        foreach ($role->user->toArray() as $key => $value) {
            $employees[] = ['id' => $value['id'], 'label' => $value['name'] . ' ' . $value['last_name']];
        }
        $platformList = $this->platformInterface->listAllPlatforms();
        $platforms = [];
        foreach ($platformList->toArray() as $key => $value) {
            $platforms[] = ['id' => $value['id'], 'label' => $value['name']];
        }
        return view('studio::admin.employee-platform-accounts.list', [
            'employeePlatformAccount' => $list,
            'headers'                 => ['id', 'Modelo', 'Plataforma', 'Usuario', 'Contraseña', 'Opciones'],
            'searchInputs'    => [['label' => 'Buscar', 'type' => 'text', 'name' => 'q'], ['label' => 'Desde', 'type' => 'date', 'name' => 'from'], ['label' => 'Hasta', 'type' => 'date', 'name' => 'to']],
            'inputs'                  => [['label' => 'Modelo', 'type' => 'select', 'options' => $employees, 'name' => 'employee_id'], ['label' => 'Plataforma', 'type' => 'select', 'options' => $platforms, 'name' => 'platform_id'], ['label' => 'Usuario', 'type' => 'text', 'name' => 'user'], ['label' => 'Contraseña', 'type' => 'text', 'name' => 'password']],
            'routeEdit'               => 'admin.employee-platform-accounts.update',
            'optionsRoutes'           => 'admin.employee-platform-accounts',
            'skip'                    => $skip,
            'pag'                     => $pageList,
            'i'                       => $countPage,
            'max'                     => $maxPage,
            'paginate'                => $paginate
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $roles = $this->roleInterface->findRoleById(3);
        $employees = [];
        $platforms = [];
        foreach ($roles->user->toArray() as $key => $value) {
            $employees[] = ['id' => $value['id'], 'label' => $value['name'] . ' ' . $value['last_name']];
        }
        $platforms = $this->platformInterface->listAllPlatforms()->toArray();
        return view('studio::admin.employee-platform-accounts.create', [
            'employees' => $employees,
            'platforms' => $platforms
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->employeePlatformAccountInterface->createEmployeePlatformAccount($request->input());

        return redirect()->route('admin.employee-platform-accounts.index')->with('message', 'Cuenta Creada Exitosamente!');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('studio::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('studio::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->employeePlatformAccountInterface->updateEmployeePlatformAccount($request->input(), $id);

        return redirect()->route('admin.employee-platform-accounts.index')->with('message', 'Cuenta Actualizada Exitosamente!');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->employeePlatformAccountInterface->deleteEmployeePlatformAccount($id);

        return redirect()->route('admin.employee-platform-accounts.index')->with('message', 'Cuenta Actualizada Exitosamente!');
    }
}
