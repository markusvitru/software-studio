<?php

namespace Modules\Studio\Http\Controllers\Admin\SendingPhotos;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Companies\Entities\Roles\Repositories\Interfaces\RoleRepositoryInterface;
use Modules\Studio\Entities\SendingPhotos\Repositories\Interfaces\SendingPhotoRepositoryInterface;

class SendingPhotoController extends Controller
{
    private $sendingPhotoInterface;
    private $roleInterface;

    public function __construct(
        SendingPhotoRepositoryInterface $sendingPhotoRepositoryInterface,
        RoleRepositoryInterface $roleRepositoryInterface
    ) {
        $this->sendingPhotoInterface = $sendingPhotoRepositoryInterface;
        $this->roleInterface         = $roleRepositoryInterface;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $skip = request()->input('skip') ? request()->input('skip') : 0;
        $from = request()->input('from') ? request()->input('from') . " 00:00:01" : Carbon::now()->subMonths(1);
        $to   = request()->input('to') ? request()->input('to') . " 23:59:59" : Carbon::now();

        if (request()->input('q') != '' && (request()->input('from') == '' || request()->input('to') == '')) {
            $list = $this->sendingPhotoInterface->searchSendingPhoto(request()->input('q'), $skip * 30);
            $paginate = $this->sendingPhotoInterface->countSendingPhoto(request()->input('q'));
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } elseif ((request()->input('q') != '' || request()->input('from') != '' || request()->input('to') != '')) {
            $list = $this->sendingPhotoInterface->searchSendingPhoto(request()->input('q'), $skip * 30, $from, $to);
            $paginate = $this->sendingPhotoInterface->countSendingPhoto(request()->input('q'), $from, $to);
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } else {
            $paginate = $this->sendingPhotoInterface->countSendingPhoto('');
            $list = $this->sendingPhotoInterface->listSendingPhotos($skip * 30);
        }

        foreach ($list as $key => $value) {
            $list[$key]['employee_id'] = $list[$key]->employee->name . ' ' . $list[$key]->employee->last_name . ' / ' . $list[$key]->employee->subsidiary->name;
        }

        $role = $this->roleInterface->findRoleById(3);
        $employees = [];
        foreach ($role->user->toArray() as $key => $value) {
            $employees[] = ['id' => $value['id'], 'label' => $value['name'] . ' ' . $value['last_name']];
        }

        $paginate = ceil($paginate  / 30);
        $skipPaginate = $skip;

        $pageList = ($skipPaginate + 1) / 5;
        if (is_int($pageList) || $pageList > 1) {
            $countPage = $skipPaginate - 5;
            $maxPage = $skipPaginate + 6 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 6;
        } else {
            $countPage = 0;
            $maxPage = $skipPaginate + 5 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 5;
        }


        return view('studio::admin.sending-photos.list', [
            'sendingPhotos'   => $list,
            'headers'         => ['id', 'Empleado', 'Fecha', 'Opciones'],
            'inputs'          => [],
            'searchInputs'    => [['label' => 'Buscar', 'type' => 'text', 'name' => 'q'], ['label' => 'Desde', 'type' => 'date', 'name' => 'from'], ['label' => 'Hasta', 'type' => 'date', 'name' => 'to']],
            'routeEdit'       => 'admin.sending-photos.update',
            'optionsRoutes'   => 'admin.sending-photos',
            'skip'            => $skip,
            'pag'             => $pageList,
            'i'               => $countPage,
            'max'             => $maxPage,
            'paginate'        => $paginate
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('studio::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }
    public function storePhotos(Request $request){
        $var = $this->sendingPhotoInterface->createSendingPhoto($request->except('_token', '_method'));

        return $var;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $sendingPhoto = $this->sendingPhotoInterface->findSendingPhotokById($id);
        return view('studio::admin.sending-photos.show', [
            'sendingPhoto' => $sendingPhoto
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('studio::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
