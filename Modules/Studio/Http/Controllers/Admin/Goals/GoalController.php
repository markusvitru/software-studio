<?php

namespace Modules\Studio\Http\Controllers\Admin\Goals;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Companies\Entities\Employees\Repositories\Interfaces\EmployeeRepositoryInterface;
use Modules\Studio\Entities\Goals\Repositories\Interfaces\GoalRepositoryInterface;

class GoalController extends Controller
{
    private $goalInterface;
    private $employeeInterface;

    public function __construct(
        GoalRepositoryInterface $goalRepositoryInterface,
        EmployeeRepositoryInterface $employeeRepositoryInterface
    ) {
        $this->goalInterface   = $goalRepositoryInterface;
        $this->employeeInterface = $employeeRepositoryInterface;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function getGoalForEmployee($id){
        return $this->goalInterface->getGoalForEmployee($id);
    }

    public function index()
    {
        dd('GoalController');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('studio::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $existGoal = $this->goalInterface->findGoalByModelAndMonthAndFortnight($request->input('employee_id'), $request->input('month'), $request->input('fortnight'));

        if(count($existGoal) > 0){
            return redirect()->route('admin.employees.show', $request->input('employee_id'))
            ->with('error', 'Esta modelo ya tiene una meta para ese mes y esa quincena!');
        }

        $goal = $this->goalInterface->createGoal($request->input());


        return redirect()->route('admin.employees.show', $request->input('employee_id'))
            ->with('message', 'Meta Añadida Exitosamente!');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('studio::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('studio::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Request $request,$id)
    {
        $this->goalInterface->deleteGoal($id);
        return redirect()->route('admin.employees.show', $request->input('employee_id'))
            ->with('message', 'Meta Eliminada Exitosamente!');
    }
}
