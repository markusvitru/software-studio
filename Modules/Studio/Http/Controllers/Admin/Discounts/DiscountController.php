<?php

namespace Modules\Studio\Http\Controllers\Admin\Discounts;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Companies\Entities\Roles\Repositories\Interfaces\RoleRepositoryInterface;
use Modules\Studio\Entities\Discounts\Repositories\Interfaces\DiscountRepositoryInterface;

class DiscountController extends Controller
{
    private $discountInterface;
    private $roleInterface;

    public function __construct(
        DiscountRepositoryInterface $discountRepositoryInterface,
        RoleRepositoryInterface $roleRepositoryInterface
    ) {
        $this->discountInterface = $discountRepositoryInterface;
        $this->roleInterface     = $roleRepositoryInterface;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $skip = request()->input('skip') ? request()->input('skip') : 0;
        $from = request()->input('from') ? request()->input('from') . " 00:00:01" : Carbon::now()->subMonths(1);
        $to   = request()->input('to') ? request()->input('to') . " 23:59:59" : Carbon::now();

        if (request()->input('q') != '' && (request()->input('from') == '' || request()->input('to') == '')) {
            $list = $this->discountInterface->searchDiscount(request()->input('q'), $skip * 30);
            $paginate = $this->discountInterface->countDiscount(request()->input('q'));
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } elseif ((request()->input('q') != '' || request()->input('from') != '' || request()->input('to') != '')) {
            $list = $this->discountInterface->searchDiscount(request()->input('q'), $skip * 30, $from, $to);
            $paginate = $this->discountInterface->countDiscount(request()->input('q'), $from, $to);
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } else {
            $paginate = $this->discountInterface->countDiscount('');
            $list = $this->discountInterface->listDiscounts($skip * 30);
        }

        $paginate = ceil($paginate  / 30);
        $skipPaginate = $skip;

        $pageList = ($skipPaginate + 1) / 5;
        if (is_int($pageList) || $pageList > 1) {
            $countPage = $skipPaginate - 5;
            $maxPage = $skipPaginate + 6 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 6;
        } else {
            $countPage = 0;
            $maxPage = $skipPaginate + 5 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 5;
        }

        foreach ($list as $key => $value) {
            $list[$key]['employee_id'] = $list[$key]->employee->name . ' ' . $list[$key]->employee->last_name . ' / ' . $list[$key]->employee->subsidiary->name;
        }
        $role = $this->roleInterface->findRoleById('3');
        $employees = [];
        foreach ($role->user->toArray() as $key => $value) {
            $employees[] = ['id' => $value['id'], 'label' => $value['name'] . ' ' . $value['last_name']];
        }
        return view('studio::admin.discounts.list', [
            'discounts' => $list,
            'headers'   => ['id', 'Modelo', 'Descuento', 'Precio', 'Opciones'],
            'searchInputs'    => [['label' => 'Buscar', 'type' => 'text', 'name' => 'q'], ['label' => 'Desde', 'type' => 'date', 'name' => 'from'], ['label' => 'Hasta', 'type' => 'date', 'name' => 'to']],
            'inputs'    => [['label' => 'Empleado', 'type' => 'select', 'options' => $employees, 'name' => 'employee_id'], ['label' => 'Descuento', 'type' => 'text', 'name' => 'name'], ['label' => 'Costo', 'type' => 'number', 'name' => 'cost']],
            'routeEdit' => 'admin.discounts.update',
            'optionsRoutes'   => 'admin.discounts',
            'skip'      => $skip,
            'pag'       => $pageList,
            'i'         => $countPage,
            'max'       => $maxPage,
            'paginate'  => $paginate
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $role = $this->roleInterface->findRoleById('3');
        $employees = [];
        foreach ($role->user->toArray() as $key => $value) {
            $employees[] = ['id' => $value['id'], 'label' => $value['name'] . ' ' . $value['last_name']];
        }
        return view('studio::admin.discounts.create', [
            'employees' => $employees,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->discountInterface->createDiscount($request->input());
        return redirect()->route('admin.discounts.index')->with('message', 'Descuento Creado Exitosamente!');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('studio::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('studio::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->discountInterface->updateDiscount($request->input(), $id);
        return redirect()->route('admin.discounts.index')->with('message', 'Descuento Actualizado Exitosamente!');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->discountInterface->deleteDiscount($id);
        return redirect()->route('admin.discounts.index')->with('message', 'Descuento Eliminado Exitosamente!');
    }
}
