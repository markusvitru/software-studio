<?php

namespace Modules\Studio\Http\Controllers\Admin\Sales;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Companies\Entities\Employees\Repositories\Interfaces\EmployeeRepositoryInterface;
use Modules\Companies\Entities\Roles\Repositories\Interfaces\RoleRepositoryInterface;
use Modules\Studio\Entities\Goals\Repositories\Interfaces\GoalRepositoryInterface;
use Modules\Studio\Entities\Platforms\Repositories\Interfaces\PlatformRepositoryInterface;
use Modules\Studio\Entities\Sales\Repositories\Interfaces\SaleRepositoryInterface;

class SaleController extends Controller
{
    private $saleInterface;
    private $roleInterface, $employeeInterface, $platformInterface, $goalInterface;

    public function __construct(
        SaleRepositoryInterface $saleRepositoryInterface,
        RoleRepositoryInterface $roleRepositoryInterface,
        EmployeeRepositoryInterface $employeeRepositoryInterface,
        PlatformRepositoryInterface $platformRepositoryInterface,
        GoalRepositoryInterface $goalRepositoryInterface
    ) {
        $this->saleInterface     = $saleRepositoryInterface;
        $this->roleInterface     = $roleRepositoryInterface;
        $this->employeeInterface = $employeeRepositoryInterface;
        $this->platformInterface = $platformRepositoryInterface;
        $this->goalInterface = $goalRepositoryInterface;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $skip = request()->input('skip') ? request()->input('skip') : 0;
        $from = request()->input('from') ? request()->input('from') . " 00:00:01" : Carbon::now()->subMonths(1);
        $to   = request()->input('to') ? request()->input('to') . " 23:59:59" : Carbon::now();

        if (request()->input('q') != '' && (request()->input('from') == '' || request()->input('to') == '')) {
            $list = $this->saleInterface->searchSale(request()->input('q'), $skip * 30);
            $paginate = $this->saleInterface->countSale(request()->input('q'));
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } elseif ((request()->input('q') != '' || request()->input('from') != '' || request()->input('to') != '')) {
            $list = $this->saleInterface->searchSale(request()->input('q'), $skip * 30, $from, $to);
            $paginate = $this->saleInterface->countSale(request()->input('q'), $from, $to);
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } else {
            $paginate = $this->saleInterface->countSale('');
            $list = $this->saleInterface->listSales($skip * 30);
        }

        $paginate = ceil($paginate  / 30);
        $skipPaginate = $skip;

        $pageList = ($skipPaginate + 1) / 5;
        if (is_int($pageList) || $pageList > 1) {
            $countPage = $skipPaginate - 5;
            $maxPage = $skipPaginate + 6 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 6;
        } else {
            $countPage = 0;
            $maxPage = $skipPaginate + 5 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 5;
        }

        foreach ($list as $key => $value) {
            $list[$key]['employee_id'] = $list[$key]->employee->name . ' ' . $list[$key]->employee->last_name;
            $list[$key]['room_id'] = $list[$key]->room->name;
            $list[$key]['horary_id'] = $list[$key]->horary->name;
            $list[$key]['subsidiary_id'] = $list[$key]->subsidiary->name;
        }

        $role      = $this->roleInterface->findRoleById(3);
        $employees = [];
        foreach ($role->user->toArray() as $key => $value) {
            $employees[] = ['id' => $value['id'], 'label' => $value['name'] . ' ' . $value['last_name']];
        }

        return view('studio::admin.sales.list', [
            'sales'           => $list,
            'employees'       => $employees,
            'searchInputs'    => [['label' => 'Buscar', 'type' => 'text', 'name' => 'q'], ['label' => 'Desde', 'type' => 'date', 'name' => 'from'], ['label' => 'Hasta', 'type' => 'date', 'name' => 'to']],
            'optionsRoutes'   => 'admin.sales',
            'skip'            => $skip,
            'pag'             => $pageList,
            'i'               => $countPage,
            'max'             => $maxPage,
            'paginate'        => $paginate
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create($employee_id)
    {
        $employee = $this->employeeInterface->findEmployeeById($employee_id);
        return view('studio::admin.sales.create', [
            'employee' => $employee
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $total = 0;
        $totalSales = 0;
        foreach ($request['platforms'] as $key => $value) {
            $platform = $this->platformInterface->findPlatformById($key);
            $total += $value * $platform['token_cost'];
        }

        $data = [
            'employee_id'   => $request['employee_id'],
            'room_id'       => $request['room_id'],
            'horary_id'     => $request['horary_id'],
            'subsidiary_id' => $request['subsidiary_id'],
            'comments' => $request['comments'],
            'total' => $total
        ];

        $sale = $this->saleInterface->createSale($data);

        foreach ($request['platforms'] as $key => $value) {
            $sale->platforms()->attach(['platform_id' => $key], ['total_cost' => $value, 'total_token' => 1]);
        }

        return redirect()->route('admin.sales.index')->with('message', 'Venta Creada Exitosamente!');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $sale = $this->saleInterface->findSaleById($id);
        return view('studio::admin.sales.show', [
            'sale' => $sale
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('studio::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
