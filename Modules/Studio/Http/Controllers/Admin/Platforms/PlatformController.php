<?php

namespace Modules\Studio\Http\Controllers\Admin\Platforms;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Studio\Entities\Platforms\Repositories\Interfaces\PlatformRepositoryInterface;

class PlatformController extends Controller
{
    private $platformInterface;

    public function __construct(
        PlatformRepositoryInterface $platformRepositoryInterface
    ) {
        $this->platformInterface   = $platformRepositoryInterface;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $skip = request()->input('skip') ? request()->input('skip') : 0;
        $from = request()->input('from') ? request()->input('from') . " 00:00:01" : Carbon::now()->subMonths(1);
        $to   = request()->input('to') ? request()->input('to') . " 23:59:59" : Carbon::now();

        if (request()->input('q') != '' && (request()->input('from') == '' || request()->input('to') == '')) {
            $list = $this->platformInterface->searchPlatform(request()->input('q'), $skip * 30);
            $paginate = $this->platformInterface->countPlatform(request()->input('q'));
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } elseif ((request()->input('q') != '' || request()->input('from') != '' || request()->input('to') != '')) {
            $list = $this->platformInterface->searchPlatform(request()->input('q'), $skip * 30, $from, $to);
            $paginate = $this->platformInterface->countPlatform(request()->input('q'), $from, $to);
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } else {
            $paginate = $this->platformInterface->countPlatform('');
            $list = $this->platformInterface->listPlatforms($skip * 30);
        }

        $paginate = ceil($paginate  / 30);
        $skipPaginate = $skip;

        $pageList = ($skipPaginate + 1) / 5;
        if (is_int($pageList) || $pageList > 1) {
            $countPage = $skipPaginate - 5;
            $maxPage = $skipPaginate + 6 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 6;
        } else {
            $countPage = 0;
            $maxPage = $skipPaginate + 5 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 5;
        }

        foreach ($list as $key => $value) {
            $list[$key]['type'] = ($list[$key]['type'] == 0) ? 'Dolar' : 'Token';
        }
        $optionsType = [['id' => 0, 'label' => 'Dolar'], ['id' => 1, 'label' => 'Token']];
        return view('studio::admin.platforms.list', [
            'platforms'     => $list,
            'headers'       => ['id', 'Nombre', 'Tipo', 'Valor', 'Opciones'],
            'searchInputs'  => [['label' => 'Buscar', 'type' => 'text', 'name' => 'q'], ['label' => 'Desde', 'type' => 'date', 'name' => 'from'], ['label' => 'Hasta', 'type' => 'date', 'name' => 'to']],
            'inputs'        => [['label' => 'Nombre', 'type' => 'text', 'name' => 'name'], ['label' => 'Tipo', 'type' => 'select', 'options' => $optionsType, 'name' => 'type'], ['label' => 'Valor del token', 'type' => 'number', 'name' => 'token_cost']],
            'routeEdit'     => 'admin.platforms.update',
            'optionsRoutes' => 'admin.platforms',
            'skip'          => $skip,
            'pag'           => $pageList,
            'i'             => $countPage,
            'max'           => $maxPage,
            'paginate'      => $paginate
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('studio::admin.platforms.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->platformInterface->createPlatform($request->input());
        return redirect()->route('admin.platforms.index')->with('message', 'Plataforma Creada Exitosamente!');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('studio::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('studio::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->platformInterface->updatePlatform($request->input(), $id);
        return redirect()->route('admin.platforms.index')->with('message', 'Plataforma Actualizada Exitosamente!');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->platformInterface->deletePlatform($id);
        return redirect()->route('admin.platforms.index')->with('message', 'Plataforma Eliminada Exitosamente!');
    }
}
