<?php

namespace Modules\Studio\Http\Controllers\Admin\PendingTasks;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Studio\Entities\TaskComments\Repositories\Interfaces\TaskCommentRepositoryInterface;
use Modules\Studio\Entities\Tasks\Repositories\Interfaces\TaskRepositoryInterface;

class PendingTaskController extends Controller
{
    private $taskInterface, $taskCommentInterface;

    public function __construct(
        TaskRepositoryInterface $taskRepositoryInterface,
        TaskCommentRepositoryInterface $taskCommentRepositoryInterface
    ) {
        $this->taskInterface = $taskRepositoryInterface;
        $this->taskCommentInterface = $taskCommentRepositoryInterface;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $employeeId = auth()->guard('employee')->user()->id;
        $skip = request()->input('skip') ? request()->input('skip') : 0;

        $list = $this->taskInterface->findPendingTaskByEmployeeId($employeeId, $skip * 30);
        $paginate = $this->taskInterface->countPendingTaskByEmployeeId($employeeId);

        foreach ($list as $key => $value) {
            $list[$key]['employee_id'] = $list[$key]->employee->name . ' ' . $list[$key]->employee->last_name . ' / ' . $list[$key]->employee->subsidiary->name;
            $list[$key]['state']       = ($list[$key]['state'] == 0) ? 'Pendiente' : 'Realizada';
        }

        $paginate = ceil($paginate  / 30);
        $skipPaginate = $skip;

        $pageList = ($skipPaginate + 1) / 5;
        if (is_int($pageList) || $pageList > 1) {
            $countPage = $skipPaginate - 5;
            $maxPage = $skipPaginate + 6 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 6;
        } else {
            $countPage = 0;
            $maxPage = $skipPaginate + 5 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 5;
        }

        return view('studio::admin.pending-tasks.list', [
            'penalties'     => $list,
            'headers'       => ['id', 'Empleado', 'Tarea', 'Estado', 'Opciones'],
            'searchInputs' => [['label' => 'Buscar', 'type' => 'text', 'name' => 'q'], ['label' => 'Desde', 'type' => 'date', 'name' => 'from'], ['label' => 'Hasta', 'type' => 'date', 'name' => 'to']],
            'inputs'        => [['label' => 'Comentario', 'name' => 'comment', 'type' => 'textarea']],
            'routeEdit'     => 'admin.pending-tasks.update',
            'optionsRoutes' => 'admin.pending-tasks',
            'skip'          => $skip,
            'pag'           => $pageList,
            'i'             => $countPage,
            'max'           => $maxPage,
            'paginate'      => $paginate
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('studio::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('studio::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $request['task_id'] = $id;
        $this->taskCommentInterface->createTaskComment($request->input(), $id);

        return redirect()->route('admin.pending-tasks.index')->with('message', 'Comentario Añadido Exitosamente!');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $dataComment = ['comment' => 'Tarea completada', 'task_id' => $id];
        $this->taskCommentInterface->createTaskComment($dataComment);
        $dataTask = ['state' => '1'];
        $this->taskInterface->updateTask($dataTask, $id);
        return redirect()->route('admin.pending-tasks.index')->with('message', 'Tarea Completada Exitosamente!');
    }
}
