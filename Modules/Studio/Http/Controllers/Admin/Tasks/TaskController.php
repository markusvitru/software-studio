<?php

namespace Modules\Studio\Http\Controllers\Admin\Tasks;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Route;
use Modules\Companies\Entities\Employees\Repositories\Interfaces\EmployeeRepositoryInterface;
use Modules\Companies\Entities\Roles\Repositories\Interfaces\RoleRepositoryInterface;
use Modules\Studio\Entities\Tasks\Repositories\Interfaces\TaskRepositoryInterface;

class TaskController extends Controller
{
    private $tasksInterface;
    private $roleInterface;

    public function __construct(
        TaskRepositoryInterface $taskRepositoryInterface,
        RoleRepositoryInterface $roleRepositoryInterface
    ) {
        $this->tasksInterface = $taskRepositoryInterface;
        $this->roleInterface  = $roleRepositoryInterface;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */

    private function viewRoutes()
    {
        $routeCollection = Route::getRoutes();
        echo "<table style='width:100%'>";
        echo "<tr>";
        echo "<td width='10%'><h4>HTTP Method</h4></td>";
        echo "<td width='10%'><h4>Uri</h4></td>";
        echo "<td width='10%'><h4>Name</h4></td>";
        echo "<td width='70%'><h4>Corresponding Action</h4></td>";
        echo "</tr>";
        foreach ($routeCollection as $value) {
            echo "<tr>";
            echo "<td>" . $value->methods[0] . "</td>";
            echo "<td>" . $value->uri . "</td>";
            echo "<td>" . $value->getName() . "</td>";
            echo "<td>" . $value->getActionName() . "</td>";
            echo "</tr>";
        }
        echo "</table>";
    }

    public function index(Request $request)
    {
        $skip = request()->input('skip') ? request()->input('skip') : 0;
        $from = request()->input('from') ? request()->input('from') . " 00:00:01" : Carbon::now()->subMonths(1);
        $to   = request()->input('to') ? request()->input('to') . " 23:59:59" : Carbon::now();

        if (request()->input('q') != '' && (request()->input('from') == '' || request()->input('to') == '')) {
            $list = $this->tasksInterface->searchTask(request()->input('q'), request()->input('state'), $skip * 30);
            $paginate = $this->tasksInterface->countTask(request()->input('q'), request()->input('state'));
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } elseif ((request()->input('q') != '' || request()->input('from') != '' || request()->input('to') != '')) {
            $list = $this->tasksInterface->searchTask(request()->input('q'), request()->input('state'), $skip * 30, $from, $to);
            $paginate = $this->tasksInterface->countTask(request()->input('q'), request()->input('state'), $from, $to);
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } else {
            $paginate = $this->tasksInterface->countTask('');
            $list = $this->tasksInterface->listTasks($skip * 30);
        }

        $paginate = ceil($paginate  / 30);
        $skipPaginate = $skip;

        $pageList = ($skipPaginate + 1) / 5;
        if (is_int($pageList) || $pageList > 1) {
            $countPage = $skipPaginate - 5;
            $maxPage = $skipPaginate + 6 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 6;
        } else {
            $countPage = 0;
            $maxPage = $skipPaginate + 5 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 5;
        }

        foreach ($list as $key => $value) {
            $list[$key]['employee_id'] = $list[$key]->employee->name . ' ' . $list[$key]->employee->last_name . ' / ' . $list[$key]->employee->subsidiary->name;
            $list[$key]['state']       = ($list[$key]['state'] == 0) ? 'Pendiente' : 'Realizada';
        }

        $roles = $this->roleInterface->findRoleByMultipleIds([2, 4]);
        $employees = [];
        foreach ($roles as $key => $role) {
            foreach ($role->user->toArray() as $key => $value) {
                $employees[] = ['id' => $value['id'], 'label' => $value['name'] . ' ' . $value['last_name']];
            }
        }
        $statuses = [
            ['id' => 0, 'label' => 'Pendiente'],
            ['id' => 1, 'label' => 'Terminada']
        ];

        return view('studio::admin.tasks.list', [
            'tasks'           => $list,
            'headers'         => ['id', 'Empleado', 'Tarea', 'Estado', 'Opciones'],
            'inputs'          => [['label' => 'Empleado', 'type' => 'select', 'options' => $employees, 'name' => 'employee_id'], ['label' => 'Tarea', 'type' => 'text', 'name' => 'task']],
            'searchInputs'    => [['label' => 'Buscar', 'type' => 'text', 'name' => 'q'], ['label' => 'Estado', 'type' => 'select', 'options' => $statuses, 'name' => 'state']],
            'routeEdit'       => 'admin.tasks.update',
            'optionsRoutes'   => 'admin.shifts',
            'skip'            => $skip,
            'pag'             => $pageList,
            'i'               => $countPage,
            'max'             => $maxPage,
            'paginate'        => $paginate
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $roles = $this->roleInterface->findRoleByMultipleIds([2, 4]);
        $employees = [];
        foreach ($roles as $key => $role) {
            foreach ($role->user->toArray() as $key => $value) {
                $employees[] = ['id' => $value['id'], 'label' => $value['name'] . ' ' . $value['last_name']];
            }
        }
        return view('studio::admin.tasks.create', [
            'employees' => $employees,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->tasksInterface->createTask($request->input());
        return redirect()->route('admin.tasks.index')->with('message', 'Tarea Creada Exitosamente!');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('studio::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('studio::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->tasksInterface->updateTask($request->input(), $id);
        return redirect()->route('admin.tasks.index')->with('message', 'Tarea Actualizada Exitosamente!');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->tasksInterface->deleteTask($id);
        return redirect()->route('admin.tasks.index')->with('message', 'Tarea Eliminada Exitosamente!');
    }
}
