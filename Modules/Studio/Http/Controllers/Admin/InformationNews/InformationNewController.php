<?php

namespace Modules\Studio\Http\Controllers\Admin\InformationNews;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Studio\Entities\InformationNews\Repositories\Interfaces\InformationNewRepositoryInterface;

class InformationNewController extends Controller
{
    private $informationNewInterface;

    public function __construct(
        InformationNewRepositoryInterface $informationNewRepositoryInterface
    ) {
        $this->informationNewInterface = $informationNewRepositoryInterface;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getNewsForApp()
    {
        $list = $this->informationNewInterface->listInformationNews();

        return $list;
    }

    public function index(Request $request)
    {
        $skip = request()->input('skip') ? request()->input('skip') : 0;
        $from = request()->input('from') ? request()->input('from') . " 00:00:01" : Carbon::now()->subMonths(1);
        $to   = request()->input('to') ? request()->input('to') . " 23:59:59" : Carbon::now();

        if (request()->input('q') != '' && (request()->input('from') == '' || request()->input('to') == '')) {
            $list = $this->informationNewInterface->searchInformationNew(request()->input('q'), $skip * 30);
            $paginate = $this->informationNewInterface->countInformationNew(request()->input('q'));
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } elseif ((request()->input('q') != '' || request()->input('from') != '' || request()->input('to') != '')) {
            $list = $this->informationNewInterface->searchInformationNew(request()->input('q'), $skip * 30, $from, $to);
            $paginate = $this->informationNewInterface->countInformationNew(request()->input('q'), $from, $to);
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } else {
            $paginate = $this->informationNewInterface->countInformationNew('');
            $list = $this->informationNewInterface->listInformationNewsAdmin($skip * 30);
        }

        $paginate = ceil($paginate  / 30);
        $skipPaginate = $skip;

        $pageList = ($skipPaginate + 1) / 5;
        if (is_int($pageList) || $pageList > 1) {
            $countPage = $skipPaginate - 5;
            $maxPage = $skipPaginate + 6 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 6;
        } else {
            $countPage = 0;
            $maxPage = $skipPaginate + 5 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 5;
        }

        return view('studio::admin.informationNews.list', [
            'news' => $list,
            'headers' => ['id', 'Noticia', 'Imagen Noticia', 'Descripción', 'Opciones'],
            'searchInputs'    => [['label' => 'Buscar', 'type' => 'text', 'name' => 'q'], ['label' => 'Desde', 'type' => 'date', 'name' => 'from'], ['label' => 'Hasta', 'type' => 'date', 'name' => 'to']],
            'inputs' => [['label' => 'Noticia', 'type' => 'text', 'name' => 'name'], ['label' => 'Descripción', 'type' => 'textarea', 'name' => 'description']],
            'routeEdit' => 'admin.news.update',
            'optionsRoutes'   => 'admin.news',
            'skip'      => $skip,
            'pag'       => $pageList,
            'i'         => $countPage,
            'max'       => $maxPage,
            'paginate'  => $paginate
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('studio::admin.informationNews.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->informationNewInterface->createInformationNew($request->except('_token', '_method'));

        return redirect()->route('admin.news.index')->with('message', 'Noticia Creada Exitosamente!');
    }

    /**
     * Show the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        return view('studio::show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        return view('studio::edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
    }
}
