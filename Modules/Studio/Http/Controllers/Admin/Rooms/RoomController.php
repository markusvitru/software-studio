<?php

namespace Modules\Studio\Http\Controllers\Admin\Rooms;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Generals\Entities\Tools\ToolRepositoryInterface;
use Modules\Companies\Entities\Subsidiaries\Repositories\Interfaces\SubsidiaryRepositoryInterface;
use Modules\Studio\Entities\Rooms\Repositories\Interfaces\RoomRepositoryInterface;

class RoomController extends Controller
{
    private $roomInterface;
    private $subsidiaryInterface;

    public function __construct(
        RoomRepositoryInterface $roomRepositoryInterface,
        ToolRepositoryInterface $toolRepositoryInterface,
        SubsidiaryRepositoryInterface $subsidiaryRepositoryInterface
    ) {
        $this->roomInterface       = $roomRepositoryInterface;
        $this->toolsInterface      = $toolRepositoryInterface;
        $this->subsidiaryInterface = $subsidiaryRepositoryInterface;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $skip = request()->input('skip') ? request()->input('skip') : 0;
        $from = request()->input('from') ? request()->input('from') . " 00:00:01" : Carbon::now()->subMonths(1);
        $to   = request()->input('to') ? request()->input('to') . " 23:59:59" : Carbon::now();

        if (request()->input('q') != '' && (request()->input('from') == '' || request()->input('to') == '')) {
            $list = $this->roomInterface->searchRoom(request()->input('q'), $skip * 30);
            $paginate = $this->roomInterface->countRoom(request()->input('q'));
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } elseif ((request()->input('q') != '' || request()->input('from') != '' || request()->input('to') != '')) {
            $list = $this->roomInterface->searchRoom(request()->input('q'), $skip * 30, $from, $to);
            $paginate = $this->roomInterface->countRoom(request()->input('q'), $from, $to);
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } else {
            $paginate = $this->roomInterface->countRoom('');
            $list = $this->roomInterface->listRooms($skip * 30);
        }

        $paginate = ceil($paginate  / 30);
        $skipPaginate = $skip;

        $pageList = ($skipPaginate + 1) / 5;
        if (is_int($pageList) || $pageList > 1) {
            $countPage = $skipPaginate - 5;
            $maxPage = $skipPaginate + 6 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 6;
        } else {
            $countPage = 0;
            $maxPage = $skipPaginate + 5 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 5;
        }

        foreach ($list as $key => $value) {
            $list[$key]['subsidiary_id'] = $value->subsidiary->name;
        }

        $subsidiaries = $this->subsidiaryInterface->getAllSubsidiaryNames();
        foreach ($subsidiaries as $key => $subsidiary) {
            $subsidiaries[$key]['label'] = $subsidiary['name'];
        }

        return view('studio::admin.rooms.list', [
            'rooms'          => $list,
            'headers'        => ['id', 'Nombre', 'Sede', 'Opciones'],
            'searchInputs'   => [['label' => 'Buscar', 'type' => 'text', 'name' => 'q'], ['label' => 'Desde', 'type' => 'date', 'name' => 'from'], ['label' => 'Hasta', 'type' => 'date', 'name' => 'to']],
            'inputs'         => [['label' => 'Nombre', 'type' => 'text', 'name' => 'name'], ['label' => 'Sede', 'type' => 'select', 'options' => $subsidiaries, 'name' => 'subsidiary_id']],
            'routeEdit'      => 'admin.rooms.update',
            'optionsRoutes'  => 'admin.rooms',
            'skip'           => $skip,
            'pag'            => $pageList,
            'i'              => $countPage,
            'max'            => $maxPage,
            'paginate'       => $paginate
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $subsidiaries = $this->subsidiaryInterface->getAllSubsidiaryNames();
        foreach ($subsidiaries as $key => $subsidiary) {
            $subsidiaries[$key]['label'] = $subsidiaries[$key]['name'];
        }
        return view('studio::admin.rooms.create', [
            'subsidiaries' => $subsidiaries
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->roomInterface->createRoom($request->input());
        return redirect()->route('admin.rooms.index')->with('message', 'Sala Creada Exitosamente!');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('studio::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('studio::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->roomInterface->updateRoom($request->input(), $id);
        return redirect()->route('admin.rooms.index')->with('message', 'Sala Actualizada Exitosamente!');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->roomInterface->deleteRoom($id);
        return redirect()->route('admin.rooms.index')->with('message', 'Sala Eliminada Exitosamente!');
    }
}
