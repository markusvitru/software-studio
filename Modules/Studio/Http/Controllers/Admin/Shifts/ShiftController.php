<?php

namespace Modules\Studio\Http\Controllers\Admin\Shifts;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Studio\Entities\Shifts\Repositories\Interfaces\ShiftRepositoryInterface;

class ShiftController extends Controller
{
    private $shiftInterface;

    public function __construct(
        ShiftRepositoryInterface $shiftRepositoryInterface
    ) {
        $this->shiftInterface   = $shiftRepositoryInterface;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $skip = request()->input('skip') ? request()->input('skip') : 0;
        $from = request()->input('from') ? request()->input('from') . " 00:00:01" : Carbon::now()->subMonths(1);
        $to   = request()->input('to') ? request()->input('to') . " 23:59:59" : Carbon::now();

        if (request()->input('q') != '' && (request()->input('from') == '' || request()->input('to') == '')) {
            $list = $this->shiftInterface->searchShift(request()->input('q'), $skip * 30);
            $paginate = $this->shiftInterface->countShift(request()->input('q'));
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } elseif ((request()->input('q') != '' || request()->input('from') != '' || request()->input('to') != '')) {
            $list = $this->shiftInterface->searchShift(request()->input('q'), $skip * 30, $from, $to);
            $paginate = $this->shiftInterface->countShift(request()->input('q'), $from, $to);
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } else {
            $paginate = $this->shiftInterface->countShift('');
            $list = $this->shiftInterface->listShifts($skip * 30);
        }

        $paginate = ceil($paginate  / 30);
        $skipPaginate = $skip;

        $pageList = ($skipPaginate + 1) / 5;
        if (is_int($pageList) || $pageList > 1) {
            $countPage = $skipPaginate - 5;
            $maxPage = $skipPaginate + 6 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 6;
        } else {
            $countPage = 0;
            $maxPage = $skipPaginate + 5 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 5;
        }

        return view('studio::admin.shifts.list', [
            'shifts'          => $list,
            'headers'         => ['id', 'Nombre', 'Opciones'],
            'inputs'          => [['label' => 'Nombre', 'type' => 'text', 'name' => 'name']],
            'searchInputs'    => [['label' => 'Buscar', 'type' => 'text', 'name' => 'q'], ['label' => 'Desde', 'type' => 'date', 'name' => 'from'], ['label' => 'Hasta', 'type' => 'date', 'name' => 'to']],
            'routeEdit'       => 'admin.shifts.update',
            'optionsRoutes'   => 'admin.shifts',
            'skip'            => $skip,
            'pag'             => $pageList,
            'i'               => $countPage,
            'max'             => $maxPage,
            'paginate'        => $paginate
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('studio::admin.shifts.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->shiftInterface->createShift($request->input());
        return redirect()->route('admin.shifts.index')->with('message', 'Turno Creado Exitosamente!');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('studio::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('studio::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->shiftInterface->updateShift($request->input(), $id);
        return redirect()->route('admin.shifts.index')->with('message', 'Turno Actualizado Exitosamente!');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->shiftInterface->deleteShift($id);
        return redirect()->route('admin.shifts.index')->with('message', 'Turno Eliminado Exitosamente!');
    }
}
