<?php

namespace Modules\Studio\Http\Controllers\Admin\Penalties;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Studio\Entities\Penalties\Repositories\Interfaces\PenaltyRepositoryInterface;

class PenaltyController extends Controller
{
    private $penaltyInterface;

    public function __construct(
        PenaltyRepositoryInterface $penaltyRepositoryInterface
    ) {
        $this->penaltyInterface   = $penaltyRepositoryInterface;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $skip = request()->input('skip') ? request()->input('skip') : 0;
        $from = request()->input('from') ? request()->input('from') . " 00:00:01" : Carbon::now()->subMonths(1);
        $to   = request()->input('to') ? request()->input('to') . " 23:59:59" : Carbon::now();

        if (request()->input('q') != '' && (request()->input('from') == '' || request()->input('to') == '')) {
            $list = $this->penaltyInterface->searchPenalty(request()->input('q'), $skip * 30);
            $paginate = $this->penaltyInterface->countPenalty(request()->input('q'));
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } elseif ((request()->input('q') != '' || request()->input('from') != '' || request()->input('to') != '')) {
            $list = $this->penaltyInterface->searchPenalty(request()->input('q'), $skip * 30, $from, $to);
            $paginate = $this->penaltyInterface->countPenalty(request()->input('q'), $from, $to);
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } else {
            $paginate = $this->penaltyInterface->countPenalty('');
            $list = $this->penaltyInterface->listPenalties($skip * 30);
        }

        $paginate = ceil($paginate  / 30);
        $skipPaginate = $skip;

        $pageList = ($skipPaginate + 1) / 5;
        if (is_int($pageList) || $pageList > 1) {
            $countPage = $skipPaginate - 5;
            $maxPage = $skipPaginate + 6 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 6;
        } else {
            $countPage = 0;
            $maxPage = $skipPaginate + 5 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 5;
        }

        return view('studio::admin.penalties.list', [
            'penalties' => $list,
            'headers'   => ['id', 'Descripcion', 'Valor', 'Opciones'],
            'searchInputs' => [['label' => 'Buscar', 'type' => 'text', 'name' => 'q'], ['label' => 'Desde', 'type' => 'date', 'name' => 'from'], ['label' => 'Hasta', 'type' => 'date', 'name' => 'to']],
            'inputs'    => [['label' => 'Descripcion', 'type' => 'text', 'name' => 'description'], ['label' => 'Costo', 'type' => 'number', 'name' => 'cost']],
            'routeEdit' => 'admin.penalties.update',
            'optionsRoutes'   => 'admin.penalties',
            'skip'      => $skip,
            'pag'       => $pageList,
            'i'         => $countPage,
            'max'       => $maxPage,
            'paginate'  => $paginate
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('studio::admin.penalties.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->penaltyInterface->createPenalty($request->input());

        return redirect()->route('admin.penalties.index')->with('message', 'Multa Creada Exitosamente!');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('studio::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('studio::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->penaltyInterface->updatePenalty($request->input(), $id);

        return redirect()->route('admin.penalties.index')->with('message', 'Multa Actualizada Exitosamente!');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->penaltyInterface->deletePenalty($id);

        return redirect()->route('admin.penalties.index')->with('message', 'Multa Actualizada Exitosamente!');
    }
}
