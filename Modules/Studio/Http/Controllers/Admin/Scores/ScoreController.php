<?php

namespace Modules\Studio\Http\Controllers\Admin\Scores;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Companies\Entities\Roles\Repositories\Interfaces\RoleRepositoryInterface;
use Modules\Studio\Entities\Scores\Repositories\Interfaces\ScoreRepositoryInterface;

class ScoreController extends Controller
{
    private $scoreInterface;
    private $roleInterface;

    public function __construct(
        ScoreRepositoryInterface $scoreRepositoryInterface,
        RoleRepositoryInterface $roleRepositoryInterface
    ) {
        $this->scoreInterface = $scoreRepositoryInterface;
        $this->roleInterface  = $roleRepositoryInterface;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $skip = request()->input('skip') ? request()->input('skip') : 0;
        $from = request()->input('from') ? request()->input('from') . " 00:00:01" : Carbon::now()->subMonths(1);
        $to   = request()->input('to') ? request()->input('to') . " 23:59:59" : Carbon::now();

        if (request()->input('q') != '' && (request()->input('from') == '' || request()->input('to') == '')) {
            $list = $this->scoreInterface->searchScore(request()->input('q'), $skip * 30);
            $paginate = $this->scoreInterface->countScore(request()->input('q'));
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } elseif ((request()->input('q') != '' || request()->input('from') != '' || request()->input('to') != '')) {
            $list = $this->scoreInterface->searchScore(request()->input('q'), $skip * 30, $from, $to);
            $paginate = $this->scoreInterface->countScore(request()->input('q'), $from, $to);
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } else {
            $paginate = $this->scoreInterface->countScore('');
            $list = $this->scoreInterface->listScores($skip * 30);
        }

        $paginate = ceil($paginate  / 30);
        $skipPaginate = $skip;

        $pageList = ($skipPaginate + 1) / 5;
        if (is_int($pageList) || $pageList > 1) {
            $countPage = $skipPaginate - 5;
            $maxPage = $skipPaginate + 6 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 6;
        } else {
            $countPage = 0;
            $maxPage = $skipPaginate + 5 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 5;
        }

        $options = [];
        for ($i = 5; $i <= 100; $i = $i + 5) {
            $options[] = ['label' => $i . '%', 'id' => $i];
        }
        foreach ($list as $key => $value) {
            $list[$key]['employee_id']          = $list[$key]->employee->name . ' ' . $list[$key]->employee->last_name . ' / ' . $list[$key]->employee->subsidiary->name;
            $list[$key]['average']              = round(($list[$key]['personal_presentation'] + $list[$key]['clothing'] + $list[$key]['hair'] + $list[$key]['hand_nails'] + $list[$key]['toe_nails'] + $list[$key]['makeup'] + $list[$key]['body_expression'] + $list[$key]['oral_expression']) / 8, 2) . '%';
            $list[$key]['have_social_networks'] = ($list[$key]['have_social_networks'] == 0) ? 'No' : 'Si';
            $list[$key]['date']                 = $list[$key]['created_at'];
            unset($list[$key]['personal_presentation'], $list[$key]['clothing'], $list[$key]['hair'], $list[$key]['hand_nails'], $list[$key]['toe_nails'], $list[$key]['makeup'], $list[$key]['body_expression'], $list[$key]['oral_expression'], $list[$key]['created_at'], $list[$key]['observation']);
        }
        $role      = $this->roleInterface->findRoleById(3);
        $employees = [];
        foreach ($role->user->toArray() as $key => $value) {
            $employees[] = ['id' => $value['id'], 'label' => $value['name'] . ' ' . $value['last_name']];
        }
        return view('studio::admin.scores.list', [
            'scores'    => $list,
            'headers'   => ['id', 'Empleado', 'Tiene redes sociales', 'Promedio', 'Fecha', 'Opciones'],
            'inputs'    => [
                ['name' => 'employee_id', 'type' => 'select', 'label' => 'Empleado', 'options' => $employees],
                ['name' => 'personal_presentation', 'type' => 'select', 'label' => 'Presentación personal', 'options' => $options],
                ['name' => 'clothing', 'type' => 'select', 'label' => 'Vestuario', 'options' => $options],
                ['name' => 'hair', 'type' => 'select', 'label' => 'Cabello', 'options' => $options],
                ['name' => 'hand_nails', 'type' => 'select', 'label' => 'Uñas de las manos', 'options' => $options],
                ['name' => 'toe_nails', 'type' => 'select', 'label' => 'Uñas de los pies', 'options' => $options],
                ['name' => 'makeup', 'type' => 'select', 'label' => 'Maquillaje', 'options' => $options],
                ['name' => 'body_expression', 'type' => 'select', 'label' => 'Expresión corporal', 'options' => $options],
                ['name' => 'oral_expression', 'type' => 'select', 'label' => 'Expresión oral', 'options' => $options],
                ['name' => 'have_social_networks', 'type' => 'select', 'label' => 'Tiene redes sociales', 'options' => [['id' => 0, 'label' => 'No'], ['id' => 1, 'label' => 'Si']]],
                ['name' => 'observation', 'type' => 'textarea', 'label' => 'Observación']
            ],
            'searchInputs'    => [['label' => 'Buscar', 'type' => 'text', 'name' => 'q'], ['label' => 'Desde', 'type' => 'date', 'name' => 'from'], ['label' => 'Hasta', 'type' => 'date', 'name' => 'to']],
            'routeEdit' =>  'admin.scores.update',
            'optionsRoutes'   => 'admin.scores',
            'skip'            => $skip,
            'pag'             => $pageList,
            'i'               => $countPage,
            'max'             => $maxPage,
            'paginate'        => $paginate
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $role = $this->roleInterface->findRoleById(3);
        $employees = [];
        foreach ($role->user->toArray() as $key => $value) {
            $employees[] = ['id' => $value['id'], 'label' => $value['name'] . ' ' . $value['last_name']];
        }
        return view('studio::admin.scores.create', [
            'employees' => $employees,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->scoreInterface->createScore($request->input());
        return redirect()->route('admin.scores.index')->with('message', 'Puntuación Creada Exitosamente!');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $score = $this->scoreInterface->findScoreById($id);
        $score['have_social_networks'] = ($score['have_social_networks'] == 0) ? 'No' : 'Si';
        $score['average']              = round(($score['personal_presentation'] + $score['clothing'] + $score['hair'] + $score['hand_nails'] + $score['toe_nails'] + $score['makeup'] + $score['body_expression'] + $score['oral_expression']) / 8, 2) . '%';
        return view('studio::admin.scores.show', [
            'score' => $score
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('studio::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->scoreInterface->updateScore($request->input(), $id);
        return redirect()->route('admin.scores.index')->with('message', 'Puntuación Actualizada Exitosamente!');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->scoreInterface->deleteScore($id);
        return redirect()->route('admin.scores.index')->with('message', 'Puntuación Eliminada Exitosamente!');
    }
}
