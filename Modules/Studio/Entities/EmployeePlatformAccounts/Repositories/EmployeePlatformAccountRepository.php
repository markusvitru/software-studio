<?php

namespace Modules\Studio\Entities\EmployeePlatformAccounts\Repositories;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Modules\Studio\Entities\EmployeePlatformAccounts\EmployeePlatformAccount;
use Modules\Studio\Entities\EmployeePlatformAccounts\Repositories\Interfaces\EmployeePlatformAccountRepositoryInterface;

class EmployeePlatformAccountRepository implements EmployeePlatformAccountRepositoryInterface
{
    private $columns = [
        'id',
        'employee_id',
        'platform_id',
        'user',
        'password',
        'created_at'
    ];

    public function __construct(EmployeePlatformAccount $employeePlatformAccount)
    {
        $this->model = $employeePlatformAccount;
    }

    public function listEmployeePlatformAccounts(int $totalView): Collection
    {
        try {
            return  $this->model->orderBy('id', 'asc')
                ->skip($totalView)->take(30)
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function searchEmployeePlatformAccount(string $text = null, int $totalView, $from = null, $to = null): Collection
    {
        try {
            if (is_null($text) && is_null($from) && is_null($to)) {
                return $this->listEmployeePlatformAccounts($totalView);
            }

            if (!is_null($text) && (is_null($from) || is_null($to))) {
                return $this->model->searchEmployeePlatformAccount($text, null, true, true)
                    ->skip($totalView)
                    ->take(30)
                    ->get($this->columns);
            }

            if (is_null($text) && (!is_null($from) || !is_null($to))) {
                return $this->model->whereBetween('created_at', [$from, $to])
                    ->skip($totalView)
                    ->take(30)
                    ->get($this->columns);
            }

            return $this->model->searchEmployeePlatformAccount($text, null, true, true)
                ->whereBetween('created_at', [$from, $to])
                ->orderBy('created_at', 'desc')
                ->skip($totalView)
                ->take(30)
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function countEmployeePlatformAccount(string $text = null,  $from = null, $to = null)
    {
        try {
            if (is_null($text) && is_null($from) && is_null($to)) {
                $data =  $this->model->get(['id']);
                return count($data);
            }

            if (!is_null($text) && (is_null($from) || is_null($to))) {
                $data =  $this->model->searchEmployeePlatformAccount($text, null, true, true)
                    ->get(['id']);
                return count($data);
            }

            if (is_null($text) && (!is_null($from) || !is_null($to))) {
                $data =  $this->model->whereBetween('created_at', [$from, $to])
                    ->get(['id', 'created_at']);
                return count($data);
            }

            $data =  $this->model->searchEmployeePlatformAccount($text, null, true, true)
                ->whereBetween('created_at', [$from, $to])
                ->get(['id', 'created_at']);
            return count($data);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function searchTrashedEmployeePlatformAccount(string $text = null): Collection
    {
        try {
            return  $this->model
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function createEmployeePlatformAccount(array $data): EmployeePlatformAccount
    {
        try {
            return $this->model->create($data);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findEmployeePlatformAccountById(int $id): EmployeePlatformAccount
    {
        try {
            return $this->model->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findTrashedEmployeePlatformAccountById(int $id): EmployeePlatformAccount
    {
        try {
            return $this->model->withTrashed()->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function updateEmployeePlatformAccount(array $params, $id): bool
    {
        $employeePlatformAccount = $this->findEmployeePlatformAccountById($id);
        try {
            return $employeePlatformAccount->update($params);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function deleteEmployeePlatformAccount($id): bool
    {
        $employeePlatformAccount = $this->findEmployeePlatformAccountById($id);
        try {
            return $employeePlatformAccount->delete();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function recoverTrashedEmployeePlatformAccount($id): bool
    {
        $employeePlatformAccount = $this->findEmployeePlatformAccountById($id);
        try {
            return $employeePlatformAccount->restore();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }
}
