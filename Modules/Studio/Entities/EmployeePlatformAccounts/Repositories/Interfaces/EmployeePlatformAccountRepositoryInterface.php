<?php

namespace Modules\Studio\Entities\EmployeePlatformAccounts\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Modules\Studio\Entities\EmployeePlatformAccounts\EmployeePlatformAccount;

interface EmployeePlatformAccountRepositoryInterface
{
    public function listEmployeePlatformAccounts(int $totalView): Collection;

    public function searchEmployeePlatformAccount(string $text = null, int $totalView, $from = null, $to = null): Collection;

    public function countEmployeePlatformAccount(string $text = null,  $from = null, $to = null);

    public function searchTrashedEmployeePlatformAccount(string $text = null): Collection;

    public function createEmployeePlatformAccount(array $data): EmployeePlatformAccount;

    public function findEmployeePlatformAccountById(int $id): EmployeePlatformAccount;

    public function findTrashedEmployeePlatformAccountById(int $id): EmployeePlatformAccount;

    public function updateEmployeePlatformAccount(array $params, $id): bool;

    public function deleteEmployeePlatformAccount($id): bool;

    public function recoverTrashedEmployeePlatformAccount($id): bool;
}
