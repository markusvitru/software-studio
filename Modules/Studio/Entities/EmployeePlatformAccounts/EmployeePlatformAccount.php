<?php

namespace Modules\Studio\Entities\EmployeePlatformAccounts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Modules\Companies\Entities\Employees\Employee;
use Modules\Studio\Entities\Platforms\Platform;
use Nicolaslopezj\Searchable\SearchableTrait;

class EmployeePlatformAccount extends Model
{
    use Notifiable, SoftDeletes, SearchableTrait;

    protected $table = 'employee_platform_accounts';

    protected $fillable = [
        'id',
        'employee_id',
        'platform_id',
        'user',
        'password',
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $hidden = [
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates  = [
        'deleted_at',
        'created_at',
        'updated_at',

    ];

    protected $searchable = [
        'columns' => [
            'employee_platform_accounts.user'   => 10,
            'employees.name'                    => 10,
            'employees.last_name'               => 10,
            'subsidiaries.name'                 => 10,
            'platforms.name'                    => 10
        ],
        'joins' => [
            'employees'     => ['employee_platform_accounts.employee_id', 'employees.id'],
            'subsidiaries'  => ['employees.subsidiary_id', 'subsidiaries.id'],
            'platforms'     => ['employee_platform_accounts.platform_id', 'platforms.id'],
        ],
    ];

    public function searchEmployeePlatformAccount($term)
    {
        return self::search($term);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function platform()
    {
        return $this->belongsTo(Platform::class);
    }
}
