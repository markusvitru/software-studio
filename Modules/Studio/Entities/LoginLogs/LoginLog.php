<?php

namespace Modules\Studio\Entities\LoginLogs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Modules\Companies\Entities\Employees\Employee;
use Nicolaslopezj\Searchable\SearchableTrait;

class LoginLog extends Model
{
    use Notifiable, SoftDeletes, SearchableTrait;

    protected $table = 'login_logs';

    protected $fillable = [
        'id',
        'employee_id',
        'type',
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $hidden = [
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates  = [
        'deleted_at',
        'created_at',
        'updated_at',

    ];

    protected $searchable = [];

    public function user()
    {
        return $this->belongsTo(Employee::class);
    }
}
