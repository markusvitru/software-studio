<?php

namespace Modules\Studio\Entities\LoginLogs\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Modules\Studio\Entities\LoginLogs\LoginLog;

interface LoginLogRepositoryInterface
{
    public function listLoginLogs(int $totalView);

    public function searchLoginlog(string $text = null): Collection;

    public function searchTrashedLoginLog(string $text = null): Collection;

    public function createLoginLog(array $data): LoginLog;

    public function findLoginLogById(int $id): LoginLog;

    public function findTrashedLoginLogById(int $id): LoginLog;

    public function updateLoginLog(array $params): bool;

    public function deleteLoginLog(): bool;

    public function recoverTrashedLoginLog(): bool;
}
