<?php

namespace Modules\Studio\Entities\LoginLogs\Repositories;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Modules\Studio\Entities\LoginLogs\Repositories\Interfaces\LoginLogRepositoryInterface;
use Modules\Studio\Entities\LoginLogs\LoginLog;

class LoginLogRepository implements LoginLogRepositoryInterface
{
    private $columns = ['id', 'employee_id', 'type'];

    public function __construct(LoginLog $loginLog)
    {
        $this->model = $loginLog;
    }

    public function listLoginLogs(int $totalView)
    {
        try {
            return  $this->model
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function searchLoginlog(string $text = null): Collection
    {
        try {
            return  $this->model
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function searchTrashedLoginLog(string $text = null): Collection
    {
        try {
            return  $this->model
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function createLoginLog(array $data): LoginLog
    {
        try {
            return $this->model->create($data);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findLoginLogById(int $id): LoginLog
    {
        try {
            return $this->model->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findTrashedLoginLogById(int $id): LoginLog
    {
        try {
            return $this->model->withTrashed()->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function updateLoginLog(array $params): bool
    {
        try {
            return $this->model->update($params);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function deleteLoginLog(): bool
    {
        try {
            return $this->model->delete();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function recoverTrashedLoginLog(): bool
    {
        try {
            return $this->model->restore();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }
}
