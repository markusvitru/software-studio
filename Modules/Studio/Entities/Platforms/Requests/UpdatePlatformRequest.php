<?php

namespace Modules\Studio\Entities\Platforms\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePlatformRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'name'       => ['required', 'max: 100', 'bail'],
            'token_cost' => ['required', 'bail'],
        ];
    }
}
