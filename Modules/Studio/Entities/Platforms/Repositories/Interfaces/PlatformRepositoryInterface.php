<?php

namespace Modules\Studio\Entities\Platforms\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Modules\Studio\Entities\Platforms\Platform;

interface PlatformRepositoryInterface
{
    public function listPlatforms(int $totalView): Collection;

    public function listAllPlatforms($platforms = []);

    public function searchPlatform(string $text = null, int $totalView, $from = null, $to = null): Collection;

    public function countPlatform(string $text = null,  $from = null, $to = null);

    public function createPlatform(array $data): Platform;

    public function findPlatformById(int $id): Platform;

    public function findTrashedPlatformById(int $id): Platform;

    public function updatePlatform(array $params, $id): bool;

    public function deletePlatform($id): bool;

    public function recoverTrashedPlatform(): bool;
}
