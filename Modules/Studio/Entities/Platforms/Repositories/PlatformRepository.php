<?php

namespace Modules\Studio\Entities\Platforms\Repositories;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Modules\Studio\Entities\Platforms\Platform;
use Modules\Studio\Entities\Platforms\Repositories\Interfaces\PlatformRepositoryInterface;

class PlatformRepository implements PlatformRepositoryInterface
{
    private $columns = [
        'id',
        'name',
        'type',
        'token_cost'
    ];

    public function __construct(Platform $platform)
    {
        $this->model = $platform;
    }

    public function listPlatforms(int $totalView): Collection
    {
        try {
            return  $this->model
                ->skip($totalView)
                ->take(30)
                ->get($this->columns);;
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function listAllPlatforms($platforms = [])
    {
        try {
            return  $this->model->whereNotIn('id', $platforms)
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function searchPlatform(string $text = null, int $totalView, $from = null, $to = null): Collection
    {
        try {
            if (is_null($text) && is_null($from) && is_null($to)) {
                return $this->listPlatforms($totalView);
            }

            if (!is_null($text) && (is_null($from) || is_null($to))) {
                return $this->model->searchPlatform($text, null, true, true)
                    ->skip($totalView)
                    ->take(30)
                    ->get($this->columns);
            }

            if (is_null($text) && (!is_null($from) || !is_null($to))) {
                return $this->model->whereBetween('created_at', [$from, $to])
                    ->skip($totalView)
                    ->take(30)
                    ->get($this->columns);
            }

            return $this->model->searchPlatform($text, null, true, true)
                ->whereBetween('created_at', [$from, $to])
                ->orderBy('created_at', 'desc')
                ->skip($totalView)
                ->take(30)
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function countPlatform(string $text = null,  $from = null, $to = null)
    {
        try {
            if (is_null($text) && is_null($from) && is_null($to)) {
                $data =  $this->model->get(['id']);
                return count($data);
            }

            if (!is_null($text) && (is_null($from) || is_null($to))) {
                $data =  $this->model->searchPlatform($text, null, true, true)
                    ->get(['id']);
                return count($data);
            }

            if (is_null($text) && (!is_null($from) || !is_null($to))) {
                $data =  $this->model->whereBetween('created_at', [$from, $to])
                    ->get(['id']);
                return count($data);
            }

            $data =  $this->model->searchPlatform($text, null, true, true)
                ->whereBetween('created_at', [$from, $to])
                ->get(['id']);
            return count($data);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function createPlatform(array $data): Platform
    {
        try {
            return $this->model->create($data);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findPlatformById(int $id): Platform
    {
        try {
            return $this->model->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findTrashedPlatformById(int $id): Platform
    {
        try {
            return $this->model->withTrashed()->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function updatePlatform(array $params, $id): bool
    {
        $platform = $this->findPlatformById($id);
        try {
            return $platform->update($params);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function deletePlatform($id): bool
    {
        $platform = $this->findPlatformById($id);
        try {
            return $platform->delete();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function recoverTrashedPlatform(): bool
    {
        try {
            return $this->model->restore();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }
}
