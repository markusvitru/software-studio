<?php

namespace Modules\Studio\Entities\Platforms;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Modules\Companies\Entities\Employees\Employee;
use Modules\Studio\Entities\Sales\Sale;
use Nicolaslopezj\Searchable\SearchableTrait;

class Platform extends Model
{
    use Notifiable, SoftDeletes, SearchableTrait;

    protected $table = 'platforms';

    protected $fillable = [
        'id',
        'name',
        'type',
        'token_cost',
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $hidden = [
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates  = [
        'deleted_at',
        'created_at',
        'updated_at',

    ];

    protected $searchable = [
        'columns' => [
            'platforms.name'       => 10,
            'platforms.type'       => 10,
            'platforms.token_cost' => 10
        ]
    ];

    public function searchPlatform($term)
    {
        return self::search($term);
    }

    public function sales()
    {
        return $this->belongsToMany(Sale::class);
    }

    /**
     * The adminSales that belong to the Platform
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function adminSales(): BelongsToMany
    {
        return $this->belongsToMany(adminSales::class);
    }

    public function users()
    {
        return $this->belongsToMany(Employee::class);
    }

    public function employeePlatformAccounts()
    {
        return $this->hasMany(EmployeePlatformAccount::class);
    }
}
