<?php

namespace Modules\Studio\Entities\SendingPhotos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Modules\Companies\Entities\Employees\Employee;
use Modules\Studio\Entities\Photos\Photo;
use Nicolaslopezj\Searchable\SearchableTrait;

class SendingPhoto extends Model
{
    use Notifiable, SoftDeletes, SearchableTrait;

    protected $table = 'sending_photos';

    protected $fillable = [
        'id',
        'employee_id',
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $hidden = [
        'deleted_at',
        'updated_at',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates  = [
        'deleted_at',
        'created_at',
        'updated_at',

    ];

    protected $searchable = [
        'columns' => [
            'employees.name'        => 10,
            'employees.last_name'   => 10
        ],
        'joins' => [
            'employees' => ['sending_photos.employee_id', 'employees.id'],
        ]
    ];

    public function searchSendingPhoto($term)
    {
        return self::search($term);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function photos()
    {
        return $this->hasMany(Photo::class);
    }
}
