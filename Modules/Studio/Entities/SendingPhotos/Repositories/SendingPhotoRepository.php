<?php

namespace Modules\Studio\Entities\SendingPhotos\Repositories;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Modules\Studio\Entities\SendingPhotos\Repositories\Interfaces\SendingPhotoRepositoryInterface;
use Modules\Studio\Entities\SendingPhotos\SendingPhoto;
use Modules\Generals\Entities\Tools\UploadableTrait;

class SendingPhotoRepository implements SendingPhotoRepositoryInterface
{
    use UploadableTrait;
    private $columns = ['id', 'employee_id', 'created_at'];

    public function __construct(SendingPhoto $sendingPhoto)
    {
        $this->model = $sendingPhoto;
    }

    public function listSendingPhotos(int $totalView): Collection
    {
        try {
            return  $this->model
                ->skip($totalView)
                ->take(30)
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function searchSendingPhoto(string $text = null, int $totalView, $from = null, $to = null): Collection
    {
        try {
            if (is_null($text) && is_null($from) && is_null($to)) {
                return $this->listSendingPhotos($totalView);
            }

            if (!is_null($text) && (is_null($from) || is_null($to))) {
                return $this->model->searchSendingPhoto($text, null, true, true)
                    ->skip($totalView)
                    ->take(30)
                    ->get($this->columns);
            }

            if (is_null($text) && (!is_null($from) || !is_null($to))) {
                return $this->model->whereBetween('created_at', [$from, $to])
                    ->skip($totalView)
                    ->take(30)
                    ->get($this->columns);
            }

            return $this->model->searchSendingPhoto($text, null, true, true)
                ->whereBetween('created_at', [$from, $to])
                ->orderBy('created_at', 'desc')
                ->skip($totalView)
                ->take(30)
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function countSendingPhoto(string $text = null,  $from = null, $to = null)
    {
        try {
            if (is_null($text) && is_null($from) && is_null($to)) {
                $data =  $this->model->get(['id']);
                return count($data);
            }

            if (!is_null($text) && (is_null($from) || is_null($to))) {
                $data =  $this->model->searchSendingPhoto($text, null, true, true)
                    ->get(['id']);
                return count($data);
            }

            if (is_null($text) && (!is_null($from) || !is_null($to))) {
                $data =  $this->model->whereBetween('created_at', [$from, $to])
                    ->get(['id']);
                return count($data);
            }

            $data =  $this->model->searchSendingPhoto($text, null, true, true)
                ->whereBetween('created_at', [$from, $to])
                ->get(['id']);
            return count($data);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function searchTrashedSendingPhoto(string $text = null): Collection
    {
        try {
            return  $this->model
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function createSendingPhoto(array $data): SendingPhoto
    {
        try {
            if (isset($params['img']) && ($params['img'] instanceof UploadedFile)) {
                $img = $this->uploadOne($params['img'], 'sendingphoto');
            }
            $sendingPhoto = new SendingPhoto();
            $sendingPhoto->save();
            return $sendingPhoto;
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findSendingPhotokById(int $id): SendingPhoto
    {
        try {
            return $this->model->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findTrashedSendingPhotoById(int $id): SendingPhoto
    {
        try {
            return $this->model->withTrashed()->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function updateSendingPhoto(array $params): bool
    {
        try {
            return $this->model->update($params);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function deleteSendingPhoto(): bool
    {
        try {
            return $this->model->delete();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function recoverTrashedSendingPhoto(): bool
    {
        try {
            return $this->model->restore();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }
}
