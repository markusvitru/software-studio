<?php

namespace Modules\Studio\Entities\SendingPhotos\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Modules\Studio\Entities\SendingPhotos\SendingPhoto;

interface SendingPhotoRepositoryInterface
{
    public function listSendingPhotos(int $totalView): Collection;

    public function searchSendingPhoto(string $text = null, int $totalView, $from = null, $to = null): Collection;

    public function countSendingPhoto(string $text = null,  $from = null, $to = null);

    public function searchTrashedSendingPhoto(string $text = null): Collection;

    public function createSendingPhoto(array $data): SendingPhoto;

    public function findSendingPhotokById(int $id): SendingPhoto;

    public function findTrashedSendingPhotoById(int $id): SendingPhoto;

    public function updateSendingPhoto(array $params): bool;

    public function deleteSendingPhoto(): bool;

    public function recoverTrashedSendingPhoto(): bool;
}
