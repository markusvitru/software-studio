<?php

namespace Modules\Studio\Entities\Goals;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Modules\Companies\Entities\Employees\Employee;
use Nicolaslopezj\Searchable\SearchableTrait;

class Goal extends Model
{
    use Notifiable, SoftDeletes, SearchableTrait;

    protected $table = 'goals';

    protected $fillable = [
        'id',
        'total',
        'month',
        'fortnight',
        'employee_id',
        'percentage_completed',
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $hidden = [
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates  = [
        'deleted_at',
        'created_at',
        'updated_at',

    ];

    protected $searchable = [];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
