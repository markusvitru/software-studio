<?php

namespace Modules\Studio\Entities\Goals\Repositories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Modules\Companies\Entities\Employees\Repositories\Interfaces\EmployeeRepositoryInterface;
use Modules\Studio\Entities\Goals\Goal;
use Modules\Studio\Entities\Goals\Repositories\Interfaces\GoalRepositoryInterface;

class GoalRepository implements GoalRepositoryInterface
{
    private $columns = [
        'id',
        'employee_id',
        'total', 'month',
        'fortnight',
        'percentage_completed',
        'created_at'
    ];

    private $employeeInterface;
    public function __construct(Goal $goal, EmployeeRepositoryInterface $employeeRepositoryInterface)
    {
        $this->model = $goal;
        $this->employeeInterface = $employeeRepositoryInterface;
    }

    public function getGoalForEmployee($id)
    {
        $employee = $this->employeeInterface->findEmployeeById($id);
        $date = Carbon::now();
        $day = $date->isoFormat('D');
        $month = $date->isoFormat('M');
        $fortnight = ($day >= 1 && $day <= 15) ? '0' : '1';

        return $employee->goals->where('month', $month)->where('fortnight', $fortnight);
    }

    public function listGoals(): Collection
    {
        try {
            return  $this->model
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function listGoalsPerEmployee($employee_id): Collection
    {
        try {
            return  $this->model->where('employee_id', $employee_id)
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function createGoalForModel($total, $employee_id)
    {
        $date = Carbon::now();
        $day = $date->isoFormat('D');
        $month = $date->isoFormat('M');
        $fortnight = ($day >= 1 && $day <= 15) ? '0' : '1';
        $data = ['employee_id' => $employee_id, 'total' => $total, 'month' => $month, 'fortnight' => $fortnight];

        return $this->createGoal($data);
    }

    public function createGoal(array $data): Goal
    {
        try {
            return $this->model->create($data);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function updateGoal(array $params, $id): bool
    {
        $goal = $this->findGoalById($id);
        try {
            return $goal->update($params);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function deleteGoal($id): bool
    {
        $goal = $this->findGoalById($id);
        try {
            return $goal->delete();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findGoalById(int $id): Goal
    {
        try {
            return $this->model->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function recoverTrashedGoal($id): bool
    {
        $goal = $this->findGoalById($id);
        try {
            return $goal->restore();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findGoalByModelAndMonthAndFortnight($employee_id, $month, $fortnight)
    {
        try {
            return  $this->model->where('employee_id', $employee_id)->where('month', $month)->where('fortnight', $fortnight)
                ->get($this->columns);
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }
}
