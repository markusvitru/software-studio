<?php

namespace Modules\Studio\Entities\Goals\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Modules\Studio\Entities\Goals\Goal;

interface GoalRepositoryInterface
{
    public function getGoalForEmployee($id);

    public function listGoals(): Collection;

    public function listGoalsPerEmployee($employee_id): Collection;

    public function createGoalForModel($total, $employee_id);

    public function createGoal(array $data): Goal;

    public function updateGoal(array $params, $id): bool;

    public function deleteGoal($id): bool;

    public function findGoalById(int $id): Goal;

    public function recoverTrashedGoal($id): bool;

    public function findGoalByModelAndMonthAndFortnight($employee_id, $month, $fortnight);
}
