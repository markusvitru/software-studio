<?php

namespace Modules\Studio\Entities\InformationNews\Repositories;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Modules\Generals\Entities\Tools\UploadableTrait;
use Modules\Studio\Entities\InformationNews\InformationNew;
use Modules\Studio\Entities\InformationNews\Repositories\Interfaces\InformationNewRepositoryInterface;

class InformationNewRepository implements InformationNewRepositoryInterface
{
    use UploadableTrait;
    protected $model;
    private $columns = ['id', 'name', 'img', 'description'];

    public function __construct(InformationNew $informationNew)
    {
        $this->model = $informationNew;
    }

    public function listInformationNews()
    {
        try {
            return  $this->model
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function listInformationNewsAdmin(int $totalView): Collection
    {
        try {
            return  $this->model
                ->orderBy('id', 'asc')
                ->skip($totalView)->take(30)
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function searchInformationNew(string $text = null, int $totalView, $from = null, $to = null): Collection
    {
        try {
            if (is_null($text) && is_null($from) && is_null($to)) {
                return $this->listInformationNewsAdmin($totalView);
            }

            if (!is_null($text) && (is_null($from) || is_null($to))) {
                return $this->model->searchInformationNew($text, null, true, true)
                    ->skip($totalView)
                    ->take(30)
                    ->get($this->columns);
            }

            if (is_null($text) && (!is_null($from) || !is_null($to))) {
                return $this->model->whereBetween('created_at', [$from, $to])
                    ->skip($totalView)
                    ->take(30)
                    ->get($this->columns);
            }

            return $this->model->searchInformationNew($text, null, true, true)
                ->whereBetween('created_at', [$from, $to])
                ->orderBy('created_at', 'desc')
                ->skip($totalView)
                ->take(30)
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function countInformationNew(string $text = null,  $from = null, $to = null)
    {
        try {
            if (is_null($text) && is_null($from) && is_null($to)) {
                $data =  $this->model->get(['id']);
                return count($data);
            }

            if (!is_null($text) && (is_null($from) || is_null($to))) {
                $data =  $this->model->searchInformationNew($text, null, true, true)
                    ->get(['id']);
                return count($data);
            }

            if (is_null($text) && (!is_null($from) || !is_null($to))) {
                $data =  $this->model->whereBetween('created_at', [$from, $to])
                    ->get(['id', 'created_at']);
                return count($data);
            }

            $data =  $this->model->searchInformationNew($text, null, true, true)
                ->whereBetween('created_at', [$from, $to])
                ->get(['id', 'created_at']);
            return count($data);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function searchTrashedInformationNew(string $text = null): Collection
    {
        try {
            return  $this->model
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function createInformationNew(array $params): InformationNew
    {
        try {
            $collection = collect($params);
            if (isset($params['name'])) {
                $slug = str_slug($params['name']);
            }
            if (isset($params['img']) && ($params['img'] instanceof UploadedFile)) {
                $img = $this->uploadOne($params['img'], 'news');
            }
            $merge = $collection->merge(compact('img'));
            $informationNew = new InformationNew($merge->all());
            $informationNew->save();

            return $informationNew;
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findInformationNewById(int $id): InformationNew
    {
        try {
            return $this->model->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findTrashedInformationNewById(int $id): InformationNew
    {
        try {
            return $this->model->withTrashed()->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function updateInformationNew(array $params, $id): bool
    {
        $shift = $this->findInformationNewById($id);
        try {
            return $shift->update($params);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function deleteInformationNew($id): bool
    {
        $shift = $this->findInformationNewById($id);
        try {
            return $shift->delete();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function recoverTrashedInformationNew(): bool
    {
        try {
            return $this->model->restore();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }
}
