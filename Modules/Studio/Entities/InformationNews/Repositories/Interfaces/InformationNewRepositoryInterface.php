<?php

namespace Modules\Studio\Entities\InformationNews\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Modules\Studio\Entities\InformationNews\InformationNew;

interface InformationNewRepositoryInterface
{
    public function listInformationNews();

    public function listInformationNewsAdmin(int $totalView): Collection;

    public function searchInformationNew(string $text = null, int $totalView, $from = null, $to = null): Collection;

    public function countInformationNew(string $text = null,  $from = null, $to = null);

    public function searchTrashedInformationNew(string $text = null): Collection;

    public function createInformationNew(array $data): InformationNew;

    public function findInformationNewById(int $id): InformationNew;

    public function findTrashedInformationNewById(int $id): InformationNew;

    public function updateInformationNew(array $params, $id): bool;

    public function deleteInformationNew($id): bool;

    public function recoverTrashedInformationNew(): bool;
}
