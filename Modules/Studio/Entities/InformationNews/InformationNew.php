<?php

namespace Modules\Studio\Entities\InformationNews;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Nicolaslopezj\Searchable\SearchableTrait;

class InformationNew extends Model
{
    use Notifiable, SoftDeletes, SearchableTrait;

    protected $table = 'information_news';

    protected $fillable = [
        'id',
        'name',
        'img',
        'description',
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $hidden = [
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates  = [
        'deleted_at',
        'created_at',
        'updated_at',

    ];

    protected $searchable = [
        'columns' => [
            'information_news.name'         => 10,
            'information_news.description'  => 5
        ]
    ];

    public function searchInformationNew($term)
    {
        return self::search($term);
    }
}
