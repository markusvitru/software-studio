<?php

namespace Modules\Studio\Entities\Trainings\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTrainingRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'name'        => ['required', 'max: 191', 'bail'],
            'description' => ['required', 'max: 250', 'bail'],
            'url'         => ['required', 'max: 100', 'bail'],
        ];
    }
}
