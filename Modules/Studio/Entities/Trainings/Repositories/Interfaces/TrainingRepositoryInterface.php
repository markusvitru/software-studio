<?php

namespace Modules\Studio\Entities\Trainings\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Modules\Studio\Entities\Trainings\Training;

interface TrainingRepositoryInterface
{
    public function listTrainings();

    public function listTrainingsAdmin(int $totalView): Collection;

    public function searchTraining(string $text = null, int $totalView, $from = null, $to = null): Collection;

    public function countTraining(string $text = null,  $from = null, $to = null);

    public function searchTrashedTraining(string $text = null): Collection;

    public function createTraining(array $data): Training;

    public function findTrainingById(int $id): Training;

    public function findTrashedTrainingById(int $id): Training;

    public function updateTraining(array $params, $id): bool;

    public function deleteTraining($id): bool;

    public function recoverTrashedTraining(): bool;
}
