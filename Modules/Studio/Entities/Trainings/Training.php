<?php

namespace Modules\Studio\Entities\Trainings;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Modules\Companies\Entities\Employees\Employee;
use Nicolaslopezj\Searchable\SearchableTrait;

class Training extends Model
{
    use Notifiable, SoftDeletes, SearchableTrait;

    protected $table = 'trainings';

    protected $fillable = [
        'id',
        'name',
        'description',
        'url',
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $hidden = [
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates  = [
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    protected $searchable = [
        'columns' => [
            'trainings.name'        => 10,
            'trainings.description' => 20,
            'trainings.url'         => 20
        ]
    ];

    public function searchTraining($term)
    {
        return self::search($term);
    }

    public function users()
    {
        return $this->belongsToMany(Employee::class);
    }
}
