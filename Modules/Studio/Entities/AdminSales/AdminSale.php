<?php

namespace Modules\Studio\Entities\AdminSales;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Modules\Companies\Entities\Employees\Employee;
use Modules\Companies\Entities\Subsidiaries\Subsidiary;
use Modules\Studio\Entities\Horaries\Horary;
use Modules\Studio\Entities\Platforms\Platform;
use Modules\Studio\Entities\Rooms\Room;
use Nicolaslopezj\Searchable\SearchableTrait;

class AdminSale extends Model
{
    use Notifiable, SoftDeletes, SearchableTrait;

    protected $table = 'admin_sales';

    protected $fillable = [
        'id',
        'employee_id',
        'room_id',
        'subsidiary_id',
        'horary_id',
        'total',
        'comments',
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $hidden = [
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates  = [
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    protected $searchable = [
        'columns' => [
            'admin_sales.total'           => 10,
            'employees.name'        => 10,
            'employees.last_name'   => 10,
            'subsidiaries.name'     => 10,
            'rooms.name'            => 10,
            'horaries.name'   => 10
        ],
        'joins' => [
            'employees'    => ['admin_sales.employee_id', 'employees.id'],
            'rooms'        => ['admin_sales.room_id', 'rooms.id'],
            'subsidiaries' => ['admin_sales.subsidiary_id', 'subsidiaries.id'],
            'horaries'     => ['admin_sales.subsidiary_id', 'horaries.id']
        ],
    ];

    public function searchSale($term)
    {
        return self::search($term);
    }

    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function horary()
    {
        return $this->belongsTo(Horary::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function platforms()
    {
        return $this->belongsToMany(Platform::class)->withPivot(['total_token', 'total_cost']);
    }
}
