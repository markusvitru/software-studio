<?php

namespace Modules\Studio\Entities\AdminSales\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Modules\Studio\Entities\AdminSales\AdminSale;

interface AdminSaleRepositoryInterface
{
    public function getAdminSalesForModelFortnight($id);

    public function listAdminSales(int $totalView): Collection;

    public function searchSale(string $text = null, int $totalView, $from = null, $to = null): Collection;

    public function countAdminSale(string $text = null,  $from = null, $to = null);

    public function createAdminSale(array $data): AdminSale;

    public function findAdminSaleById(int $id): AdminSale;

    public function findTrashedSaleById(int $id): AdminSale;

    public function updateSale(array $params): bool;

    public function deleteSale(): bool;

    public function recoverTrashedSale(): bool;
}
