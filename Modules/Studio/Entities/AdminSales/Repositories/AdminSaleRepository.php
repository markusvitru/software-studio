<?php

namespace Modules\Studio\Entities\AdminSales\Repositories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Modules\Studio\Entities\AdminSales\AdminSale;
use Modules\Studio\Entities\AdminSales\Repositories\Interfaces\AdminSaleRepositoryInterface;

class AdminSaleRepository implements AdminSaleRepositoryInterface
{
    private $columns = ['id', 'employee_id', 'room_id', 'subsidiary_id', 'horary_id', 'total'];

    public function __construct(AdminSale $adminSale)
    {
        $this->model = $adminSale;
    }

    public function getAdminSalesForModelFortnight($id)
    {
        $date = Carbon::now();
        $year = $date->isoFormat('Y');
        $day = $date->isoFormat('D');
        $month = $date->isoFormat('MM');
        $startDay = ($day >= 1 && $day <= 15) ? '01' : '16';
        $endDay = ($day >= 1 && $day <= 15) ? '15' : '31';
        $startDate = $year . '-' . $month . '-' . $startDay . ' 01:00:00';
        $endDate = $year . '-' . $month . '-' . $endDay . ' 23:59:59';

        return  $this->model->where('employee_id', $id)->where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate)->get('total');
    }

    public function listAdminSales(int $totalView): Collection
    {
        try {
            return  $this->model
                ->skip($totalView)
                ->take(30)
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function searchSale(string $text = null, int $totalView, $from = null, $to = null): Collection
    {
        try {
            if (is_null($text) && is_null($from) && is_null($to)) {
                return $this->listAdminSales($totalView);
            }

            if (!is_null($text) && (is_null($from) || is_null($to))) {
                return $this->model->searchSale($text, null, true, true)
                    ->skip($totalView)
                    ->take(30)
                    ->get($this->columns);
            }

            if (is_null($text) && (!is_null($from) || !is_null($to))) {
                return $this->model->whereBetween('created_at', [$from, $to])
                    ->skip($totalView)
                    ->take(30)
                    ->get($this->columns);
            }

            return $this->model->searchSale($text, null, true, true)
                ->whereBetween('created_at', [$from, $to])
                ->orderBy('created_at', 'desc')
                ->skip($totalView)
                ->take(30)
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function countAdminSale(string $text = null,  $from = null, $to = null)
    {
        try {
            if (is_null($text) && is_null($from) && is_null($to)) {
                $data =  $this->model->get(['id']);
                return count($data);
            }

            if (!is_null($text) && (is_null($from) || is_null($to))) {
                $data =  $this->model->searchSale($text, null, true, true)
                    ->get(['id']);
                return count($data);
            }

            if (is_null($text) && (!is_null($from) || !is_null($to))) {
                $data =  $this->model->whereBetween('created_at', [$from, $to])
                    ->get(['id']);
                return count($data);
            }

            $data =  $this->model->searchSale($text, null, true, true)
                ->whereBetween('created_at', [$from, $to])
                ->get(['id']);
            return count($data);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function createAdminSale(array $data): AdminSale
    {
        try {
            return $this->model->create($data);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findAdminSaleById(int $id): AdminSale
    {
        try {
            return $this->model->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findTrashedSaleById(int $id): AdminSale
    {
        try {
            return $this->model->withTrashed()->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function updateSale(array $params): bool
    {
        try {
            return $this->model->update($params);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function deleteSale(): bool
    {
        try {
            return $this->model->delete();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function recoverTrashedSale(): bool
    {
        try {
            return $this->model->restore();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }
}
