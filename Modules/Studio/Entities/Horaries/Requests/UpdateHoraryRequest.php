<?php

namespace Modules\Studio\Entities\Horaries\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateHoraryRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'name' => ['required'],
        ];
    }
}
