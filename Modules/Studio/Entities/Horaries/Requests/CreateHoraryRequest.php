<?php

namespace Modules\Studio\Entities\Horaries\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateHoraryRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'name' => ['required'],
        ];
    }
}
