<?php

namespace Modules\Studio\Entities\Horaries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Modules\Companies\Entities\Employees\Employee;
use Modules\Studio\Entities\Sales\Sale;
use Nicolaslopezj\Searchable\SearchableTrait;

class Horary extends Model
{
    use Notifiable, SoftDeletes, SearchableTrait;

    protected $table = 'horaries';

    protected $fillable = [
        'id',
        'name',
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $hidden = [
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates  = [
        'deleted_at',
        'created_at',
        'updated_at',

    ];

    protected $searchable = [
        'columns' => [
            'horaries.time_start'  => 10,
            'horaries.time_end'    => 10
        ]
    ];

    public function searchHorary($term)
    {
        return self::search($term);
    }

    public function users()
    {
        return $this->belongsToMany(Employee::class);
    }

    public function sales()
    {
        return $this->belongsTo(Sale::class);
    }
}
