<?php

namespace Modules\Studio\Entities\Horaries\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Modules\Studio\Entities\Horaries\Horary;

interface HoraryRepositoryInterface
{
    public function listHoraries(int $totalView): Collection;

    public function listAllHoraries($horaries = []);

    public function searchHorary(string $text = null, int $totalView, $from = null, $to = null): Collection;

    public function countHorary(string $text = null,  $from = null, $to = null);

    public function searchTrashedHorary(string $text = null): Collection;

    public function createHorary(array $data): Horary;

    public function findHoraryById(int $id): Horary;

    public function findTrashedHoraryById(int $id): Horary;

    public function updateHorary(array $params, $id): bool;

    public function deleteHorary($id);

    public function recoverTrashedHorary(): bool;
}
