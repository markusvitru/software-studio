<?php

namespace Modules\Studio\Entities\Horaries\Repositories;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Modules\Studio\Entities\Horaries\Horary;
use Modules\Studio\Entities\Horaries\Repositories\Interfaces\HoraryRepositoryInterface;

class HoraryRepository implements HoraryRepositoryInterface
{
    private $columns = ['id', 'name'];

    public function __construct(Horary $horary)
    {
        $this->model = $horary;
    }

    public function listHoraries(int $totalView): Collection
    {
        try {
            return  $this->model->orderBy('id', 'asc')
                ->skip($totalView)->take(30)
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function listAllHoraries($horaries = [])
    {
        try {
            return $this->model->whereNotIn('id', $horaries)
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function searchHorary(string $text = null, int $totalView, $from = null, $to = null): Collection
    {
        try {
            if (is_null($text) && is_null($from) && is_null($to)) {
                return $this->listHoraries($totalView);
            }

            return $this->model->searchHorary($text, null, true, true)
                ->orderBy('created_at', 'desc')
                ->skip($totalView)
                ->take(30)
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function countHorary(string $text = null,  $from = null, $to = null)
    {
        try {
            if (is_null($text) && is_null($from) && is_null($to)) {
                $data =  $this->model->get(['id']);
                return count($data);
            }

            if (!is_null($text) && (is_null($from) || is_null($to))) {
                $data =  $this->model->searchHorary($text, null, true, true)
                    ->get(['id']);
                return count($data);
            }

            if (is_null($text) && (!is_null($from) || !is_null($to))) {
                $data =  $this->model->whereBetween('created_at', [$from, $to])
                    ->get(['id', 'created_at']);
                return count($data);
            }

            $data =  $this->model->searchHorary($text, null, true, true)
                ->whereBetween('created_at', [$from, $to])
                ->get(['id', 'created_at']);
            return count($data);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function searchTrashedHorary(string $text = null): Collection
    {
        try {
            return  $this->model
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function createHorary(array $data): Horary
    {
        try {
            return $this->model->create($data);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findHoraryById(int $id): Horary
    {
        try {
            return $this->model->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findTrashedHoraryById(int $id): Horary
    {
        try {
            return $this->model->withTrashed()->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function updateHorary(array $params, $id): bool
    {
        $horary = $this->findHoraryById($id);
        try {
            return $horary->update($params);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function deleteHorary($id)
    {
        $horary = $this->findHoraryById($id);
        try {
            return $horary->delete();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function recoverTrashedHorary(): bool
    {
        try {
            return $this->model->restore();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }
}
