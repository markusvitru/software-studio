<?php

namespace Modules\Studio\Entities\ProofPayments\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Modules\Studio\Entities\ProofPayments\ProofPayment;


interface ProofPaymentRepositoryInterface
{
    public function listProofPayments(int $totalView): Collection;

    public function searchProofPayment(string $text = null, int $totalView, $from = null, $to = null): Collection;

    public function countProofPayment(string $text = null,  $from = null, $to = null);

    public function searchTrashedProofPayment(string $text = null): Collection;

    public function createProofPayment(array $data): ProofPayment;

    public function findProofPaymentsByModel($employee_id): Collection;

    public function findProofPaymentById(int $id): ProofPayment;

    public function findTrashedProofPaymentById(int $id): ProofPayment;

    public function updateProofPayment(array $params): bool;

    public function deleteProofPayment(): bool;

    public function recoverTrashedProofPayment(): bool;
}
