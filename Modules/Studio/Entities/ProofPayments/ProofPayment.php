<?php

namespace Modules\Studio\Entities\ProofPayments;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Modules\Companies\Entities\Employees\Employee;
use Nicolaslopezj\Searchable\SearchableTrait;

class ProofPayment extends Model
{
    use Notifiable, SoftDeletes, SearchableTrait;

    protected $table = 'proof_payments';

    protected $fillable = [
        'id',
        'employee_id',
        'total_discounts',
        'total_penalties',
        'total_sales',
        'total',
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $hidden = [
        'deleted_at',
        'updated_at',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates  = [
        'deleted_at',
        'created_at',
        'updated_at',

    ];

    protected $searchable = [
        'columns' => [
            'proof_payments.total_discounts' => 10,
            'proof_payments.total_penalties' => 10,
            'proof_payments.total_sales'     => 10,
            'proof_payments.total'           => 10,
            'employees.name'                 => 10,
            'employees.last_name'            => 10
        ],
        'joins' => [
            'employees' => ['proof_payments.employee_id', 'employees.id'],
        ],
    ];

    public function searchProofPayment($term)
    {
        return self::search($term);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
