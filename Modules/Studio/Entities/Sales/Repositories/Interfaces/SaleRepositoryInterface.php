<?php

namespace Modules\Studio\Entities\Sales\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Modules\Studio\Entities\Sales\Sale;

interface SaleRepositoryInterface
{

    public function getSalesForModelFortnight($id);

    public function listSales(int $totalView): Collection;

    public function searchSale(string $text = null, int $totalView, $from = null, $to = null): Collection;

    public function countSale(string $text = null,  $from = null, $to = null);

    public function createSale(array $data): Sale;

    public function findSaleById(int $id): Sale;

    public function findTrashedSaleById(int $id): Sale;

    public function updateSale(array $params): bool;

    public function deleteSale(): bool;

    public function recoverTrashedSale(): bool;
}
