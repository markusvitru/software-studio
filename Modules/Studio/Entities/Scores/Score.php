<?php

namespace Modules\Studio\Entities\Scores;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Modules\Companies\Entities\Employees\Employee;
use Modules\Studio\Entities\Platforms\Platform;
use Nicolaslopezj\Searchable\SearchableTrait;

class Score extends Model
{
    use Notifiable, SoftDeletes, SearchableTrait;

    protected $table = 'scores';

    protected $fillable = [
        'id',
        'employee_id',
        'personal_presentation',
        'clothing',
        'hair',
        'hand_nails',
        'toe_nails',
        'makeup',
        'body_expression',
        'oral_expression',
        'have_social_networks',
        'observation',
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $hidden = [
        'deleted_at',
        'updated_at',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates  = [
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    protected $searchable = [
        'columns' => [
            'scores.personal_presentation' => 10,
            'scores.have_social_networks'  => 10,
            'employees.name'               => 10,
            'employees.last_name'          => 10
        ],
        'joins' => [
            'employees' => ['scores.employee_id', 'employees.id'],
        ]
    ];

    public function searchScore($term)
    {
        return self::search($term);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function platforms()
    {
        return $this->belongsToMany(Platform::class);
    }
}
