<?php

namespace Modules\Studio\Entities\Scores\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Modules\Studio\Entities\Scores\Score;

interface ScoreRepositoryInterface
{
    public function listScores(int $totalView): Collection;

    public function countScore(string $text = null,  $from = null, $to = null);

    public function searchScore(string $text = null, int $totalView, $from = null, $to = null): Collection;

    public function searchTrashedScore(string $text = null): Collection;

    public function createScore(array $data): Score;

    public function findScoreById(int $id): Score;

    public function findTrashedScoreById(int $id): Score;

    public function updateScore(array $params, $id): bool;

    public function deleteScore($id): bool;

    public function recoverTrashedScore($id): bool;
}
