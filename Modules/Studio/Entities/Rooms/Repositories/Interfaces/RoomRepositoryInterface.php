<?php

namespace Modules\Studio\Entities\Rooms\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Modules\Studio\Entities\Rooms\Room;
use Modules\Studio\Entities\Shifts\Shift;

interface RoomRepositoryInterface
{
    public function listRooms(int $totalView);

    public function searchRoom(string $text = null, int $totalView, $from = null, $to = null): Collection;

    public function countRoom(string $text = null,  $from = null, $to = null);

    public function searchTrashedRoom(string $text = null): Collection;

    public function createRoom(array $data): Room;

    public function findRoomById(int $id): Room;

    public function findTrashedRoomById(int $id): Room;

    public function updateRoom(array $params, $id): bool;

    public function deleteRoom($id): bool;

    public function recoverTrashedRoom(): bool;
}
