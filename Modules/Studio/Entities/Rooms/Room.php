<?php

namespace Modules\Studio\Entities\Rooms;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Modules\Companies\Entities\Subsidiaries\Subsidiary;
use Modules\Studio\Entities\Sales\Sale;
use Illuminate\Support\Collection;
use Nicolaslopezj\Searchable\SearchableTrait;

class Room extends Model
{
    use Notifiable, SoftDeletes, SearchableTrait;

    protected $table = 'rooms';

    protected $fillable = [
        'id',
        'name',
        'subsidiary_id',
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $hidden = [
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates  = [
        'deleted_at',
        'created_at',
        'updated_at',

    ];

    protected $searchable = [
        'columns' => [
            'rooms.id'          => 10,
            'rooms.name'        => 10,
            'subsidiaries.name' => 10
        ],
        'joins' => [
            'subsidiaries' => ['rooms.subsidiary_id', 'subsidiaries.id'],
        ],
    ];

    public function searchRoom(string $term)
    {
        return self::search($term);
    }

    public function sale()
    {
        return $this->belongsTo(Sale::class);
    }

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }
}
