<?php

namespace Modules\Studio\Entities\Rooms\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateRoomRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'name' => ['required', 'max:191', 'bail'],
        ];
    }
}
