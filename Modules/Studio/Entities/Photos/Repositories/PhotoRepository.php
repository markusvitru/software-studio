<?php

namespace Modules\Studio\Entities\Photos\Repositories;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Modules\Studio\Entities\Photos\Photo;
use Modules\Studio\Entities\Photos\Repositories\Interfaces\PhotoRepositoryInterface;

class PhotoRepository implements PhotoRepositoryInterface
{
    private $columns = ['id', 'sending_photo_id', 'photo'];

    public function __construct(Photo $photo)
    {
        $this->model = $photo;
    }

    public function listPhotos(int $totalView)
    {
        try {
            return  $this->model
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function createPhoto(array $data): Photo
    {
        try {
            return $this->model->create($data);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function deletePhoto(): bool
    {
        try {
            return $this->model->delete();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function recoverTrashedPhoto(): bool
    {
        try {
            return $this->model->restore();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }
}
