<?php

namespace Modules\Studio\Entities\Photos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Modules\Studio\Entities\SendingPhotos\SendingPhoto;
use Nicolaslopezj\Searchable\SearchableTrait;

class Photo extends Model
{
    use Notifiable, SoftDeletes, SearchableTrait;

    protected $table = 'photos';

    protected $fillable = [
        'id',
        'sending_photo_id',
        'photo',
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $hidden = [
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates  = [
        'deleted_at',
        'created_at',
        'updated_at',

    ];

    protected $searchable = [];

    public function sendingPhoto()
    {
        return $this->belongsTo(SendingPhoto::class);
    }
}
