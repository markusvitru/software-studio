<?php

namespace Modules\Studio\Entities\Shifts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Modules\Companies\Entities\Employees\Employee;
use Nicolaslopezj\Searchable\SearchableTrait;

class Shift extends Model
{
    use Notifiable, SoftDeletes, SearchableTrait;

    protected $table = 'shifts';

    protected $fillable = [
        'id',
        'name',
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $hidden = [
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates  = [
        'deleted_at',
        'created_at',
        'updated_at',

    ];

    protected $searchable = [
        'columns' => [
            'shifts.name' => 10
        ]
    ];

    public function searchShift($term)
    {
        return self::search($term);
    }

    public function users()
    {
        return $this->belongsToMany(Employee::class);
    }
}
