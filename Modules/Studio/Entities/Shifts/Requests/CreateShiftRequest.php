<?php

namespace Modules\Studio\Entities\Shifts\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateShiftRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'name' => ['required', 'max:191', 'bail'],
        ];
    }
}
