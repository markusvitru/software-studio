<?php

namespace Modules\Studio\Entities\Shifts\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Modules\Studio\Entities\Shifts\Shift;

interface ShiftRepositoryInterface
{
    public function listShifts(int $totalView): Collection;

    public function searchShift(string $text = null, int $totalView, $from = null, $to = null): Collection;

    public function countShift(string $text = null,  $from = null, $to = null);

    public function searchTrashedShift(string $text = null): Collection;

    public function createShift(array $data): Shift;

    public function findShiftById(int $id): Shift;

    public function findTrashedShiftById(int $id): Shift;

    public function updateShift(array $params, $id): bool;

    public function deleteShift($id): bool;

    public function recoverTrashedShift(): bool;
}
