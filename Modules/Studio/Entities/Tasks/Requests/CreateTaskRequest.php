<?php

namespace Modules\Studio\Entities\Tasks\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTaskRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'task' => ['required', 'max: 100', 'bail'],
        ];
    }
}
