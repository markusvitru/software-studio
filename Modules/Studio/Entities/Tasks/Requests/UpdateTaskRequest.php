<?php

namespace Modules\Studio\Entities\Discounts\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTaskRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'tesk' => ['required', 'max: 150', 'bail'],
        ];
    }
}
