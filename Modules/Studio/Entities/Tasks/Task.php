<?php

namespace Modules\Studio\Entities\Tasks;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Modules\Companies\Entities\Employees\Employee;
use Modules\Studio\Entities\TaskComments\TaskComment;
use Nicolaslopezj\Searchable\SearchableTrait;

class Task extends Model
{
    use Notifiable, SoftDeletes, SearchableTrait;

    protected $table = 'tasks';

    protected $fillable = [
        'id',
        'employee_id',
        'task',
        'state',
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $hidden = [
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates  = [
        'deleted_at',
        'created_at',
        'updated_at',

    ];

    protected $searchable = [
        'columns' => [
            'tasks.task'            => 10,
            'employees.name'        => 10,
            'employees.last_name'   => 10
        ],
        'joins' => [
            'employees' => ['tasks.employee_id', 'employees.id'],
        ]
    ];

    public function searchTask($term)
    {
        return self::search($term);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function comments()
    {
        return $this->hasMany(TaskComment::class);
    }
}
