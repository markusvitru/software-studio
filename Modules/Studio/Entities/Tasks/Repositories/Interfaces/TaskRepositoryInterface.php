<?php

namespace Modules\Studio\Entities\Tasks\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Modules\Studio\Entities\Tasks\Task;

interface TaskRepositoryInterface
{
    public function listTasks(int $totalView): Collection;

    public function searchTask(string $text = null, $state = null, int $totalView, $from = null, $to = null): Collection;

    public function countTask(string $text = null, $state = null,  $from = null, $to = null);

    public function searchTrashedTask(string $text = null): Collection;

    public function createTask(array $data): Task;

    public function findTaskById(int $id): Task;

    public function findPendingTaskByEmployeeId(int $employeeId, $totalView): Collection;

    public function countPendingTaskByEmployeeId(int $employeeId);

    public function findTrashedTaskById(int $id): Task;

    public function updateTask(array $params, $id): bool;

    public function deleteTask($id): bool;

    public function recoverTrashedTask(): bool;
}
