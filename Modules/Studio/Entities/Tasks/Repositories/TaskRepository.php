<?php

namespace Modules\Studio\Entities\Tasks\Repositories;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Modules\Studio\Entities\Tasks\Repositories\Interfaces\TaskRepositoryInterface;
use Modules\Studio\Entities\Tasks\Task;

class TaskRepository implements TaskRepositoryInterface
{
    private $columns = ['id', 'employee_id', 'task', 'state'];

    public function __construct(Task $task)
    {
        $this->model = $task;
    }

    public function listTasks(int $totalView): Collection
    {
        try {
            return  $this->model
                ->skip($totalView)
                ->take(30)
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function searchTask(string $text = null, $state = null, int $totalView, $from = null, $to = null): Collection
    {
        try {
            if (is_null($text) && is_null($from) && is_null($to)) {
                return $this->listTasks($totalView);
            }

            if (!is_null($text) && (is_null($from) || is_null($to))) {
                return $this->model->searchTask($text, null, true, true)
                    ->when($state, function ($q, $state) {
                        return $q->where('state', $state);
                    })
                    ->skip($totalView)
                    ->take(30)
                    ->get($this->columns);
            }

            if (is_null($text) && (!is_null($from) || !is_null($to))) {
                return $this->model->whereBetween('created_at', [$from, $to])
                    ->when($state, function ($q, $state) {
                        return $q->where('state', $state);
                    })
                    ->skip($totalView)
                    ->take(30)
                    ->get($this->columns);
            }

            return $this->model->searchTask($text, null, true, true)
                ->whereBetween('created_at', [$from, $to])
                ->when($state, function ($q, $state) {
                    return $q->where('state', $state);
                })
                ->orderBy('created_at', 'desc')
                ->skip($totalView)
                ->take(30)
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function countTask(string $text = null, $state = null,  $from = null, $to = null)
    {
        try {
            if (is_null($text) && is_null($from) && is_null($to)) {
                $data =  $this->model->get(['id']);
                return count($data);
            }

            if (!is_null($text) && (is_null($from) || is_null($to))) {
                $data =  $this->model->searchTask($text, null, true, true)
                    ->when($state, function ($q, $state) {
                        return $q->where('state', $state);
                    })
                    ->get(['id']);
                return count($data);
            }

            if (is_null($text) && (!is_null($from) || !is_null($to))) {
                $data =  $this->model->whereBetween('created_at', [$from, $to])
                    ->when($state, function ($q, $state) {
                        return $q->where('state', $state);
                    })
                    ->get(['id']);
                return count($data);
            }

            $data =  $this->model->searchTask($text, null, true, true)
                ->when($state, function ($q, $state) {
                    return $q->where('state', $state);
                })
                ->whereBetween('created_at', [$from, $to])
                ->get(['id']);
            return count($data);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function searchTrashedTask(string $text = null): Collection
    {
        try {
            return  $this->model
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function createTask(array $data): Task
    {
        try {
            return $this->model->create($data);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findTaskById(int $id): Task
    {
        try {
            return $this->model->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findPendingTaskByEmployeeId(int $employeeId, $totalView): Collection
    {
        try {
            return $this->model->where('employee_id', $employeeId)
                ->where('state', 0)
                ->skip($totalView)
                ->take(30)
                ->get($this->columns);
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function countPendingTaskByEmployeeId(int $employeeId)
    {
        try {
            $data = $this->model->where('employee_id', $employeeId)
                ->where('state', 0)
                ->get('id');

            return count($data);
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findTrashedTaskById(int $id): Task
    {
        try {
            return $this->model->withTrashed()->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function updateTask(array $params, $id): bool
    {
        $task = $this->findTaskById($id);
        try {
            return $task->update($params);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function deleteTask($id): bool
    {
        $task = $this->findTaskById($id);
        try {
            return $task->delete();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function recoverTrashedTask(): bool
    {
        try {
            return $this->model->restore();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }
}
