<?php

namespace Modules\Studio\Entities\Discounts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Modules\Companies\Entities\Employees\Employee;
use Nicolaslopezj\Searchable\SearchableTrait;

class Discount extends Model
{
    use Notifiable, SoftDeletes, SearchableTrait;

    protected $table = 'discounts';

    protected $fillable = [
        'id',
        'employee_id',
        'name',
        'cost',
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $hidden = [
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates  = [
        'deleted_at',
        'created_at',
        'updated_at',

    ];

    protected $searchable = [
        'columns' => [
            'discounts.name'        => 10,
            'discounts.cost'        => 5,
            'employees.name'        => 10,
            'employees.last_name'   => 10,
            'subsidiaries.name'     => 10
        ],
        'joins' => [
            'employees'     => ['discounts.employee_id', 'employees.id'],
            'subsidiaries'  => ['employees.subsidiary_id', 'subsidiaries.id']
        ],
    ];

    public function searchDiscount($term)
    {
        return self::search($term);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
