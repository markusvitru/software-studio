<?php

namespace Modules\Studio\Entities\Discounts\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateDiscountRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'name' => ['required', 'max: 150', 'bail'],
            'cost' => ['required', 'max: 15', 'bail'],
        ];
    }
}
