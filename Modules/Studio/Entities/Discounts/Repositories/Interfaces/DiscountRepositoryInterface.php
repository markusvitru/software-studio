<?php

namespace Modules\Studio\Entities\Discounts\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Modules\Studio\Entities\Discounts\Discount;

interface DiscountRepositoryInterface
{
    public function listDiscounts(int $totalView): Collection;

    public function searchDiscount(string $text = null, int $totalView, $from = null, $to = null): Collection;

    public function countDiscount(string $text = null,  $from = null, $to = null);

    public function searchTrashedDiscount(string $text = null): Collection;

    public function createDiscount(array $data): Discount;

    public function findDiscountById(int $id): Discount;

    public function findTrashedDiscountById(int $id): Discount;

    public function updateDiscount(array $params, $id): bool;

    public function deleteDiscount($id): bool;

    public function recoverTrashedDiscount(): bool;
}
