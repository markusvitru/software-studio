<?php

namespace Modules\Studio\Entities\TaskComments\Repositories;

use Illuminate\Database\QueryException;
use Modules\Studio\Entities\TaskComments\Repositories\Interfaces\TaskCommentRepositoryInterface;
use Modules\Studio\Entities\TaskComments\TaskComment;

class TaskCommentRepository implements TaskCommentRepositoryInterface
{
    public function __construct(TaskComment $taskComment)
    {
        $this->model = $taskComment;
    }

    public function createTaskComment(array $data): TaskComment
    {
        try {
            return $this->model->create($data);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }
}
