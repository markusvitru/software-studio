<?php

namespace Modules\Studio\Entities\TaskComments\Repositories\Interfaces;

use Modules\Studio\Entities\TaskComments\TaskComment;

interface TaskCommentRepositoryInterface
{
    public function createTaskComment(array $data): TaskComment;
}
