<?php

namespace Modules\Studio\Entities\TaskComments;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Modules\Studio\Entities\Tasks\Task;
use Nicolaslopezj\Searchable\SearchableTrait;

class TaskComment extends Model
{
    use Notifiable, SoftDeletes, SearchableTrait;

    protected $table = 'task_comments';

    protected $fillable = [
        'id',
        'task_id',
        'comment',
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $hidden = [
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates  = [
        'deleted_at',
        'created_at',
        'updated_at',

    ];

    protected $searchable = [];

    public function task()
    {
        return $this->belongsTo(Task::class);
    }
}
