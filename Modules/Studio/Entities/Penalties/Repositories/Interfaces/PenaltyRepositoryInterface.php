<?php

namespace Modules\Studio\Entities\Penalties\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Modules\Studio\Entities\Penalties\Penalty;

interface PenaltyRepositoryInterface
{
    public function listPenalties(int $totalView): Collection;

    public function listAllPenalties();

    public function searchPenalty(string $text = null, int $totalView, $from = null, $to = null): Collection;

    public function countPenalty(string $text = null,  $from = null, $to = null);

    public function searchTrashedPenalty(string $text = null): Collection;

    public function createPenalty(array $data): Penalty;

    public function findPenaltyById(int $id): Penalty;

    public function findTrashedPenaltyById(int $id): Penalty;

    public function updatePenalty(array $params, $id): bool;

    public function deletePenalty($id): bool;

    public function recoverTrashedPenalty(): bool;
}
