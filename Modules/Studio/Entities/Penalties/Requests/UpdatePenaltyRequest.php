<?php

namespace Modules\Studio\Entities\Penalties\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePenaltyRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'description' => ['required', 'max: 250', 'bail'],
            'cost'        => ['required', 'bail'],
        ];
    }
}
