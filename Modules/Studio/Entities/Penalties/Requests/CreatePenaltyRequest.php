<?php

namespace Modules\Studio\Entities\Penalties\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreatePenaltyRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'name' => ['required', 'max:250', 'bail'],
            'cost' => ['required', 'bail'],
        ];
    }
}
