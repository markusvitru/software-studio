<?php

namespace Modules\Studio\Entities\Penalties;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Modules\Companies\Entities\Employees\Employee;
use Nicolaslopezj\Searchable\SearchableTrait;

class Penalty extends Model
{
    use Notifiable, SoftDeletes, SearchableTrait;

    protected $table = 'penalties';

    protected $fillable = [
        'id',
        'description',
        'cost',
        'penalty_date',
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $hidden = [
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates  = [
        'deleted_at',
        'created_at',
        'updated_at',

    ];

    protected $searchable = [
        'columns' => [
            'penalties.description'  => 10,
            'penalties.cost'         => 5
        ]
    ];

    public function searchPenalty($term)
    {
        return self::search($term);
    }

    public function users()
    {
        return $this->belongsToMany(Employee::class)->withPivot('created_at');
    }
}
