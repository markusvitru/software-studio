<?php

namespace Modules\Studio\Providers;

use Modules\Studio\Entities\Discounts\Repositories\DiscountRepository;
use Modules\Studio\Entities\Discounts\Repositories\Interfaces\DiscountRepositoryInterface;
use Modules\Studio\Entities\Goals\Repositories\GoalRepository;
use Modules\Studio\Entities\Goals\Repositories\Interfaces\GoalRepositoryInterface;
use Modules\Studio\Entities\Horaries\Repositories\HoraryRepository;
use Modules\Studio\Entities\Horaries\Repositories\Interfaces\HoraryRepositoryInterface;
use Modules\Studio\Entities\LoginLogs\Repositories\Interfaces\LoginLogRepositoryInterface;
use Modules\Studio\Entities\LoginLogs\Repositories\LoginLogRepository;
use Modules\Studio\Entities\Penalties\Repositories\Interfaces\PenaltyRepositoryInterface;
use Modules\Studio\Entities\Penalties\Repositories\PenaltyRepository;
use Modules\Studio\Entities\Photos\Repositories\Interfaces\PhotoRepositoryInterface;
use Modules\Studio\Entities\Photos\Repositories\PhotoRepository;
use Modules\Studio\Entities\Platforms\Repositories\Interfaces\PlatformRepositoryInterface;
use Modules\Studio\Entities\Platforms\Repositories\PlatformRepository;
use Modules\Studio\Entities\ProofPayments\Repositories\Interfaces\ProofPaymentRepositoryInterface;
use Modules\Studio\Entities\ProofPayments\Repositories\ProofPaymentRepository;
use Modules\Studio\Entities\Rooms\Repositories\Interfaces\RoomRepositoryInterface;
use Modules\Studio\Entities\Rooms\Repositories\RoomRepository;
use Modules\Studio\Entities\Sales\Repositories\Interfaces\SaleRepositoryInterface;
use Modules\Studio\Entities\Sales\Repositories\SaleRepository;
use Modules\Studio\Entities\SendingPhotos\Repositories\Interfaces\SendingPhotoRepositoryInterface;
use Modules\Studio\Entities\SendingPhotos\Repositories\SendingPhotoRepository;
use Modules\Studio\Entities\Shifts\Repositories\Interfaces\ShiftRepositoryInterface;
use Modules\Studio\Entities\Shifts\Repositories\ShiftRepository;
use Modules\Studio\Entities\Tasks\Repositories\Interfaces\TaskRepositoryInterface;
use Modules\Studio\Entities\Tasks\Repositories\TaskRepository;
use Modules\Studio\Entities\Trainings\Repositories\Interfaces\TrainingRepositoryInterface;
use Modules\Studio\Entities\Trainings\Repositories\TrainingRepository;
use Illuminate\Support\ServiceProvider;
use Modules\Studio\Entities\AdminSales\Repositories\AdminSaleRepository;
use Modules\Studio\Entities\AdminSales\Repositories\Interfaces\AdminSaleRepositoryInterface;
use Modules\Studio\Entities\EmployeePlatformAccounts\Repositories\EmployeePlatformAccountRepository;
use Modules\Studio\Entities\EmployeePlatformAccounts\Repositories\Interfaces\EmployeePlatformAccountRepositoryInterface;
use Modules\Studio\Entities\InformationNews\Repositories\InformationNewRepository;
use Modules\Studio\Entities\InformationNews\Repositories\Interfaces\InformationNewRepositoryInterface;
use Modules\Studio\Entities\Scores\Repositories\Interfaces\ScoreRepositoryInterface;
use Modules\Studio\Entities\Scores\Repositories\ScoreRepository;
use Modules\Studio\Entities\TaskComments\Repositories\Interfaces\TaskCommentRepositoryInterface;
use Modules\Studio\Entities\TaskComments\Repositories\TaskCommentRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            AdminSaleRepositoryInterface::class,
            AdminSaleRepository::class
        );

        $this->app->bind(
            InformationNewRepositoryInterface::class,
            InformationNewRepository::class
        );

        $this->app->bind(
            ScoreRepositoryInterface::class,
            ScoreRepository::class
        );

        $this->app->bind(
            EmployeePlatformAccountRepositoryInterface::class,
            EmployeePlatformAccountRepository::class
        );

        $this->app->bind(
            DiscountRepositoryInterface::class,
            DiscountRepository::class
        );

        $this->app->bind(
            TaskCommentRepositoryInterface::class,
            TaskCommentRepository::class
        );

        $this->app->bind(
            GoalRepositoryInterface::class,
            GoalRepository::class
        );

        $this->app->bind(
            HoraryRepositoryInterface::class,
            HoraryRepository::class
        );

        $this->app->bind(
            LoginLogRepositoryInterface::class,
            LoginLogRepository::class
        );

        $this->app->bind(
            PenaltyRepositoryInterface::class,
            PenaltyRepository::class
        );

        $this->app->bind(
            PhotoRepositoryInterface::class,
            PhotoRepository::class
        );

        $this->app->bind(
            PlatformRepositoryInterface::class,
            PlatformRepository::class
        );

        $this->app->bind(
            ProofPaymentRepositoryInterface::class,
            ProofPaymentRepository::class
        );

        $this->app->bind(
            RoomRepositoryInterface::class,
            RoomRepository::class
        );

        $this->app->bind(
            SaleRepositoryInterface::class,
            SaleRepository::class
        );

        $this->app->bind(
            SendingPhotoRepositoryInterface::class,
            SendingPhotoRepository::class
        );

        $this->app->bind(
            ShiftRepositoryInterface::class,
            ShiftRepository::class
        );

        $this->app->bind(
            TaskRepositoryInterface::class,
            TaskRepository::class
        );

        $this->app->bind(
            TrainingRepositoryInterface::class,
            TrainingRepository::class
        );
    }
}
