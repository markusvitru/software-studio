{{-- <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet" href="{{asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
<link rel="stylesheet" href="{{asset('css/admin/frontAdmin.css')}}">
<link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/jqvmap/jqvmap.min.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
<link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.css')}}">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> --}}

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
{{-- <meta name="description" content="Start your development with a Dashboard for Bootstrap 4."> --}}
{{-- <meta name="author" content="Creative Tim"> --}}
<title>Studio Dashboard</title>
<!-- Favicon -->
<link rel="icon" href="{{asset('argonTemplate/img/icons/shop.png')}}" type="image/png">
<!-- Fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
<!-- Icons -->
<link rel="stylesheet" href="{{asset('argonTemplate/resources/nucleo/css/nucleo.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('argonTemplate/resources/@fortawesome/fontawesome-free/css/all.min.css')}}"
    type="text/css">
<!-- Page plugins -->
<!-- Argon CSS -->
<link rel="stylesheet" href="{{asset('argonTemplate/css/argon.css?v=1.1.0')}}" type="text/css">
{{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
<style>
    .button-reset {
        border: 0px;
        background: white;
    }

    .button-reset:hover {
        background: white;
    }

    .button-reset:focus {
        border: 0px;
        background: white;
        outline: 1px !important;
    }

</style>