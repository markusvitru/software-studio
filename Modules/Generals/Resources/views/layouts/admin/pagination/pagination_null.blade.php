<p class="alert alert-warning">No hay datos.
</p>

<ul class="pagination justify-content-star ">
    <li class="page-item">
        <a class="page-link" id="previous" name="previous" type="submit" href="{{ URL::previous() }}">
            <i class="fas fa-angle-left"></i>
            <span class="sr-only">Previous</span>
        </a>
    </li>
</ul>
