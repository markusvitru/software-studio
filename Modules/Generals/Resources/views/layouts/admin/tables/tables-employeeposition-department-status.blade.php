<div class="table-responsive">
    <table class="table align-items-center table-flush table-hover">
        <thead class="thead-light">
            <tr>
                @foreach ($headers as $header)
                <th class="text-center">{{ $header }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody class="list">
        @if($datas)
            @foreach($datas as $data)
            <tr>
                @foreach($data->toArray() as $key => $value)
                @if($key == 'type_contract')
                    <td>
                        @if ($value == 0 && $value != '')
                            <span>Término fijo</span>
                        @elseif ($value == 1)
                            <span>Prestación de servicios</span>
                        @elseif ($value == 2)
                            <span>Obra o labor</span>
                        @elseif($value == 3)
                            <span>Indefinido</span>
                        @endif
                    </td>
                @else
                    <td class="text-center">
                        {{ $data[$key] }}
                    </td>
                @endif
                @endforeach
                <td class="text-center">{{ $data->subsidiary->name }}</td>
                <td class="text-center">
                    @include('generals::layouts.status', ['status' => $data->status])</td>
                <td class="text-center">
                    @include('generals::layouts.admin.tables.table_options', [$data, 'optionsRoutes' => $optionsRoutes])
                </td>
            </tr>
            <!-- Modal -->
            <div class="modal fade" id="modal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
                aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Actualizar <b>{{$data->name}}</b></h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <form action="{{ route('admin.employees.update', $data->id) }}" method="post" class="form">
                      @csrf
                      @method('PUT')
                      <div class="modal-body py-0">
                        <input name="employee_id" id="employee_id" type="hidden" value="{{ $data->id }}">
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="form-control-label" for="name">Nombre</label>
                              <div class="input-group input-group-merge">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fas fa-user"></i></span>
                                </div>
                                <input type="text" name="name" id="name" validation-pattern="name" placeholder="Nombre"
                                  class="form-control" value="{!! $data->name ?: old('name')  !!}" required>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="form-control-label" for="last_name">Apellido</label>
                              <div class="input-group input-group-merge">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fas fa-user"></i></span>
                                </div>
                                <input type="text" name="last_name" id="last_name" validation-pattern="name" placeholder="Apellido"
                                  class="form-control" value="{!! $data->last_name ?: old('last_name')  !!}" required>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="form-control-label" for="email">Email</label>
                              <div class="input-group input-group-merge">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                </div>
                                <input type="text" name="email" id="email" validation-pattern="email" placeholder="Email"
                                  class="form-control" value="{!! $data->email ?: old('email')  !!}" required>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div id="cities" class="form-group">
                              <label class="form-control-label" for="subsidiary_id">Sucursal</label>
                              <div class="input-group">
                                <select name="subsidiary_id" id="subsidiary_id" class="form-control">
                                  @foreach($all_subsiarries as $subsidiary)
                                  @if($subsidiary->id == $data->subsidiary_id)
                                  <option selected="selected" value="{{ $subsidiary->id }}">{{ $subsidiary->name }}
                                  </option>
                                  @else
                                  <option value="{{ $subsidiary->id }}">{{ $subsidiary->name }}</option>
                                  @endif
                                  @endforeach
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-6 col-12">
                            <div class="form-group">
                                <label class="form-control-label" for="start_date">Fecha de inicio</label>
                                <div class="input-group input-group-merge">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-lock"></i></span>
                                    </div>
                                    <input type="date" name="start_date" id="start_date" class="form-control" value="{{ $data->start_date }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label class="form-control-label" for="type_contract">Tipo de contrato</label>
                            <select class="form-control" name="type_contract" id="type_contract">
                                <option @if($data->type_contract == 0) selected @endif value="0">Término fijo</option>
                                <option @if($data->type_contract == 1) selected @endif value="1">Prestación de servicios</option>
                                <option @if($data->type_contract == 2) selected @endif value="2">Obra o labor</option>
                                <option @if($data->type_contract == 3) selected @endif value="3">Indefinido</option>
                            </select>
                        </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="form-control-label" for="password">Password</label>
                              <div class="input-group input-group-merge">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fa fa-lock"></i></span>
                                </div>
                                <input type="password" name="password" id="password" placeholder="xxxxx" class="form-control"
                                  value="{!! $data->phone ?: old('phone')  !!}">
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            @include('generals::admin.shared.status-select', ['status' => $data->status])
                          </div>
                          <div class="col-sm-6">
                            <label class="form-control-label" for="password">Rol</label>
                            @php
                                if(isset($data->roles[0])){
                                    $employeeRole = $data->roles[0]->id;
                                }else{
                                    $employeeRole = 0;
                                }
                            @endphp
                            <select name="role" id="role" class="form-control" onchange="validateManager()">
                                @foreach($roles as $role)
                                    <option @if($employeeRole == $role->id) selected='selected' @endif value="{{ $role->id }}">{{ $role->display_name }}</option>
                                @endforeach
                            </select>
                          </div>
                          @php
                              $style = ($employeeRole == 3) ? 'block' : 'none' ;
                          @endphp
                          <div class="col-sm-6" id="containManager" style="display:{{ $style }}">
                            <label class="form-control-label" for="manager_id">Supervisor</label>
                            <select class="form-control" name="manager_id" id="manager_id">
                                <option value="">Seleccione</option>
                                @foreach($managers as $manager)
                                    <option @if($data->getOriginal()['manager_id'] == $manager->id) selected='selected' @endif value="{{ $manager->id }}">{{ $manager->name." ".$manager->last_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        </div>
                      </div>
                      <div class="card-footer text-right">
                        <button type="submit" class="btn btn-primary btn-sm">Actualizar</button>
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cerrar</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            @endforeach
            @endif
        </tbody>
    </table>
</div>

<script>
    function validateManager(){
        var cod = document.getElementById("role").value;
        if(cod == 3){
            document.getElementById("containManager").style.display = 'block';
        }else{
            document.getElementById("manager_id").value = "";
            document.getElementById("containManager").style.display = 'none';
        }
    };
</script>
