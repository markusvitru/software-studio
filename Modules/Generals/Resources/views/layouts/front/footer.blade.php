<footer class="footer footer-style mt-auto p-4">
    <div class="container-footer mx-auto">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-2 col-xl-3 d-flex justify-content-center">
                <img src="{{ asset('img/fvn/logoWhite.png')}}" class="logo-footer pr-xl-5" alt="fvn-blanco">
            </div>
            <div class="col-sm-6 justify-content-center d-flex col-lg-3 col-xl-2">
                <ol class="text-white pl-0 pl-xl-2 footer-responsive">
                    <li><b>HOME</b></li>
                    <li>ZAPATOS NIÑO</li>
                    <li>ZAPATOS NIÑA</li>
                    <li>ZAPATO FISIOLÓGICO</li>
                    <li><b>OUTLET</b></li>
                </ol>
            </div>
            <div class="col-sm-6 justify-content-center d-flex col-lg-3 col-xl-3">
                <ol class="text-white pl-0 pl-xl-5 footer-responsive">
                    <li><b>SERVICIO AL CLIENTE</b></li>
                    <li>NUESTRA EMPRESA</li>
                    <li class="li-width">POLITICA DE TRATAMIENTO DE DATOS</li>
                    <li>MÉTODOS DE PAGO</li>
                    <li>METODO DE ENTREGA</li>
                    <li>CERTIFICACIÓNES</li>
                </ol>
            </div>
            <div class="col-12 col-lg-3 col-xl-3 pl-0 pr-xl-5 text-center">
                <h4 class="text-white contact-footer"><b>CONTÁCTANOS</b></h4>
                <ol class="text-white pl-0">
                    <li>322 352 1542</li>
                </ol>
            </div>
            <div class="col-12 col-lg-1 col-xl-1 text-center d-flex my-auto">
                <div class="row mx-auto">
                    <div class="row">
                        <div class="col-3 col-lg-12">
                            <img src="{{ asset('img/fvn/facebook.png')}}" class="icon-footer" alt="facebook">
                        </div>
                        <div class="col-3 col-lg-12">
                            <img src="{{ asset('img/fvn/instagram.png')}}" class="icon-footer" alt="instagram">
                        </div>
                        <div class="col-3 col-lg-12">
                            <img src="{{ asset('img/fvn/youtube.png')}}" class="icon-footer" alt="youtube">
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</footer>