<div class="container-reset mt-5">
    <div class="row mx-0">
        <div class=" col-12 col-sm-5 p-3 p-lg-4">
            <img src="{{ asset('img/fvn/girl.png')}}" class="d-block w-100 img-children" alt="...">
        </div>
        <div class="col-12 col-sm-7 px-2 px-lg-3 d-flex">
            <div class="row mx-0">
                <div class="col-6 px-2">
                    <div class="card-body px-0">
                        <img src="{{ asset('storage/'.$products1[1]->cover) }}" alt="{{ $products1[1]->name }}"
                            class="img-baner-product-girl">
                    </div>
                </div>
                <div class="col-6 px-2">
                    <div class="card-body px-0">
                        <img src="{{ asset('storage/'.$products1[2]->cover) }}" alt="{{ $products1[2]->name }}"
                            class="img-baner-product-girl">
                    </div>
                </div>
                <div class="col-6 px-2">
                    <div class="card-body px-0">
                        <img src="{{ asset('storage/'.$products1[3]->cover) }}" alt="{{ $products1[3]->name }}"
                            class="img-baner-product-girl">
                    </div>
                </div>
                <div class="col-6 px-2">
                    <div class="card-body px-0">
                        <img src="{{ asset('storage/'.$products1[0]->cover) }}" alt="{{ $products1[0]->name }}"
                            class="img-baner-product-girl">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0">
        <div class="col-12 order-sm-last col-sm-5 p-3 p-lg-4">
            <img src="{{ asset('img/fvn/boy.png')}}" class="d-block w-100 img-children" alt="...">
        </div>
        <div class="col-12 col-sm-7 px-2 px-lg-3 d-flex">
            <div class="row mx-0">
                <div class="col-6 px-2">
                    <div class="card-body px-0">
                        <img src="{{ asset('storage/'.$products1[1]->cover) }}" alt="{{ $products1[1]->name }}"
                            class="img-baner-product-girl">
                    </div>
                </div>
                <div class="col-6 px-2">
                    <div class="card-body px-0">
                        <img src="{{ asset('storage/'.$products1[2]->cover) }}" alt="{{ $products1[2]->name }}"
                            class="img-baner-product-girl">
                    </div>
                </div>
                <div class="col-6 px-2">
                    <div class="card-body px-0">
                        <img src="{{ asset('storage/'.$products1[3]->cover) }}" alt="{{ $products1[3]->name }}"
                            class="img-baner-product-girl">
                    </div>
                </div>
                <div class="col-6 px-2">
                    <div class="card-body px-0">
                        <img src="{{ asset('storage/'.$products1[0]->cover) }}" alt="{{ $products1[0]->name }}"
                            class="img-baner-product-girl">
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>