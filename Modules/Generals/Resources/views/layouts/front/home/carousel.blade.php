<div id="slider" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#slider" data-slide-to="0" class="active"></li>
        <li data-target="#slider" data-slide-to="1"></li>
        <li data-target="#slider" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="{{ asset('img/fvn/slider1.png')}}" class="d-block w-100" alt="slider1">
        </div>
        <div class="carousel-item">
            <img src="{{ asset('img/fvn/slider2.png')}}" class="d-block w-100" alt="slider2">
        </div>
        <div class="carousel-item">
            <img src="{{ asset('img/fvn/slider3.png')}}" class="d-block w-100" alt="slider3">
        </div>
    </div>
</div>