<div class="mx-0 text-white" style=" background: #bf3d6b; ">
    <div class="mx-auto container-reset" style=" max-width: 1500px; ">
        <div class="row mx-0 content-statements">
            <div class="col-6 mx-0 col-md-3 d-lg-flex relative">
                <div class="absolute content-icon-statements">
                    <img src="{{ asset('img/fvn/send.png')}}" class="" alt="envios">
                </div>
                <div class="content-text-statements text-center">
                    <p>Despachos a todo Colombia</p>
                </div>
            </div>
            <div class="col-6 mx-0 col-md-3 d-lg-flex relative">
                <div class="absolute content-icon-statements">
                    <img src="{{ asset('img/fvn/card.png')}}" class="" alt="pagos">
                </div>
                <div class="content-text-statements text-center">
                    <p>Todos los metodos de pago</p>
                </div>

            </div>
            <div class="col-6 mx-0 col-md-3 d-lg-flex relative">
                <div class="absolute content-icon-statements-third">
                    <img src="{{ asset('img/fvn/favorite.png')}}" class="" alt="satisfechos">
                </div>
                <div class="content-text-statements text-center">
                    <p>Clientes Satisfechos</p>
                </div>

            </div>
            <div class="col-6 mx-0 col-md-3 d-lg-flex relative">
                <div class="absolute content-icon-statements">
                    <img src="{{ asset('img/fvn/contact.png')}}" class="" alt="contacto">
                </div>
                <div class="content-text-statements margin-t text-center">
                    <p>Contáctanos</p>
                </div>

            </div>
        </div>
    </div>
</div>