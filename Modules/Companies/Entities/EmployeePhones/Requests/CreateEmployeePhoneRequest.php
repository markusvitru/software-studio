<?php

namespace Modules\Companies\Entities\EmployeePhones\Requests;

use Modules\Generals\Entities\Base\BaseFormRequest;

class CreateEmployeePhoneRequest extends BaseFormRequest
{

    public function rules()
    {
        return [
            'employee_id' => ['required', 'bail'],
            'phone_type'  => ['required', 'bail'],
            'name'        => ['string', 'min:1'],
            'kinship'     => ['string', 'min:1'],
            'phone'       => ['required', 'max:30', 'unique:employee_phones']
        ];
    }
}
