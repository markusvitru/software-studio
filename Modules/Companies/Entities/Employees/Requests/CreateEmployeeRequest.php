<?php

namespace Modules\Companies\Entities\Employees\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateEmployeeRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'name'                 => ['required', 'max:255', 'bail'],
            'last_name'            => ['required', 'max:255', 'bail'],
            'email'                => ['required', 'max:255', 'email', 'unique:employees', 'bail'],
            'password'             => ['required', 'min:6', 'max:255', 'bail'],
            'role'                 => ['required'],
            'status'               => ['max:3'],
        ];
    }
}
