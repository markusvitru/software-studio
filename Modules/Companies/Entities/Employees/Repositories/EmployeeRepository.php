<?php

namespace Modules\Companies\Entities\Employees\Repositories;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Modules\Companies\Entities\Employees\Employee;
use Modules\Companies\Entities\Employees\Repositories\Interfaces\EmployeeRepositoryInterface;

class EmployeeRepository implements EmployeeRepositoryInterface
{
    private $columns = ['id', 'name', 'last_name', 'email', 'status', 'start_date', 'type_contract', 'subsidiary_id', 'manager_id'];

    public function __construct(Employee $employee)
    {
        $this->model = $employee;
    }

    public function listEmployees(int $totalView)
    {
        try {
            return  $this->model
                ->orderBy('name', 'desc')
                ->skip($totalView)->take(30)
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function searchEmployee(string $text = null, int $totalView, $from = null, $to = null): Collection
    {
        try {
            if (is_null($text) && is_null($from) && is_null($to)) {
                return $this->listEmployees($totalView);
            }

            if (!is_null($text) && (is_null($from) || is_null($to))) {
                return $this->model->searchEmployee($text, null, true, true)
                    ->skip($totalView)
                    ->take(30)
                    ->get($this->columns);
            }

            if (is_null($text) && (!is_null($from) || !is_null($to))) {
                return $this->model->whereBetween('created_at', [$from, $to])

                    ->skip($totalView)
                    ->take(30)
                    ->get($this->columns);
            }

            return $this->model->searchEmployee($text, null, true, true)
                ->whereBetween('created_at', [$from, $to])

                ->orderBy('created_at', 'desc')
                ->skip($totalView)
                ->take(30)
                ->get($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function countEmployees(string $text = null, $from = null, $to = null)
    {
        try {
            if (is_null($text) && is_null($from) && is_null($to)) {
                $data = $this->model->get(['id']);

                return count($data);
            }

            if (!is_null($text) && (is_null($from) || is_null($to))) {
                $data = $this->model->searchEmployee($text, null, true, true)

                    ->get(['id']);

                return count($data);
            }

            if (is_null($text) && (!is_null($from) || !is_null($to))) {
                $data = $this->model->whereBetween('created_at', [$from, $to])

                    ->get(['id']);

                return count($data);
            }

            $data = $this->model->searchEmployee($text, null, true, true)
                ->whereBetween('created_at', [$from, $to])

                ->get(['id']);

            return count($data);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }

        return $this->model->searchEmployee($text)->get();
    }

    public function searchTrashedEmployee(string $text = null): Collection
    {
        if (is_null($text)) {
            return $this->model->onlyTrashed($text)->get($this->columns);
        }

        return $this->model->onlyTrashed()->get($this->columns);
    }

    public function createEmployee(array $data): Employee
    {
        $token = Str::random(60);
        try {
            $data['password'] = Hash::make($data['password']);
            $data['company_id'] = 1;
            $data['access_token'] = $token;
            $data['api_token'] = hash('sha256', $token);

            return $this->model->create($data);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findEmployeeById(int $id): Employee
    {
        try {
            return $this->model->with([
                'employeeCommentaries',
                'department',
                'employeeStatusesLogs',
                'employeePosition',
                'employeeEmails',
                'employeePhones',
                'employeeAddresses',
                'employeeIdentities',
                'employeeEpss',
                'employeeProfessions',
                'bankData',
            ])->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findEmployeeByManagerId(int $managerId): Collection
    {
        try {
            return $this->model->where('manager_id', $managerId)->get($this->columns);
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findTrashedEmployeeById(int $id): Employee
    {
        try {
            return $this->model->withTrashed()->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function updateEmployee(array $params): bool
    {
        try {
            if (isset($params['password'])) {
                $params['password'] = Hash::make($params['password']);
            }

            return $this->model->update($params);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function syncRoles(array $roleIds)
    {
        try {
            $this->model->roles()->sync($roleIds);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function hasRole(string $roleName): bool
    {
        return $this->model->hasRole($roleName);
    }

    public function isAuthUser(Employee $employee): bool
    {
        $isAuthUser = false;
        if (Auth::guard('employee')->user()->id == $employee->id) {
            $isAuthUser = true;
        }

        return $isAuthUser;
    }

    public function deleteEmployee(): bool
    {
        try {
            return $this->model->delete();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function recoverTrashedEmployee(): bool
    {
        try {
            return $this->model->restore();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }
}
