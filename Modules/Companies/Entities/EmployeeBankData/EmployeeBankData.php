<?php

namespace Modules\Companies\Entities\EmployeeBankData;

use Modules\Companies\Entities\Employees\Employee;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeBankData extends Model
{
    use SoftDeletes;

    protected $table = 'employee_bank_data';

    protected $fillable = [
        'employee_id',
        'basic_salary',
        'extra_hours',
        'bank',
        'account_number'
    ];

    protected $hidden = [
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $dates  = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
