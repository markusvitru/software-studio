<?php

namespace Modules\Companies\Entities\EmployeeBankData\Repositories\Interfaces;

use Modules\Companies\Entities\EmployeeBankData\EmployeeBankData;

interface EmployeeBankDataRepositoryInterface
{
    public function createEmployeeBankData(array $data): EmployeeBankData;

    public function updateEmployeeBankData(array $params, $id): EmployeeBankData;

    public function findEmployeeBankDataByEmployeeId($employeeID): array;
}
