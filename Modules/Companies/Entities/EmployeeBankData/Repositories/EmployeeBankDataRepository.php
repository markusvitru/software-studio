<?php

namespace Modules\Companies\Entities\EmployeeBankData\Repositories;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Modules\Companies\Entities\EmployeeBankData\EmployeeBankData;
use Modules\Companies\Entities\EmployeeBankData\Repositories\Interfaces\EmployeeBankDataRepositoryInterface;

class EmployeeBankDataRepository implements EmployeeBankDataRepositoryInterface
{
    protected $model;

    public function __construct(EmployeeBankData $employeeBankData)
    {
        $this->model = $employeeBankData;
    }

    public function createEmployeeBankData(array $data): EmployeeBankData
    {
        $employeeBankData = $this->findEmployeeBankDataByEmployeeId($data['employee_id']);
        if($employeeBankData){
            return $this->updateEmployeeBankData($data, $employeeBankData[0]['id']);
        }

        try {
            return $this->model->create($data);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function updateEmployeeBankData(array $params, $id): EmployeeBankData
    {
        $employeeBankData = $this->findEmployeeBankDataById($id);
        try {
            $employeeBankData->update($params);
            return $employeeBankData;
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findEmployeeBankDataByEmployeeId($employeeID): array
    {
        try {
            return $this->model->where('employee_id', $employeeID)->get()->toArray();
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findEmployeeBankDataById($id): EmployeeBankData
    {
        try {
            return $this->model->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(503, $e->getMessage());
        }
    }
}
