@extends('generals::layouts.admin.app')
@section('header')
<div class="header pb-2">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item " active aria-current="page">Empleados</li>
                            <li class="breadcrumb-item active">Crear Empleado</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<section class="content">
    @include('generals::layouts.errors-and-messages')
    <div class="card">
        <form action="{{ route('admin.employees.store') }}" method="post" class="form">
            <div class="card-body">
                @csrf
                <div class="col pl-0 mb-3">
                    <h2>Crear Empleado</h2>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <label class="form-control-label" for="name">Nombre</label>
                            <div class="input-group input-group-merge">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                                </div>
                                <input type="text" name="name" id="name" validation-pattern="name" placeholder="Nombre"
                                    class="form-control" value="{{ old('name') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <label class="form-control-label" for="last_name">Apellido</label>
                            <div class="input-group input-group-merge">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                                </div>
                                <input type="text" name="last_name" id="last_name" validation-pattern="name"
                                    placeholder="Apellido" class="form-control" value="{{ old('last_name') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <label class="form-control-label" for="email">Email Usuario</label>
                            <div class="input-group input-group-merge">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                </div>
                                <input type="text" name="email" id="email" validation-pattern="email"
                                    placeholder="Email" class="form-control" value="{{ old('email') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div id="cities" class="form-group">
                            <label class="form-control-label" for="subsidiary_id">Sucursal</label>
                            <div class="input-group">
                                <select name="subsidiary_id" id="subsidiary_id" class="form-control"
                                    enabled>
                                    @foreach($all_subsiarries as $subsidiary)
                                        <option value="{{ $subsidiary->id }}">{{ $subsidiary->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-12">
                        <div class="form-group">
                            <label class="form-control-label" for="start_date">Fecha de inicio</label>
                            <div class="input-group input-group-merge">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-lock"></i></span>
                                </div>
                                <input type="date" name="start_date" id="start_date"
                                    class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label class="form-control-label" for="type_contract">Tipo de contrato</label>
                        <select class="form-control" name="type_contract" id="type_contract">
                            <option value="0">Término fijo</option>
                            <option value="1">Prestación de servicios</option>
                            <option value="2">Obra o labor</option>
                            <option value="3">Indefinido</option>
                        </select>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <label class="form-control-label" for="password">Password</label>
                            <div class="input-group input-group-merge">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-lock"></i></span>
                                </div>
                                <input type="password" name="password" id="password" placeholder="xxxxx"
                                    class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label class="form-control-label" for="role">Rol</label>
                        <select class="form-control" name="role" id="role" onchange="validateManager()">
                            @foreach($roles as $role)
                                <option value="{{ $role->id }}">{{ $role->display_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-12" id="containManager" style="display:none">
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <label class="form-control-label" for="manager_id">Supervisor</label>
                                <select class="form-control" name="manager_id" id="manager_id">
                                    <option value="">Seleccione</option>
                                    @foreach($managers as $manager)
                                        <option value="{{ $manager->id }}">{{ $manager->name." ".$manager->last_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-12 col-sm-6">
                                <label for="total">Meta Inicial</label>
                                <input type="number" name="total" id="total" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-primary btn-sm">Crear</button>
                <a href="{{ route('admin.employees.index') }}" class="btn btn-default btn-sm">Regresar</a>
            </div>
        </form>
    </div>
</section>

<script>
    function validateManager(){
        var cod = document.getElementById("role").value;
        if(cod == 3){
            document.getElementById("containManager").style.display = 'block';
        }else{
            document.getElementById("manager_id").value = "";
            document.getElementById("containManager").style.display = 'none';
        }
    };
</script>
@endsection
