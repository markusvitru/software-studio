@php
$fechaActual = strtotime(date("Y-m-d"));
$fechaMayorEdad = date("Y-m-d", strtotime("-18 years", $fechaActual));
@endphp
@extends('generals::layouts.admin.app')
@section('header')
<div class="header pb-2">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('admin.employees.index') }}">Empleados</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{$employee->name}}
                                {{$employee->last_name}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<section class="content">
    @include('generals::layouts.errors-and-messages')
    <div class="card">
        <div class="card-body">
            <ul class="nav nav-tabs border-0" id="myTab" role="tablist">
                <li class="nav-item {{ ($employee->role[0]->id != '3' && $employee->role[0]->id != '2') ? "active" : "" }}" role="presentation">
                    <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab"
                        aria-controls="home" aria-selected="true">Empleado</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                        aria-controls="profile" aria-selected="false">Contacto</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                        aria-controls="contact" aria-selected="false">Seguimiento</a>
                </li>
                @if ($employee->role[0]->id == '3' || $employee->role[0]->id == '2')
                <li class="nav-item active" role="presentation">
                    <a class="nav-link active" id="contact-tab" data-toggle="tab" href="#others" role="tab"
                        aria-controls="contact" aria-selected="false">Varios</a>
                </li>
                @endif
                @if ($employee->role[0]->id == '2')
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#models" role="tab"
                        aria-controls="contact" aria-selected="false">Modelos</a>
                </li>
                @endif
                @if ($employee->role[0]->id != '2')
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#bank" role="tab"
                        aria-controls="contact" aria-selected="false">Datos Bancarios</a>
                </li>
                @endif
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade {{ ($employee->role[0]->id != '3' && $employee->role[0]->id != '2') ? "active show" : "" }}" id="home" role="tabpanel" aria-labelledby="home-tab">
                    @include('companies::layouts.generals')
                    @include('companies::layouts.ids')
                    <div class="row">
                        @include('companies::layouts.epss')
                    </div>
                </div>
                <div class="tab-pane fade" id="bank" role="tabpanel" aria-labelledby="home-tab">
                    @include('companies::layouts.bank_data')
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="row">
                        @include('companies::layouts.addresses')
                        @include('companies::layouts.phones')
                        @include('companies::layouts.emails')
                    </div>
                </div>
                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                    <div class="row">
                        @include('generals::layouts.admin.commentaries', ['datas' => $employee->employeeCommentaries])
                        @include('companies::layouts.statusesLog', ['datas' => $employee->employeeStatusesLogs])
                    </div>
                </div>
                <div class="tab-pane fade {{ ($employee->role[0]->id == '3' || $employee->role[0]->id == '2') ? "active show" : "" }}" id="others" role="tabpanel" aria-labelledby="contact-tab">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            @include('studio::layouts.horaries.horaryEmployee', ['employee' => $employee, 'horaries' => $horaries])
                        </div>
                        @if ($employee->role[0]->id == '3')
                            <div class="col-12 col-sm-6">
                                @include('studio::layouts.penalties.penaltyEmployee', ['employee' => $employee, 'penalties' => $penalties])
                            </div>
                            <div class="col-12 col-sm-6">
                                @include('studio::layouts.platforms.platformEmployee', ['employee' => $employee, 'penalties' => $penalties])
                            </div>
                            <div class="col-12 col-sm-6">
                                @include('studio::layouts.goals.goalEmployee', ['employee' => $employee, 'goals' => $goals])
                            </div>
                        @endif
                    </div>
                </div>
                <div class="tab-pane fade" id="models" role="tabpanel" aria-labelledby="contact-tab">
                    <div class="row">
                        <div class="col-12">
                            @include('studio::layouts.models', [$models])
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <a href="{{ route('admin.employees.index') }}" class="btn btn-default btn-sm">Regresar</a>
            </div>
        </div>
    </div>

    @include('companies::layouts.add_address_modal')
    @include('companies::layouts.add_email_modal')
    @include('companies::layouts.add_phone_modal')
    @include('companies::layouts.add_identity_modal')
    @include('companies::layouts.add_comment_modal')
    @include('companies::layouts.add_eps_modal')
    @include('companies::layouts.add_bank_data_modal')
    @include('companies::layouts.add_profession_modal')
</section>
@endsection
