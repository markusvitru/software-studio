@extends('generals::layouts.admin.app')
@section('header')
<div class="header pb-2">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('admin.subsidiaries.index') }}">Sucursales</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{$subsidiary->name}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')

<section class="content">
    @include('generals::layouts.errors-and-messages')
    <div class="card">
        <div class="card-body">
            <ul class="nav nav-tabs border-0" id="infoTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#info" role="tab"
                        aria-controls="home" aria-selected="true">Información</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="managersTab" data-toggle="tab" href="#managers" role="tab"
                        aria-controls="profile" aria-selected="false">Managers - Salas</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="info" role="tabpanel" aria-labelledby="home-tab">
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center mb-3">
                                <div class="col-12">
                                    <h2>{{ $subsidiary->name }} {{ $subsidiary->city->name }}</h2>
                                    <div class="table-responsive">
                                        <table class="table align-items-center table-flush table-hover">
                                                <thead class="thead-light">
                                                <tr>
                                                    <th class="text-center" scope="col">Dirección</th>
                                                    <th class="text-center" scope="col">Telefono</th>
                                                    <th class="text-center" scope="col">Horario</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="text-center">{{ $subsidiary->address }}</td>
                                                    <td class="text-center">{{ $subsidiary->phone }}</td>
                                                    <td class="text-center">{{ $subsidiary->opening_hours }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="managers" role="tabpanel" aria-labelledby="home-tab">
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center mb-3">
                                <div class="col-12 col-sm-6">
                                    <h2>{{ $subsidiary->name }} {{ $subsidiary->city->name }} - Managers</h2>
                                    <div class="table-responsive">
                                        <table class="table align-items-center table-flush table-hover">
                                                <thead class="thead-light">
                                                <tr>
                                                    <th class="text-center" scope="col">Nombre</th>
                                                    <th class="text-center" scope="col">Apellido</th>
                                                    <th class="text-center" scope="col">Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($employees as $employee)
                                                    <tr>
                                                        <td class="text-center">{{ $employee->name }}</td>
                                                        <td class="text-center">{{ $employee->last_name }}</td>
                                                        <td class="text-center">
                                                            <a href="{{ route('admin.employees.show', $employee->id) }}" class=" table-action table-action" data-toggle="tooltip" data-original-title="Ver Empleado">
                                                                <i class="fas fa-search"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <h2>Salas</h2>
                                    <div class="table-responsive">
                                        <table class="table align-items-center table-flush table-hover">
                                                <thead class="thead-light">
                                                <tr>
                                                    <th class="text-center" scope="col">Nombre</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($subsidiary->rooms as $rooms)
                                                    <tr>
                                                        <td class="text-center">{{ $rooms->name }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <div class="btn-group">
            <a href="{{ route('admin.subsidiaries.index') }}" class="btn btn-default btn-sm">Regresar</a>
        </div>
    </div>

</section>

@endsection
