<div class="modal fade" id="bankDataModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Agregar Datos Bancarios</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ route('admin.employee-bank-data.store') }}" method="post" class="form"
          enctype="multipart/form-data">
        <div class="modal-body py-0">
          @csrf
          <input name="employee_id" id="employee_id" type="hidden" value="{{ $employee->id }}">
          @if($employee->bankData)
          <div class="row">
            <div class="col-sm-12 col-md-6 form-group">
                <label class="form-control-label" for="bank">Banco</label>
                <input type="text" name="bank" id="bank" value="{!! $employee->bankData->bank ?: ''  !!}" class="form-control">
			</div>
            <div class="col-sm-12 col-md-6 form-group">
                <label class="form-control-label" for="account_number">Número de Cuenta</label>
                <input type="text" name="account_number" id="account_number" value="{!! $employee->bankData->account_number ?: ''  !!}" class="form-control">
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12 col-md-6 form-group">
                  <label for="basic_salary" class="form-control-label">Salario Básico</label>
                  <input type="number" name="basic_salary" id="basic_salary" value="{!! $employee->bankData->basic_salary ?: ''  !!}" class="form-control">
            </div>
            <div class="col-sm-12 col-md-6 form-group">
                <label for="extra_hours" class="form-control-label">Horas Extras</label>
                <input type="number" name="extra_hours" id="extra_hours" value="{!! $employee->bankData->extra_hours ?: ''  !!}" class="form-control">
			</div>
          </div>
          @else
          <div class="row">
            <div class="col-sm-12 col-md-6 form-group">
                <label class="form-control-label" for="bank">Banco</label>
                <input type="text" name="bank" id="bank" class="form-control">
			</div>
            <div class="col-sm-12 col-md-6 form-group">
                <label class="form-control-label" for="account_number">Número de Cuenta</label>
                <input type="text" name="account_number" id="account_number" class="form-control">
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12 col-md-6 form-group">
                  <label for="basic_salary" class="form-control-label">Salario Básico</label>
                  <input type="number" name="basic_salary" id="basic_salary" class="form-control">
            </div>
            <div class="col-sm-12 col-md-6 form-group">
                <label for="extra_hours" class="form-control-label">Horas Extras</label>
                <input type="number" name="extra_hours" id="extra_hours" class="form-control">
			</div>
          </div>
        @endif
        </div>
        <div class="w-100 p-3 text-right">
          <button type="submit" class="btn btn-primary">Agregar</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </form>
    </div>
  </div>
</div>
