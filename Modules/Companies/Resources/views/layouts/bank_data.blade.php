<div class="card">
  <div class="card-body">
    <div class="row align-items-center mb-3">
      <div class="col">
        <h3 class="mb-0">Datos Bancarios</h3>
      </div>
      <div class="col text-right">
      <a href="#" data-toggle="modal" data-target="#bankDataModal" class="btn btn-primary btn-sm"><i
                class="fa fa-edit"></i>
              Agregar Datos</a>
      </div>
    </div>
    <div class="w-100">
      <div class="table-responsive">
      @if($employee->bankData)

        <table class="table align-items-center table-flush table-hover text-center">
          <thead class="thead-light">
            <tr>
                <th  scope="col">Salario Básico</th>
                <th  scope="col">Horas Extras</th>
                <th  scope="col">Banco</th>
                <th  scope="col">Número de Cuenta</th>
            </tr>
          </thead>
          <tbody class="list">
            <tr>
                <td>{{ $employee->bankData->basic_salary }}</td>
                <td>{{ $employee->bankData->extra_hours }}</td>
                <td>{{ $employee->bankData->bank }}</td>
                <td>{{ $employee->bankData->account_number }}</td>
            </tr>
          </tbody>
        </table>
        @else
        <span class="text-sm">Aún no tiene Identificación</span>
        @endif
        <div class="row mt-3 mx-0">
          <div class="col text-right">
            <form action="{{ route('admin.employees.destroy', $employee['id']) }}" method="post" class="form-horizontal">
              @csrf
              <input type="hidden" name="_method" value="delete">
              <div class="btn-group">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>

</div>
