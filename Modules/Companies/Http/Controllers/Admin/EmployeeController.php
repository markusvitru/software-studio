<?php

namespace Modules\Companies\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Modules\Generals\Entities\Cities\Repositories\Interfaces\CityRepositoryInterface;
use Modules\Generals\Entities\IdentityTypes\Repositories\Interfaces\IdentityTypeRepositoryInterface;
use Modules\Generals\Entities\Stratums\Repositories\Interfaces\StratumRepositoryInterface;
use Modules\Generals\Entities\Housings\Repositories\Interfaces\HousingRepositoryInterface;
use Modules\Generals\Entities\Epss\Repositories\Interfaces\EpsRepositoryInterface;
use Modules\Generals\Entities\ProfessionsLists\Repositories\Interfaces\ProfessionsListRepositoryInterface;
use Modules\Companies\Entities\Employees\Requests\UpdateEmployeeRequest;
use Modules\Companies\Entities\Employees\Repositories\EmployeeRepository;
use Modules\Companies\Entities\Employees\Repositories\Interfaces\EmployeeRepositoryInterface;
use Modules\Companies\Entities\Roles\Repositories\Interfaces\RoleRepositoryInterface;
use Modules\Companies\Entities\Departments\Repositories\Interfaces\DepartmentRepositoryInterface;
use Modules\Companies\Entities\EmployeePositions\Repositories\Interfaces\EmployeePositionRepositoryInterface;
use Modules\Companies\Entities\EmployeeCommentaries\Repositories\Interfaces\EmployeeCommentaryRepositoryInterface;
use Modules\Companies\Entities\EmployeeStatusesLogs\Repositories\Interfaces\EmployeeStatusesLogRepositoryInterface;
use Modules\Companies\Entities\Employees\Requests\CreateEmployeeRequest;
use Modules\Companies\Entities\Subsidiaries\Repositories\Interfaces\SubsidiaryRepositoryInterface;
use Modules\Generals\Entities\Tools\ToolRepositoryInterface;
use Modules\Studio\Entities\Goals\Repositories\Interfaces\GoalRepositoryInterface;
use Modules\Studio\Entities\Horaries\Repositories\Interfaces\HoraryRepositoryInterface;
use Modules\Studio\Entities\Penalties\Repositories\Interfaces\PenaltyRepositoryInterface;
use Modules\Studio\Entities\Platforms\Repositories\Interfaces\PlatformRepositoryInterface;

class EmployeeController extends Controller
{
    private $employeeInterface, $roleInterface, $departmentInterface;
    private $employeePositionInterface, $employeeCommentaryInterface;
    private $employeeStatusesLogInterface, $cityInterface, $identityTypeInterface;
    private $stratumInterface, $housingInterface, $epsInterface;
    private $professionsListInterface, $toolsInterface;
    private $subsidiaryInterface;
    private $horaryInterface, $penaltyInterface;
    private $platformInterface;
    private $goalInterface;

    public function __construct(
        EmployeeRepositoryInterface $employeeRepositoryInterface,
        RoleRepositoryInterface $roleRepositoryInterface,
        DepartmentRepositoryInterface $departmentRepositoryInterface,
        EmployeePositionRepositoryInterface $employeePositionRepositoryInterface,
        EmployeeCommentaryRepositoryInterface $employeeCommentaryRepositoryInterface,
        EmployeeStatusesLogRepositoryInterface $employeeStatusesLogRepositoryInterface,
        CityRepositoryInterface $cityRepositoryInterface,
        IdentityTypeRepositoryInterface $identityTypeRepositoryInterface,
        StratumRepositoryInterface $stratumRepositoryInterface,
        HousingRepositoryInterface $housingRepositoryInterface,
        EpsRepositoryInterface $epsRepositoryInterface,
        ProfessionsListRepositoryInterface $professionsListRepositoryInterface,
        ToolRepositoryInterface $toolRepositoryInterface,
        SubsidiaryRepositoryInterface $subsidiaryRepositoryInterface,
        HoraryRepositoryInterface $horaryRepositoryInterface,
        PenaltyRepositoryInterface $penaltyRepositoryInterface,
        PlatformRepositoryInterface $platformRepositoryInterface,
        GoalRepositoryInterface $goalRepositoryInterface
    ) {
        $this->toolsInterface               = $toolRepositoryInterface;
        $this->employeeInterface            = $employeeRepositoryInterface;
        $this->roleInterface                = $roleRepositoryInterface;
        $this->departmentInterface          = $departmentRepositoryInterface;
        $this->employeePositionInterface    = $employeePositionRepositoryInterface;
        $this->employeeCommentaryInterface  = $employeeCommentaryRepositoryInterface;
        $this->employeeStatusesLogInterface = $employeeStatusesLogRepositoryInterface;
        $this->cityInterface                = $cityRepositoryInterface;
        $this->identityTypeInterface        = $identityTypeRepositoryInterface;
        $this->stratumInterface             = $stratumRepositoryInterface;
        $this->housingInterface             = $housingRepositoryInterface;
        $this->epsInterface                 = $epsRepositoryInterface;
        $this->professionsListInterface     = $professionsListRepositoryInterface;
        $this->subsidiaryInterface          = $subsidiaryRepositoryInterface;
        $this->horaryInterface              = $horaryRepositoryInterface;
        $this->penaltyInterface             = $penaltyRepositoryInterface;
        $this->platformInterface            = $platformRepositoryInterface;
        $this->goalInterface = $goalRepositoryInterface;
        $this->middleware(['permission:employees, guard:employee']);
    }

    public function index(Request $request)
    {
        $skip = request()->input('skip') ? request()->input('skip') : 0;
        $from = request()->input('from') ? request()->input('from') . " 00:00:01" : Carbon::now()->subMonths(1);
        $to   = request()->input('to') ? request()->input('to') . " 23:59:59" : Carbon::now();

        $list = $this->employeeInterface->listEmployees($skip * 30);
        $role = $this->roleInterface->findRoleById('2');
        $managers = $role->user;
        if (request()->input('q') != '' && (request()->input('from') == '' || request()->input('to') == '')) {
            $list = $this->employeeInterface->searchEmployee(request()->input('q'), $skip * 30);
            $paginate = $this->employeeInterface->countEmployees(request()->input('q'));
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } elseif ((request()->input('q') != '' || request()->input('from') != '' || request()->input('to') != '')) {
            $list = $this->employeeInterface->searchEmployee(request()->input('q'), $skip * 30, $from, $to);
            $paginate = $this->employeeInterface->countEmployees(request()->input('q'), $from, $to);
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } else {
            $paginate = $this->employeeInterface->countEmployees('');
            $list = $this->employeeInterface->listEmployees($skip * 30);
        }

        $paginate = ceil($paginate  / 30);

        $skipPaginate = $skip;

        $pageList = ($skipPaginate + 1) / 5;
        if (is_int($pageList) || $pageList > 1) {
            $countPage = $skipPaginate - 5;
            $maxPage = $skipPaginate + 6 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 6;
        } else {
            $countPage = 0;
            $maxPage = $skipPaginate + 5 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 5;
        }

        return view('companies::admin.employees.list', [
            'employees'          => $list,
            'searchInputs'    => [['label' => 'Buscar', 'type' => 'text', 'name' => 'q'], ['label' => 'Desde', 'type' => 'date', 'name' => 'from'], ['label' => 'Hasta', 'type' => 'date', 'name' => 'to']],
            'optionsRoutes'      => 'admin.' . (request()->segment(2)),
            'skip'               => $skip,
            'pag'                => $pageList,
            'i'                  => $countPage,
            'max'                => $maxPage,
            'paginate'           => $paginate,
            'headers'            => ['Id', 'Nombre', 'Apellido', 'Email', 'Fecha de inicio', 'Tipo de contrato', 'Sucursal', 'Estado', 'Opciones'],
            'roles'              => $this->roleInterface->getAllRoleNames(),
            'all_subsiarries'    => $this->subsidiaryInterface->getAllSubsidiaryNames(),
            'managers'           => $managers
        ]);
    }

    public function create()
    {
        $role = $this->roleInterface->findRoleById('2');
        $managers = $role->user;
        return view('companies::admin.employees.create', [
            'roles'           => $this->roleInterface->getAllRoleNames(),
            'all_subsiarries' => $this->subsidiaryInterface->getAllSubsidiaryNames(),
            'managers'        => $managers
        ]);
    }

    public function store(CreateEmployeeRequest $request)
    {
        $employee = $this->employeeInterface->createEmployee($request->except('total'));
        if ($request->input('role') == 3) {
            if (gettype($request->input('total')) !== 'NULL' && $request->input('total') > 0) {
                $this->goalInterface->createGoalForModel($request->input('total'), $employee->id);
            }
        }
        $data = array(
            'employee_id' => $employee->id,
            'status'      => 'Creado',
            'user_id'     => auth()->guard('employee')->user()->id
        );

        $this->employeeStatusesLogInterface->createEmployeeStatusesLog($data);

        if ($request->has('role')) {
            $employeeRepo = new EmployeeRepository($employee);
            $employeeRepo->syncRoles([$request->input('role')]);
        }

        return redirect()->route('admin.employees.index')->with('message', 'Empleado Creado Exitosamente!');
    }

    public function show(int $id)
    {
        $goals = [];
        $horaries          = [];
        $platforms         = [];
        $employee          = $this->employeeInterface->findEmployeeById($id);
        $employeeHoraries  = $employee->horaries->toArray();
        $employeePlatforms = $employee->platforms->toArray();
        $models            = $this->employeeInterface->findEmployeeByManagerId($id);
        $employeeGoals = $employee->goals->toArray();
        if (count($employeeHoraries) > 0) {
            foreach ($employeeHoraries as  $horary) {
                $horaries[] = $horary['id'];
            }
        }

        if (count($employeePlatforms)) {
            foreach ($employeePlatforms as  $platform) {
                $platforms[] = $platform['id'];
            }
        }

        if (count($employeeGoals)) {
            foreach ($employeeGoals as $goal) {
                $goal['month'] = $this->toolsInterface->getMonthPerNumber($goal['month']);
                $goal['fortnight'] = ($goal['fortnight'] == 0) ? '01-15' : '16-30';
                $goals[] = $goal;
            }
        }

        return view('companies::admin.employees.show', [
            'employee'          => $employee,
            'cities'            => $this->cityInterface->listCities(),
            'identity_types'    => $this->identityTypeInterface->getAllIdentityTypesNames(),
            'stratums'          => $this->stratumInterface->getAllStratumsNames(),
            'housings'          => $this->housingInterface->getAllHousingsNames(),
            'epss'              => $this->epsInterface->getAllEpsNames(),
            'professions_lists' => $this->professionsListInterface->getAllProfessionsNames(),
            'horaries'          => $this->horaryInterface->listAllHoraries($horaries),
            'penalties'         => $this->penaltyInterface->listAllPenalties(),
            'platforms'         => $this->platformInterface->listAllPlatforms($platforms),
            'models'            => $models,
            'goals' => $goals
        ]);
    }

    public function edit(int $id)
    {
        return redirect()->route('admin.employees.index');
    }

    public function update(UpdateEmployeeRequest $request, $id)
    {
        $employee      = $this->employeeInterface->findEmployeeById($id);
        $empRepo       = new EmployeeRepository($employee);
        $empRepo->updateEmployee($request->except('_token', '_method', 'password'));

        if ($request->has('password') && !empty($request->input('password'))) {
            $employee->password = Hash::make($request->input('password'));
            $employee->save();
        }

        if ($request->has('role')) {
            $employeeRepo = new EmployeeRepository($employee);
            $employeeRepo->syncRoles([$request->input('role')]);
        }

        return redirect()->route('admin.employees.index')->with('message', 'Actualización exitosa');
    }

    public function destroy(int $id)
    {
        $employee     = $this->employeeInterface->findEmployeeById($id);
        $employeeRepo = new EmployeeRepository($employee);
        $employeeRepo->deleteEmployee();

        return redirect()->route('admin.employees.index')
            ->with('message', 'Eliminado Satisfactoriamente');
    }

    public function getProfile($id)
    {
        return view('companies::admin.employees.profile', [
            'employee' => $this->employeeInterface->findEmployeeById($id)
        ]);
    }

    public function updateProfile(UpdateEmployeeRequest $request, $id)
    {
        $employee = $this->employeeInterface->findEmployeeById($id);
        $update   = new EmployeeRepository($employee);
        $update->updateEmployee($request->except('_token', '_method', 'password'));

        if ($request->has('password') && $request->input('password') != '') {
            $update->updateEmployee($request->only('password'));
        }

        return redirect()->route('admin.employee.profile', $id)
            ->with('message', 'Actualización Exitosa!');
    }

    public function recoverTrashedEmployee(int $id)
    {
        $employee     = $this->employeeInterface->findTrashedEmployeeById($id);
        $employeeRepo = new EmployeeRepository($employee);
        $employeeRepo->recoverTrashedEmployee();

        return redirect()->route('admin.employees.index')
            ->with('message', 'Recuperación Exitosa!');
    }

    public function addHorary(Request $request, $id)
    {
        $employee = $this->employeeInterface->findEmployeeById($id);
        $employee->horaries()->attach($request->input('horary_id'));

        return redirect()->route('admin.employees.show', $id)
            ->with('message', 'Horario Añadido Exitosamente!');
    }

    public function deleteHorary(Request $request, $id)
    {
        $employee = $this->employeeInterface->findEmployeeById($id);
        $employee->horaries()->detach($request->input('horary_id'));

        return redirect()->route('admin.employees.show', $id)
            ->with('message', 'Horario Eliminado Exitosamente!');
    }

    public function addPenalty(Request $request, $id)
    {
        $employee = $this->employeeInterface->findEmployeeById($id);
        $employee->penalties()->attach($request->input('penalty_id'), ['created_at' => date('Y-m-d h:m:s')]);

        return redirect()->route('admin.employees.show', $id)
            ->with('message', 'Multa Añadida Exitosamente!');
    }

    public function deletePenalty(Request $request, $id)
    {
        $employee = $this->employeeInterface->findEmployeeById($id);
        $employee->penalties()->detach($request->input('horary_id'));

        return redirect()->route('admin.employees.show', $id)
            ->with('message', 'Multa Eliminada Exitosamente!');
    }

    public function addPlatform(Request $request, $id)
    {
        $employee = $this->employeeInterface->findEmployeeById($id);
        $employee->platforms()->attach($request->input('platform_id'));

        return redirect()->route('admin.employees.show', $id)
            ->with('message', 'Plataforma Añadida Exitosamente!');
    }

    public function deletePlatform(Request $request, $id)
    {
        $employee = $this->employeeInterface->findEmployeeById($id);
        $employee->platforms()->detach($request->input('platform_id'));

        return redirect()->route('admin.employees.show', $id)
            ->with('message', 'Plataforma Eliminada Exitosamente!');
    }
}
