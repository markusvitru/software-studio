<?php

namespace Modules\Companies\Http\Controllers\Admin\Subsidiaries;

use Modules\Companies\Entities\Subsidiaries\Repositories\SubsidiaryRepository;
use Modules\Companies\Entities\Subsidiaries\Repositories\Interfaces\SubsidiaryRepositoryInterface;
use Modules\Companies\Entities\Subsidiaries\Requests\CreateSubsidiaryRequest;
use Modules\Companies\Entities\Subsidiaries\Requests\UpdateSubsidiaryRequest;
use Modules\Generals\Entities\Cities\Repositories\Interfaces\CityRepositoryInterface;
use App\Http\Controllers\Controller;
use Modules\Generals\Entities\Tools\ToolRepositoryInterface;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Modules\Companies\Entities\Roles\Repositories\Interfaces\RoleRepositoryInterface;

class SubsidiaryController extends Controller
{
    private $subsidiaryInterface, $cityInterface, $toolsInterface;
    private $roleInterface;

    public function __construct(
        SubsidiaryRepositoryInterface $subsidiaryRepositoryInterface,
        CityRepositoryInterface $cityRepositoryInterface,
        ToolRepositoryInterface $toolRepositoryInterface,
        RoleRepositoryInterface $roleRepositoryInterface
    ) {
        $this->toolsInterface      = $toolRepositoryInterface;
        $this->subsidiaryInterface = $subsidiaryRepositoryInterface;
        $this->cityInterface       = $cityRepositoryInterface;
        $this->roleInterface       = $roleRepositoryInterface;
        $this->middleware(['permission:subsidiaries, guard:employee']);
    }


    public function index(Request $request)
    {
        $skip    = request()->input('skip') ? request()->input('skip') : 0;
        $from    = request()->input('from') ? request()->input('from') . " 00:00:01" : Carbon::now()->subMonths(1);
        $to      = request()->input('to') ? request()->input('to') . " 23:59:59" : Carbon::now();
        $role = $this->roleInterface->findRoleById('4');
        $employees = [];
        foreach ($role->user->toArray() as $key => $value) {
            $employees[] = ['id' => $value['id'], 'label' => $value['name'] . ' ' . $value['last_name']];
        }

        if (request()->input('q') != '' && (request()->input('from') == '' || request()->input('to') == '')) {
            $list = $this->subsidiaryInterface->searchSubsidiary(request()->input('q'), $skip * 30);
            $paginate = $this->subsidiaryInterface->countSubsidiaries(request()->input('q'));
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } elseif ((request()->input('q') != '' || request()->input('from') != '' || request()->input('to') != '')) {
            $list = $this->subsidiaryInterface->searchSubsidiary(request()->input('q'), $skip * 30, $from, $to);
            $paginate = $this->subsidiaryInterface->countSubsidiaries(request()->input('q'), $from, $to);
            $request->session()->flash('message', 'Resultado de la Busqueda');
        } else {
            $paginate = $this->subsidiaryInterface->countSubsidiaries('');
            $list = $this->subsidiaryInterface->listSubsidiaries($skip * 30);
        }

        $paginate = ceil($paginate  / 30);

        $skipPaginate = $skip;

        $pageList = ($skipPaginate + 1) / 5;
        if (is_int($pageList) || $pageList > 1) {
            $countPage = $skipPaginate - 5;
            $maxPage = $skipPaginate + 6 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 6;
        } else {
            $countPage = 0;
            $maxPage = $skipPaginate + 5 > $paginate ? intval($skipPaginate + ($paginate - $skipPaginate)) : $skipPaginate + 5;
        }

        return view('companies::admin.subsidiaries.list', [
            'subsidiaries'  =>  $list,
            'skip'          => $skip,
            'cities'        => $this->cityInterface->listCities(),
            'optionsRoutes' => 'admin.' . (request()->segment(2)),
            'headers'       => ['ID', 'Sucursal', 'Dirección', 'Teléfono', 'Ciudad', 'Opciones'],
            'searchInputs'    => [['label' => 'Buscar', 'type' => 'text', 'name' => 'q'], ['label' => 'Desde', 'type' => 'date', 'name' => 'from'], ['label' => 'Hasta', 'type' => 'date', 'name' => 'to']],
            'skip'          => $skip,
            'pag'           => $pageList,
            'i'             => $countPage,
            'employees'     => $employees,
            'max'           => $maxPage,
            'paginate'      => $paginate
        ]);
    }

    public function create()
    {
        $role = $this->roleInterface->findRoleById('4');
        $employees = [];
        foreach ($role->user->toArray() as $key => $value) {
            $employees[] = ['id' => $value['id'], 'label' => $value['name'] . ' ' . $value['last_name']];
        }
        return view('companies::admin.subsidiaries.create', [
            'cities' => $this->cityInterface->getAllCityNames(),
            'employees' => $employees
        ]);
    }

    public function store(CreateSubsidiaryRequest $request)
    {
        $this->subsidiaryInterface->createSubsidiary($request->except('_token', '_method'));

        return redirect()->route('admin.subsidiaries.index')
            ->with('message', 'Sucursal Creada Exitosamente!');
    }

    public function show($id)
    {
        $subsidiary = $this->subsidiaryInterface->findSubsidiaryById($id);
        $role = $this->roleInterface->findRoleById('2');
        $employees = $role->user()->where('subsidiary_id', $id)->get();
        return view('companies::admin.subsidiaries.show', [
            'subsidiary'   => $subsidiary,
            'subsidiaries' => $subsidiary->children,
            'employees'    => $employees,
        ]);
    }

    public function edit($id)
    {
        $subsidiary = $this->subsidiaryInterface->findSubsidiaryById($id);

        return view('companies::admin.subsidiaries.edit', [
            'subsidiary' => $subsidiary,
            'cities'     => $this->cityInterface->listCities(),
            'cityId'     => $subsidiary->city_id,
        ]);
    }

    public function update(UpdateSubsidiaryRequest $request, $id)
    {
        $update = new SubsidiaryRepository($this->subsidiaryInterface->findSubsidiaryById($id));
        $update->updateSubsidiary($request->except('_token', '_method'));
        $request->session()->flash('message', 'Actualizacion Exitosa');

        return redirect()->route('admin.subsidiaries.index');
    }


    public function destroy(int $id)
    {
        $subsidiary = new SubsidiaryRepository($this->subsidiaryInterface->findSubsidiaryById($id));
        $subsidiary->deleteSubsidiary();

        request()->session()->flash('message', 'Eliminado Satisfactoriamente');
        return redirect()->route('admin.subsidiaries.index');
    }

    public function recoverTrashedSubsidiary(int $id)
    {
        $subsidiaryRepo = new SubsidiaryRepository($this->subsidiaryInterface->findTrashedSubsidiaryById($id));
        $subsidiaryRepo->recoverTrashedSubsidiary();

        return redirect()->route('admin.subsidiaries.index')
            ->with('message', 'Recuperación Exitosa!');
    }
}
