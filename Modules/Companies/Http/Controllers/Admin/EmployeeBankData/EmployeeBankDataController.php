<?php

namespace Modules\Companies\Http\Controllers\Admin\EmployeeBankData;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Companies\Entities\EmployeeBankData\Repositories\Interfaces\EmployeeBankDataRepositoryInterface;

class EmployeeBankDataController extends Controller
{

    private $employeeBankDataRepository;

    public function __construct(
        EmployeeBankDataRepositoryInterface $employeeBankDataRepositoryInterface
    ) {
        $this->employeeBankDataRepository = $employeeBankDataRepositoryInterface;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employeeBankData = $this->employeeBankDataRepository->createEmployeeBankData($request->input());

        $request->session()->flash('message', 'Datos Bancarios Agregados Exitosamente');

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employeeBankData = $this->employeeBankDataRepository->updateEmployeeBankData($request->input(), $id);

        $request->session()->flash('message', 'Información actualizada');

        return back();
    }
}
